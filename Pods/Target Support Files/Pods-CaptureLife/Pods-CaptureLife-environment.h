
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// CardIO
#define COCOAPODS_POD_AVAILABLE_CardIO
#define COCOAPODS_VERSION_MAJOR_CardIO 5
#define COCOAPODS_VERSION_MINOR_CardIO 0
#define COCOAPODS_VERSION_PATCH_CardIO 4

// IQKeyboardManager
#define COCOAPODS_POD_AVAILABLE_IQKeyboardManager
#define COCOAPODS_VERSION_MAJOR_IQKeyboardManager 3
#define COCOAPODS_VERSION_MINOR_IQKeyboardManager 2
#define COCOAPODS_VERSION_PATCH_IQKeyboardManager 3

// MKNetworkKit
#define COCOAPODS_POD_AVAILABLE_MKNetworkKit
#define COCOAPODS_VERSION_MAJOR_MKNetworkKit 0
#define COCOAPODS_VERSION_MINOR_MKNetworkKit 87
#define COCOAPODS_VERSION_PATCH_MKNetworkKit 0

// Reachability
#define COCOAPODS_POD_AVAILABLE_Reachability
#define COCOAPODS_VERSION_MAJOR_Reachability 3
#define COCOAPODS_VERSION_MINOR_Reachability 1
#define COCOAPODS_VERSION_PATCH_Reachability 1

// SDWebImage
#define COCOAPODS_POD_AVAILABLE_SDWebImage
#define COCOAPODS_VERSION_MAJOR_SDWebImage 3
#define COCOAPODS_VERSION_MINOR_SDWebImage 7
#define COCOAPODS_VERSION_PATCH_SDWebImage 2

// SDWebImage/Core
#define COCOAPODS_POD_AVAILABLE_SDWebImage_Core
#define COCOAPODS_VERSION_MAJOR_SDWebImage_Core 3
#define COCOAPODS_VERSION_MINOR_SDWebImage_Core 7
#define COCOAPODS_VERSION_PATCH_SDWebImage_Core 2

// Wit
#define COCOAPODS_POD_AVAILABLE_Wit
#define COCOAPODS_VERSION_MAJOR_Wit 4
#define COCOAPODS_VERSION_MINOR_Wit 0
#define COCOAPODS_VERSION_PATCH_Wit 0

