//
//  VC_pickupStore.h
//  CaptureLife
//
//  Created by Fahim Bilwani on 11/06/2015.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VC_pickupStore : UIViewController

@property (strong,nonatomic) NSString *storeName;
@end
