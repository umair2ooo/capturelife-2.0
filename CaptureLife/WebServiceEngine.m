#import "WebServiceEngine.h"

@implementation WebServiceEngine

-(id) initWithDefaultSettings {
    
    if(self = [super initWithHostName:k_MKNetworkURL customHeaderFields:@{@"x-client-identifier" : @"iOS"}])
    {
        
    }
    
    return self;
}

-(void) basicAuthTest {
    
    MKNetworkOperation *op = [self operationWithPath:@"basic_auth.php"
                                              params:nil
                                          httpMethod:@"GET"];
    
    [op setUsername:@"admin" password:@"password" basicAuth:YES];
    
    [op addCompletionHandler:^(MKNetworkOperation *operation) {
        
        DLog(@"%@", [operation responseString]);
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        DLog(@"%@", [error localizedDescription]);
    }];
    [self enqueueOperation:op];
}

-(void) digestAuthTest {
    
    MKNetworkOperation *op = [self operationWithPath:@"digest_auth.php"
                                              params:nil
                                          httpMethod:@"GET"];
    
    [op setUsername:@"admin" password:@"password"];
    [op setCredentialPersistence:NSURLCredentialPersistenceNone];
    
    [op addCompletionHandler:^(MKNetworkOperation *operation) {
        
        DLog(@"%@", [operation responseString]);
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        DLog(@"%@", [error localizedDescription]);
    }];
    [self enqueueOperation:op];
}

-(MKNetworkOperation*) downloadFatAssFileFrom:(NSString*) remoteURL toFile:(NSString*) filePath {
    
    MKNetworkOperation *op = [self operationWithURLString:remoteURL];
    
    [op addDownloadStream:[NSOutputStream outputStreamToFileAtPath:filePath
                                                            append:YES]];
    
    [self enqueueOperation:op];
    return op;
}

-(MKNetworkOperation*) uploadImageFromFile:(NSString*) file
                         completionHandler:(IDBlock) completionBlock
                              errorHandler:(MKNKErrorBlock) errorBlock {
    
    MKNetworkOperation *op = [self operationWithPath:@"upload.php"
                                              params:@{@"Submit": @"YES"}
                                          httpMethod:@"POST"];
    
    [op addFile:file forKey:@"image"];
    
    // setFreezable uploads your images after connection is restored!
    [op setFreezable:YES];
    
    [op addCompletionHandler:^(MKNetworkOperation* completedOperation) {
        
        NSString *xmlString = [completedOperation responseString];
        
        DLog(@"%@", xmlString);
        completionBlock(xmlString);
    }
                errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
                    
                    errorBlock(error);
                }];
    
    [self enqueueOperation:op];
    
    
    return op;
}






-(MKNetworkOperation*)webService_getProdDetail:(void(^)(id parameters, BOOL cacheResponse))completionBlock
                                       onError:(MKNKErrorBlock) errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:[NSString stringWithFormat:@"%@wcs/resources/store/10001/productview/byId/10002", k_url]
                                              params:nil
                                          httpMethod:@"GET"];
    DLog(@"in getissuelist");

    [op addCompletionHandler:^(MKNetworkOperation *completedOperation)
    {
        DLog(@"%@", [completedOperation responseString]);
    }
                errorHandler:^(MKNetworkOperation *completedOperation, NSError *error)
    {
        DLog(@"%@", [error localizedDescription]);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

-(MKNetworkOperation*) method_getTopCategories:(NSString*) id
completionHandler:(IDBlock) completionBlock
errorHandler:(MKNKErrorBlock) errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:[NSString stringWithFormat:@"wcs/resources/store/10001/categoryview/@top"]
                                              params:nil
                                          httpMethod:@"GET"];
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation)
     {
         //DLog(@"%@", [completedOperation responseString]);
         completionBlock([completedOperation responseJSON]);
     }
                errorHandler:^(MKNetworkOperation *completedOperation, NSError *error)
     {
         DLog(@"%@", [error localizedDescription]);
     }];
    
    [self enqueueOperation:op];
    
    return op;
}

-(MKNetworkOperation*) method_getProductsBySearch:(NSString*)searchTerm
completionHandler:(IDBlock) completionBlock
errorHandler:(MKNKErrorBlock) errorBlock
{
    NSString *strUrl = [NSString stringWithFormat:@"wcs/resources/store/10001/productview/bySearchTerm/%@",searchTerm];
    strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    DLog(@"%@",strUrl);
    MKNetworkOperation *op = [self operationWithPath:strUrl
                                              params:nil
                                          httpMethod:@"GET"];
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation)
     {
         completionBlock([completedOperation responseJSON]);
     }
                errorHandler:^(MKNetworkOperation *completedOperation, NSError *error)
     {
         DLog(@"%@", [error localizedDescription]);
         errorBlock(error);
     }];
    
    [self enqueueOperation:op];
    
    return op;
}

-(MKNetworkOperation*) method_getSubCategories:(NSString*)categoryId
completionHandler:(IDBlock) completionBlock
errorHandler:(MKNKErrorBlock) errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:[NSString stringWithFormat:@"wcs/resources/store/10001/categoryview/byParentCategory/%@",categoryId]
                                              params:nil
                                          httpMethod:@"GET"];
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation)
     {
         completionBlock([completedOperation responseJSON]);
     }
                errorHandler:^(MKNetworkOperation *completedOperation, NSError *error)
     {
         DLog(@"%@", [error localizedDescription]);
         errorBlock(error);
     }];
    
    [self enqueueOperation:op];
    
    return op;
}
-(MKNetworkOperation*) method_getProductsByCatgeory:(NSString*)categoryId
                             completionHandler:(IDBlock) completionBlock
                                  errorHandler:(MKNKErrorBlock) errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:[NSString stringWithFormat:@"wcs/resources/store/10001/productview/byCategory/%@",categoryId]
                                              params:nil
                                          httpMethod:@"GET"];
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation)
     {
         completionBlock([completedOperation responseJSON]);
     }
                errorHandler:^(MKNetworkOperation *completedOperation, NSError *error)
     {
         DLog(@"%@", [error localizedDescription]);
         errorBlock(error);
     }];
    
    [self enqueueOperation:op];
    
    return op;
}

//#pragma mark - method_getProductDetailByUniqueId
//-(void)method_getProductDetailByUniqueId:(id)id_
//{
//    DLog(@"id: %@", id_);
//    
//    //    7794 shoe
//    //    6565 orange jacket
//    //    6986 wrist watch
//    //    78954 black jacket
//    
//    //    if ([id_ isEqualToString:@"6565"])                      //orange jacket
//    //    {
//    //        [self method_fetchData:@"10002"];
//    //    }
//    //    else
//    //    {
//    //        [[[UIAlertView alloc] initWithTitle:nil
//    //                                    message:@"Product doesn't exists in store"
//    //                                   delegate:nil
//    //                          cancelButtonTitle:@"Ok"
//    //                          otherButtonTitles:nil, nil]
//    //         show];
//    //    }
//    
//    
//    //    [self method_fetchData:@"15955"];
//    
//    
//    if ([id_ isEqualToString:@"7794"])//shoe
//    {
//        [self method_getProductDetail:@"10003"];
//    }
//    else if ([id_ isEqualToString:@"6565"])//orange jacket
//    {
//        [self method_getProductDetail:@"10002"];
//    }
//    else if ([id_ isEqualToString:@"6986"])//wrist watch
//    {
//        [self method_getProductDetail:@"10004"];
//    }
//    else if ([id_ isEqualToString:@"78954"])//black jacket
//    {
//        [self method_getProductDetail:@"10005"];
//    }
//    else if ([id_ isEqualToString:@"4325"])//Bed
//    {
//        [self method_getProductDetail:@"10006"];
//    }
//    
//    
//    
//    //////////////////////////              for bed room
//    
//    //    Beautiful Double Bed
//    //    15951
//    //
//    //    Light Ball
//    //    15952
//    //
//    //    Artframe
//    //    15957
//    //
//    //    Wall Tree
//    //    15959
//    //
//    //    White Flower Vase
//    //    15955
//    
//    
//    
//    else if ([id_ isEqualToString:@"15951"] || [id_ isEqualToString:@"15952"] || [id_ isEqualToString:@"15955"] || [id_ isEqualToString:@"15957"] || [id_ isEqualToString:@"15959"])
//    {
//        [self method_getProductDetail:id_];
//    }
//    else
//    {
//        //        [[[UIAlertView alloc] initWithTitle:nil
//        //                                    message:@"Product doesn't exists in store"
//        //                                   delegate:nil
//        //                          cancelButtonTitle:@"Ok"
//        //                          otherButtonTitles:nil, nil]
//        //         show];
//        
//        //for coppel
//        [self method_getProductDetail:id_];
//    }
//}




//-(MKNetworkOperation*) method_getProduct:(NSString*) path
//                       completionHandler:(IDBlock) completionBlock
//                            errorHandler:(MKNKErrorBlock) errorBlock
//{
//    MKNetworkOperation *op = [self operationWithPath:[NSString stringWithFormat:@"wcs/resources/store/10001/productview/byId/13209"]
//                                              params:nil
//                                          httpMethod:@"GET"];
//    
//    
//    
//    // setFreezable uploads your images after connection is restored!
//    [op setFreezable:YES];
//    
//    [op addCompletionHandler:^(MKNetworkOperation* completedOperation)
//     {
//         JSONParser *obj = [[JSONParser alloc] initWithJSON:[completedOperation responseJSON]];
//         
//         //        completionBlock([completedOperation responseJSON]);
//     }
//                errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
//                    
//                    errorBlock(error);
//                }];
//    
//    [self enqueueOperation:op];
//    
//    
//    return op;
//}




#pragma mark - method_getProductDetail
-(MKNetworkOperation*) method_getProductDetail:(NSString*)id_
                                  completionHandler:(IDBlock) completionBlock
                                       errorHandler:(MKNKErrorBlock) errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:[NSString stringWithFormat:@"wcs/resources/store/10001/productview/byId/%@",id_]
                                              params:nil
                                          httpMethod:@"GET"];
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation)
     {
         JSONParser *obj = [[JSONParser alloc] init];
         
         completionBlock([obj method_parseJSON:[completedOperation responseJSON]]);
     }
                errorHandler:^(MKNetworkOperation *completedOperation, NSError *error)
     {
         DLog(@"%@", [error localizedDescription]);
         errorBlock(error);
     }];
    [self enqueueOperation:op];

    return op;
}


-(MKNetworkOperation*) method_autocompleteAddress:(NSString*)input
                                completionHandler:(IDBlock) completionBlock
                                     errorHandler:(MKNKErrorBlock) errorBlock
{
    NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=geocode&key=AIzaSyBvvEaJQjZB83UqmTduXI-F6jMk75QL9OE",input];
    strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    MKNetworkOperation *op = [self operationWithURLString:strUrl params:nil httpMethod:@"GET"];
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation)
     {
         completionBlock([completedOperation responseJSON]);
     }
                errorHandler:^(MKNetworkOperation *completedOperation, NSError *error)
     {
         DLog(@"%@", [error localizedDescription]);
         errorBlock(error);
     }];
    [self enqueueOperation:op];
    
    return op;

}



-(MKNetworkOperation*) method_getZipcodeViaLocation:(NSString*)input
                                completionHandler:(IDBlock) completionBlock
                                     errorHandler:(MKNKErrorBlock) errorBlock
{
    NSString *strUrl = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?address=%@",input];
    strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    MKNetworkOperation *op = [self operationWithURLString:strUrl params:nil httpMethod:@"GET"];
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation)
     {
         completionBlock([completedOperation responseJSON]);
     }
                errorHandler:^(MKNetworkOperation *completedOperation, NSError *error)
     {
         DLog(@"%@", [error localizedDescription]);
         errorBlock(error);
     }];
    [self enqueueOperation:op];
    
    return op;
    
}


//method_fetchLocationByZipcode_Latitude
-(MKNetworkOperation*) method_fetchLocationByZipcode_Latitude:(NSString *)lat Longitude:(NSString *)lon
                                  completionHandler:(IDBlock) completionBlock
                                       errorHandler:(MKNKErrorBlock) errorBlock
{
    MKNetworkOperation *op = [self operationWithPath:[NSString stringWithFormat:@"wcs/resources/store/10001/storelocator/latitude/%@/longitude/%@?maxItems=&siteLevelStoreSearch=false&responseFormat=json",lat,lon]
                                              params:nil
                                          httpMethod:@"GET"];
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation)
     {
         completionBlock([completedOperation responseJSON]);
     }
                errorHandler:^(MKNetworkOperation *completedOperation, NSError *error)
     {
         DLog(@"%@", [error localizedDescription]);
         errorBlock(error);
     }];
    [self enqueueOperation:op];
    
    return op;
}



//method_fetchLocation
-(MKNetworkOperation*) method_fetchLocationByCityStateNRadius:(NSString *)id_ City:(NSString *)city State:(NSString *)state Miles:(NSString *)miles
                                            completionHandler:(IDBlock) completionBlock
                                                 errorHandler:(MKNKErrorBlock) errorBlock
{
    NSString *strUrl;
    if ([state length])
    {
        strUrl = [NSString stringWithFormat:@"wcs/resources/store/10001/storelocator/byLocation?city=%@&state=%@&siteLevelStoreSearch=false&radius=%@",city,state,miles];
    }
    else
    {
        strUrl = [NSString stringWithFormat:@"wcs/resources/store/10001/storelocator/byLocation?city=%@&siteLevelStoreSearch=false&radius=%@",city,miles];
    }
    DLog(@"strUrl:%@",strUrl);
    strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    MKNetworkOperation *op = [self operationWithPath:strUrl
                                              params:nil
                                          httpMethod:@"GET"];
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation)
     {
         completionBlock([completedOperation responseJSON]);
     }
                errorHandler:^(MKNetworkOperation *completedOperation, NSError *error)
     {
         DLog(@"%@", [error localizedDescription]);
         errorBlock(error);
     }];
    [self enqueueOperation:op];
    
    return op;
}

-(MKNetworkOperation*) method_editShoppingCart:(NSDictionary *)dic
                             completionHandler:(IDBlock) completionBlock
                                  errorHandler:(MKNKErrorBlock) errorBlock
{
    NSString *strUrl = [NSString stringWithFormat:@"%@wcs/resources/store/10001/cart/@self",k_url];
    
    DLog(@"strUrl:%@",strUrl);
    strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    MKNetworkOperation *op = [self operationWithURLString:strUrl params:dic httpMethod:@"PUT"];
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation)
     {
         completionBlock([completedOperation responseJSON]);
     }
                errorHandler:^(MKNetworkOperation *completedOperation, NSError *error)
     {
         DLog(@"%@", [error localizedDescription]);
         errorBlock(error);
     }];
    [self enqueueOperation:op];
    
    return op;

}
-(MKNetworkOperation*) method_removeShoppingCart:(NSDictionary *)dic
                             completionHandler:(IDBlock) completionBlock
                                  errorHandler:(MKNKErrorBlock) errorBlock
{
    NSString *strUrl = [NSString stringWithFormat:@"%@wcs/resources/store/10001/cart/@self/delete_order_item",k_url];
    
    DLog(@"strUrl:%@",strUrl);
    strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    MKNetworkOperation *op = [self operationWithURLString:strUrl params:dic httpMethod:@"POST"];
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation)
     {
         completionBlock([completedOperation responseJSON]);
     }
                errorHandler:^(MKNetworkOperation *completedOperation, NSError *error)
     {
         DLog(@"%@", [error localizedDescription]);
         errorBlock(error);
     }];
    [self enqueueOperation:op];
    
    return op;
    
}
-(MKNetworkOperation*) method_guestLogin:(NSDictionary *)dic
                       completionHandler:(IDBlock) completionBlock
                            errorHandler:(MKNKErrorBlock) errorBlock
{
    NSString *strUrl = [NSString stringWithFormat:@"%@wcs/resources/store/10001/guestidentity",k_url];
//NSString *strUrl = [NSString stringWithFormat:@"%@/wcs/resources/store/10001/loginidentity",k_url];
    DLog(@"strUrl:%@",strUrl);
    DLog(@"%@",dic);
    strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    MKNetworkOperation *op = [self operationWithURLString:strUrl params:dic httpMethod:@"POST"];
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation)
     {
         completionBlock([completedOperation responseJSON]);
     }
                errorHandler:^(MKNetworkOperation *completedOperation, NSError *error)
     {
         DLog(@"%@", [error localizedDescription]);
         errorBlock(error);
     }];
    [self enqueueOperation:op];
    
    return op;
}

-(MKNetworkOperation*) method_solarSearch:(id)text_
                        completionHandler:(IDBlock) completionBlock
                             errorHandler:(MKNKErrorBlock) errorBlock
{
    NSString *strUrl = [NSString stringWithFormat:@"http://arandell.royalcyber.com:3737/search/resources/store/10001/sitecontent/keywordSuggestionsByTerm/%@", text_];

    DLog(@"strUrl:%@",strUrl);
    strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    MKNetworkOperation *op = [self operationWithURLString:strUrl params:nil httpMethod:@"GET"];
    
    
    [op setHeader:@"Accept" withValue:@"application/json"];
    [op setHeader:@"Content-Type" withValue:@"application/json"];
//    [op setHeader:@"WCToken" withValue:ApplicationDelegate.single.string_WCToken];
//    [op setHeader:@"WCTrustedToken" withValue:ApplicationDelegate.single.string_WCTrustedToken];
    
    
    DLog(@"op: %@", op);
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation)
     {
         completionBlock([completedOperation responseJSON]);
     }
                errorHandler:^(MKNetworkOperation *completedOperation, NSError *error)
     {
         DLog(@"%@", [error localizedDescription]);
         errorBlock(error);
     }];
    [self enqueueOperation:op];
    
    return op;
}

@end