#import "scroller_home.h"

#define k_time 5.0
#define k_imageName @"homeBG"
#define k_imageNameForiPhone5 @"homeBG_iPhone5"
#define k_numberOfPage 3



@implementation scroller_home

@synthesize delegate_ = _delegate_;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if(self = [super initWithCoder:aDecoder])
    {
        [self performSelector:@selector(method_setupScroller) withObject:nil afterDelay:0.2];
    }
    
    return self;
}


#pragma mark - setup scroller
-(void)method_setupScroller
{
    self.delegate = self;
    
    float float_x = 0.0;
    
    for (unsigned i = 0; i<k_numberOfPage; i++)
    {
        UIView *view_temp = [[UIView alloc] initWithFrame:CGRectMake(float_x,
                                                                     0,
                                                                     self.frame.size.width,
                                                                     self.frame.size.height)];
        
        
        UIImageView *imageView_temp = [[UIImageView alloc] initWithFrame:CGRectMake(0,
                                                                                    0,
                                                                                    view_temp.frame.size.width,
                                                                                    view_temp.frame.size.height)];
//        [imageView_temp setContentMode:UIViewContentModeScaleAspectFit];
        
//        if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS)
//        {
//            DLog(@"%@",[NSString stringWithFormat:@"%@%d", k_imageNameForiPhone5, i+1]);
//            [imageView_temp setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@%d", k_imageNameForiPhone5, i+1]]];
//        }
//        else
//        {
            [imageView_temp setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@%d", k_imageName, i+1]]];
       // }
        [view_temp addSubview:imageView_temp];
        
        UIButton *button_ = [UIButton buttonWithType:UIButtonTypeCustom];
        
        //[button_ setBackgroundImage:[UIImage imageNamed:@"front-btn"] forState:UIControlStateNormal];
        
//        [button_ setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.4]];
//        button_.layer.cornerRadius = k_cornerRadiusButton;
//        button_.layer.borderWidth = 2.0;
//        [button_.layer setMasksToBounds:YES]
    

        
        if (IS_IPHONE_4_OR_LESS)
        {
            [button_ setFrame:CGRectMake(10, 210, 155, 40)];
        }
        else if (IS_IPHONE_5)
        {
            [button_ setFrame:CGRectMake(10, 210, 155, 50)];
        }
        else if (IS_IPHONE_6P)
        {
            [button_ setFrame:CGRectMake(10, 280, 155, 50)];
        }
        else
        {
            [button_ setFrame:CGRectMake(10, 255, 155, 50)];
        }
        
        
        
        [button_ setTintAdjustmentMode:UIViewTintAdjustmentModeDimmed];
        [button_ setReversesTitleShadowWhenHighlighted:YES];
        button_.titleLabel.adjustsFontSizeToFitWidth = YES;
        [button_ setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

        
        switch (i)
        {
            case 1:
                //[button_ setTitle:@"Catalogue" forState:UIControlStateNormal];
                [button_ setBackgroundImage:[UIImage imageNamed:@"see-catalog-btn"] forState:UIControlStateNormal];
                [button_ addTarget:self action:@selector(method_catalog) forControlEvents:UIControlEventTouchUpInside];
                [view_temp addSubview:button_];
                
                break;
            case 2:
                //[button_ setTitle:@"QR Code" forState:UIControlStateNormal];
                [button_ setBackgroundImage:[UIImage imageNamed:@"scan-qrcode-btn"] forState:UIControlStateNormal];
                [button_ addTarget:self action:@selector(method_barCode) forControlEvents:UIControlEventTouchUpInside];
                [view_temp addSubview:button_];
                break;
            case 3:
                [button_ setTitle:@"Store Locator" forState:UIControlStateNormal];
                //                [button_ setBackgroundImage:[UIImage imageNamed:@"hmLocateStore"] forState:UIControlStateNormal];
                [button_ addTarget:self action:@selector(method_locateStore) forControlEvents:UIControlEventTouchUpInside];
                [view_temp addSubview:button_];
                break;
            case 4:
                [button_ setTitle:@"Loyalty Points" forState:UIControlStateNormal];
                //                [button_ setBackgroundImage:[UIImage imageNamed:@"hmloypoints"] forState:UIControlStateNormal];
                [button_ addTarget:self action:@selector(method_loyalityPoints) forControlEvents:UIControlEventTouchUpInside];
                [view_temp addSubview:button_];
                
                break;
                
            default:
                break;
        }
        
        
        
        
        
        
        
        
        [self addSubview:view_temp];
        
        float_x += view_temp.frame.size.width;
        
        
    }
    
    



    UIView *view_upperLayer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, float_x, self.frame.size.height)];
    [view_upperLayer setBackgroundColor:[UIColor blackColor]];
    view_upperLayer.alpha = 0.0;
    view_upperLayer.tag = 111;

    [self addSubview: view_upperLayer];
    
    self.bounces = NO;
    [self setContentSize:CGSizeMake(float_x, 0)];
    
    [self method_setupTimer];
}

#pragma mark - Timer
-(void)method_setupTimer
{
    if (![timer_ isValid])
    {
        timer_ = [NSTimer scheduledTimerWithTimeInterval:k_time
                                                  target:self
                                                selector:@selector(method_scrollToNext:)
                                                userInfo:nil
                                                 repeats:YES];
    }
}


-(void)method_cancelTimer
{
    if ([timer_ isValid])
    {
        [timer_ invalidate];
        timer_ = nil;
    }
}


#pragma mark - view alpha variation
-(void)method_setViewColor:(float)alpha_
{
    UIView *view_upperLayer = [self viewWithTag:111];
    view_upperLayer.alpha = alpha_;
    
    
//    view_upperLayer.backgroundColor = [color_ colorWithAlphaComponent:0.6];
}



#pragma mark - scroller delegates and methods
-(void)method_scrollToNext:(NSTimer *)timer
{
    [self method_setViewColor:0.6];
    
    CGFloat width = self.frame.size.width;
    NSInteger page = (self.contentOffset.x + (0.5f * width)) / width;
    
    CGRect frame = self.frame;
    frame.origin.x = frame.size.width * page+1;
    frame.origin.y = 0;

    if (page == 0 || (page <= k_numberOfPage-2))
    {
        page = page + 1;
    }
    else if(page == k_numberOfPage-1)
    {
        page = 0;
    }
    
    [self setContentOffset:CGPointMake(self.frame.size.width*page, 0) animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView                                               // any offset changes
{
//    [self method_cancelTimer];
}



// called on start of dragging (may require some time and or distance to move)
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    DLog(@"scrollViewWillBeginDragging");
    
    
    [self method_cancelTimer];
    
    [self method_setViewColor:0.0];
}

// called on finger up if the user dragged. velocity is in points/millisecond. targetContentOffset may be changed to adjust where the scroll view comes to rest

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset NS_AVAILABLE_IOS(5_0)
{
    DLog(@"scrollViewWillEndDragging");
    
    [self method_setupTimer];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    DLog(@"scrollViewDidEndDragging");
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView   // called on finger up as we are moving
{
    DLog(@"scrollViewWillBeginDecelerating");
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView      // called when scroll view grinds to a halt
{
    DLog(@"scrollViewDidEndDecelerating");
}


- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView // called when setContentOffset/scrollRectVisible:animated: finishes. not called if not animating
{
    DLog(@"scrollViewDidEndScrollingAnimation");
    
    [self method_setViewColor:0.0];
}



#pragma mark - scroller start stop
-(void)method_startScroller
{
    self.delegate = self;
    [self method_setupTimer];
}

-(void)method_stopScroller
{
    self.delegate = nil;
    [self method_cancelTimer];
}



#pragma mark - buttons methods
-(void)method_catalog
{
    if ([self.delegate_ respondsToSelector:@selector(method_catalog)])
    {
        [self.delegate_ method_catalog];
    }
}


-(void)method_barCode
{
    if ([self.delegate_ respondsToSelector:@selector(method_barcode)])
    {
        [self.delegate_ method_barcode];
    }
}


-(void)method_locateStore
{
    if ([self.delegate_ respondsToSelector:@selector(method_locateStore)])
    {
        [self.delegate_ method_locateStore];
    }
}


-(void)method_loyalityPoints
{
    if ([self.delegate_ respondsToSelector:@selector(method_loyalityPoints)])
    {
        [self.delegate_ method_loyalityPoints];
    }
}


////////////////////////////////////////////////////////////////////////            Following methods call when user drag

//-[scroller_home scrollViewWillBeginDragging:]
//-[scroller_home scrollViewWillEndDragging:withVelocity:targetContentOffset:]


////////////////////////////////////////////////////////////////////////            Following methods call Automaticall drag

//scrollViewDidEndScrollingAnimation

@end