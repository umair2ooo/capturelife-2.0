#import <Foundation/Foundation.h>

@protocol NSURLResponseDelegate <NSObject>

@required
-(void)delegate_outPut:(NSData *)data urlRes:(NSURLResponse *)urlRes error_:(NSError *)error_ seriveName:(NSString *)serivename;

@end
@interface webService_Post_Put_Delete : NSObject
{
}
@property(nonatomic, strong)id<NSURLResponseDelegate> delegate;

-(id)initWithURL:(NSString *)url_ data:(NSDictionary *)dic seriveType:(NSString *)serivetype seriveName:(NSString *)serivename;

@end