//
//  cell_Cart.h
//  CaptureLife
//
//  Created by Fahim Bilwani on 4/27/15.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

@protocol DetailObject <NSObject>

-(void)method_details:(int)tag;
-(void)method_deleteObject:(int)tag;
-(void)method_editObject:(int)tag;

@end

#import <UIKit/UIKit.h>

@interface cell_Cart : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label_name;
@property (weak, nonatomic) IBOutlet UILabel *lable_Price;
@property (weak, nonatomic) IBOutlet UILabel *label_TotalPrice;
@property (weak, nonatomic) IBOutlet UILabel *label_Quantity;
@property (weak, nonatomic) IBOutlet UIImageView *image_product;
@property (weak, nonatomic) IBOutlet UIButton *button_details;
@property (weak, nonatomic) IBOutlet UIButton *button_Edit;
@property (weak, nonatomic) IBOutlet UIButton *button_Remove;

@property (nonatomic)id <DetailObject> delegate;

- (IBAction)action_details:(id)sender;
- (IBAction)action_Edit:(id)sender;
- (IBAction)action_Remove:(id)sender;



-(void)method_setValues:(NSDictionary *)dic;
@end
