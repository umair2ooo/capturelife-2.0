#import "JSONParser.h"


@implementation JSONParser

- (id)initWithJSON:(id)json
{
    self = [super init];
    if (self)
    {
//        [self method_parseJSON:json];
    }
    return self;
}


-(NSDictionary *)method_parseJSON:(id)json
{
    DLog(@"%@", json);
    

    NSMutableArray *array_independentObjects = [[NSMutableArray alloc] init];
    
    if ([[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_hasSingleSKU])
    {
        
//////////////////////////////////////////////////////////////////////////////// it means multiple selection of this product exists
        
        if ([[[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_hasSingleSKU] boolValue] == false ||
            [[[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_hasSingleSKU] boolValue] == true)
        {

//////////////////////////////////////////////////////////////////////////////// Get all the independent attributes via below code
            
            __block NSArray *array_temp = [[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_Attributes];
            NSArray *array_attributes = [[NSArray alloc] initWithArray:array_temp copyItems:YES];
            array_temp = nil;
            
            
            [array_attributes enumerateObjectsUsingBlock:^(id attribute, NSUInteger idxOne, BOOL *stop) {
                
                if ([[attribute valueForKey:k_usage] isEqualToString:k_Defining])         // it means, this attribute exist on UI
                {
                    NSArray *array_Values = [attribute valueForKey:k_Values];
                    
                    [array_Values enumerateObjectsUsingBlock:^(id Value, NSUInteger idxTwo, BOOL *stop) {
                        
                        
                        NSDictionary *dic_temp = [NSDictionary dictionaryWithObjectsAndKeys:
                                                  
                                                  [attribute valueForKey:k_name], k_name,
                                                  [NSString stringWithFormat:@"%lu", (unsigned long)idxOne], k_index,
                                                  [Value valueForKey:k_values],     @"value",
                                                  [Value valueForKey:k_uniqueID],   k_uniqueID, nil];
                        
                        [array_independentObjects addObject:dic_temp];
                        
                        dic_temp = nil;
                    }];
                }
            }];
            
            
            DLog(@"%@", array_independentObjects);
            
//////////////////////////////////////////////////////////////////////////////// Got all the independent attributes via above code
            
            
            
            
//////////////////////////////////////////////////////////////////////////////// Get all the Dependent attributes via below code
            
            NSMutableArray *array_dependentObjects = [[NSMutableArray alloc] init];
            
            
            [array_independentObjects enumerateObjectsUsingBlock:^(id independentObject, NSUInteger idx_independent, BOOL *stop) {
    
                
                
                
                
                array_temp = [[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_SKUs];
                NSArray *array_SKUs = [[NSArray alloc] initWithArray:array_temp copyItems:YES];
                array_temp = nil;
                
                
                
                
                [array_SKUs enumerateObjectsUsingBlock:^(id SKU, NSUInteger idx_dependent, BOOL *stop) {
                    
                    
                    NSArray *array_SKU_Attributes = [SKU valueForKey:k_Attributes];
                    
                    
                    
                    
        
                    
                    
                    
                    int int_indexOfindependentObject = [[[array_independentObjects objectAtIndex:idx_independent] valueForKey:k_index] intValue];
                    
                    
                    DLog(@"%d", [[[array_independentObjects objectAtIndex:idx_independent] valueForKey:k_index] intValue]);
                    DLog(@"%lu", [array_SKU_Attributes count]-1-int_indexOfindependentObject);
                    
                    
                    
                    //                    NSDictionary *dic_SKU_Attributes_Values = [array_SKU_Attributes lastObject];
                    
                    NSDictionary *dic_SKU_Attributes_Values = [array_SKU_Attributes objectAtIndex:[array_SKU_Attributes count]-1-int_indexOfindependentObject];
                
                    

                    DLog(@"%@", [independentObject valueForKey:k_uniqueID]);
                    DLog(@"%@", [[[dic_SKU_Attributes_Values valueForKey:k_Values] objectAtIndex:0] valueForKey:k_uniqueID]);
                    
                    
                    
                    
///////////////////////////////////////////////////////////////////////////// Below is the comparison of dependent and independent Object ID
                    
                    if ([[independentObject valueForKey:k_uniqueID] isEqualToString:[[[dic_SKU_Attributes_Values valueForKey:k_Values] objectAtIndex:0] valueForKey:k_uniqueID]])
                    {
                        DLog(@"%@", [NSDictionary dictionaryWithObjectsAndKeys:
                                     
                                     [SKU valueForKey:k_SKUUniqueID],k_SKUUniqueID,
                                     [[[SKU valueForKey:k_Price] objectAtIndex:0] valueForKey:k_SKUPriceValue], k_SKUPriceValue, nil]);
                        

                        NSMutableArray *array_holdSubValues = [[NSMutableArray alloc] init];
                        
                        
////////////////////////////////////////////////////////////////////////////        following loop will iterate upto the second last index
                        for (unsigned i = 0; i<[array_SKU_Attributes count]-1 ; i++)
                        {
                            [array_holdSubValues addObject:[array_SKU_Attributes objectAtIndex:i]];
                        }
                        
                        
                        [array_dependentObjects addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                                           
                                                           array_holdSubValues, k_Values,
                                                           [SKU valueForKey:k_SKUUniqueID],k_SKUUniqueID,
                                                           [[[SKU valueForKey:k_Price] objectAtIndex:0] valueForKey:k_SKUPriceValue], k_SKUPriceValue, nil]];
                    }
                    
                    
                    
                    
                }];
                
                
                DLog(@"%@", array_dependentObjects);
                
                
                
                NSMutableDictionary *dic_temp = [NSMutableDictionary dictionaryWithDictionary:independentObject];
                
                
                [dic_temp setObject:[NSArray arrayWithArray:array_dependentObjects] forKey:k_Values];
                
                
                [array_independentObjects replaceObjectAtIndex:idx_independent withObject:[NSDictionary dictionaryWithDictionary:dic_temp]];
                
                dic_temp = nil;
                
                [array_dependentObjects removeAllObjects];
            }];
            
//////////////////////////////////////////////////////////////////////////////// Got all the Dependent attributes via above code
            

            
            NSMutableArray *array_dataModel = [[NSMutableArray alloc] initWithCapacity:[array_independentObjects count]];
            
            [array_independentObjects enumerateObjectsUsingBlock:^(id objOne, NSUInteger idx, BOOL *stop) {
                
                
                
                
                
                NSMutableArray *array_tempTwo = [[NSMutableArray alloc] initWithCapacity:[[objOne valueForKey:k_Values] count]];
                
                [[objOne valueForKey:k_Values] enumerateObjectsUsingBlock:^(id objTwo, NSUInteger idx, BOOL *stop) {
                    
                    
                    
                    
                    
                    
                    NSMutableArray *array_tempOne = [[NSMutableArray alloc] initWithCapacity:[[objTwo valueForKey:k_Values] count]];
                    
                    [[objTwo valueForKey:k_Values] enumerateObjectsUsingBlock:^(id objThree, NSUInteger idx, BOOL *stop) {
                        
                        
                        
                        dataModel_SKUsLevelOne *dataModel_obj = [[dataModel_SKUsLevelOne alloc] init];
                        
                        [dataModel_obj setValuesForKeysWithDictionary:
                         
                         [NSDictionary dictionaryWithObjectsAndKeys:
                         
                          [[[objThree valueForKey:k_Values] objectAtIndex:0] valueForKey:k_identifier],k_identifier,
                          [objThree valueForKey:k_identifier],k_identifierTwo,
                          [objThree valueForKey:k_name], k_name,
                          [objThree valueForKey:k_uniqueID], k_uniqueID,
                          [objThree valueForKey:k_usage], k_usage,
                          
                          
                          nil]];
                        
                        
                        [array_tempOne addObject:dataModel_obj];
                        
                        
                        
//                        DLog(@"%@", dataModel_obj.identifier);
//                        DLog(@"%@", dataModel_obj.identifierTwo);
//                        DLog(@"%@", dataModel_obj.name);
//                        DLog(@"%@", dataModel_obj.uniqueID);
//                        DLog(@"%@", dataModel_obj.usage);
                        
                        DLog(@"===========================================");
                    }];
                    
                    
                    
                    
                    
                    
                    dataModel_SKUsLevelTwo *leveTwo_obj = [[dataModel_SKUsLevelTwo alloc] init];
                    
                    
                    [leveTwo_obj setValuesForKeysWithDictionary:
                     [NSDictionary dictionaryWithObjectsAndKeys:
                      
                      [objTwo valueForKey:k_SKUPriceValue], k_SKUPriceValue,
                      [objTwo valueForKey:k_SKUUniqueID], k_SKUUniqueID,
                      array_tempOne, k_Values,
                      nil]];
                    
//                    DLog(@"%@", leveTwo_obj.SKUPriceValue);
//                    DLog(@"%@", leveTwo_obj.SKUUniqueID);
//                    DLog(@"%@", leveTwo_obj.Values);
                    
                    
                    [array_tempTwo addObject:leveTwo_obj];
                    
                }];
                
                
                
                
                
                dataModel_SKUsLevelThree *leveThree_obj = [[dataModel_SKUsLevelThree alloc] init];
                
                
                [leveThree_obj setValuesForKeysWithDictionary:
                 [NSDictionary dictionaryWithObjectsAndKeys:
                  
                  array_tempTwo,                k_Values,
                  [objOne valueForKey:k_index], k_index,
                  [objOne valueForKey:k_name], k_name,
                  [objOne valueForKey:k_uniqueID], k_uniqueID,
                  
                  [objOne valueForKey:@"value"], @"value",
                  
                  nil]];
                
                
                
//                DLog(@"%@", leveThree_obj.Values);
//                DLog(@"%@", leveThree_obj.index);
//                DLog(@"%@", leveThree_obj.name);
//                DLog(@"%@", leveThree_obj.uniqueID);
//                DLog(@"%@", leveThree_obj.value);
                
                
                
                
                [array_dataModel addObject:leveThree_obj];
            }];
            
            
            
            
            
            
            
            
            DLog(@"total visible independent objects: %lu", (unsigned long)[array_independentObjects count]);
            
            
            if ([array_independentObjects count])
            {
//                DLog(@"array_independentObjects: %@", [array_independentObjects objectAtIndex:0]);
                
                
                
                
//                DLog(@"%lu", (unsigned long)[array_dataModel count]);
//                
//                DLog(@"array_dataModel: %@", [array_dataModel objectAtIndex:0]);
                
                
//                dataModel_SKUsLevelThree *xyz = [array_dataModel objectAtIndex:0];
                
//                DLog(@"%@", xyz.Values);
//                DLog(@"%@", xyz.index);
//                DLog(@"%@", xyz.name);
//                DLog(@"%@", xyz.uniqueID);
//                DLog(@"%@", xyz.value);
                
                
                
                
                
//                if ([xyz.Values count])
//                {
//                    dataModel_SKUsLevelTwo *zyx = [xyz.Values objectAtIndex:0];
//                    
////                    DLog(@"%@", zyx.SKUPriceValue);
////                    DLog(@"%@", zyx.SKUUniqueID);
////                    DLog(@"%@", zyx.Values);
//                    
//                    
//                    
//                    
//                    
//                    dataModel_SKUsLevelOne *abc = [zyx.Values objectAtIndex:0];
//                    
////                    DLog(@"%@", abc.identifier);
////                    DLog(@"%@", abc.identifierTwo);
////                    DLog(@"%@", abc.name);
////                    DLog(@"%@", abc.uniqueID);
////                    DLog(@"%@", abc.usage);
//                    
//                    
//                    
//                    
//                    
////                    DLog(@"array_independentObjects: %@", [array_independentObjects objectAtIndex:0]);
//                }
                
            }
            
            

            [array_independentObjects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                
                
//                DLog(@"Object Id: %@", [obj valueForKey:k_uniqueID]);
//                DLog(@"under which section? == %@", [obj valueForKey:k_name]);
//                DLog(@"name: %@", [obj valueForKey:k_values]);
//                DLog(@"Total number of dependent objects: %lu", (unsigned long)[[obj valueForKey:k_Values] count]);
//                DLog(@"Dependent Object: %@", [obj valueForKey:k_Values]);
                
            }];
            
            
            
            
            
            
            
            

        

            NSString *string_singleSKUUniqueID;
            
            if ([[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_singleSKUUniqueID])
            {
                DLog(@"It means there are no multiple choices for this product");
                
                string_singleSKUUniqueID = [[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_singleSKUUniqueID];
            }
            else
            {
                string_singleSKUUniqueID = @"";
            }
            
            
            
            
            
            
            ApplicationDelegate.single.dic_productDetail = [NSDictionary dictionaryWithDictionary:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                   array_dataModel,
                                                                                                   k_dataModel,
                                                                                                   
                                                                                                   [self method_getProductDetail_:json],
                                                                                                   k_ProductInfo,
                                                                                                   
                                                                                                   [[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_uniqueID],
                                                                                                   k_uniqueID,
                                                                                                   
                                                                                                   [[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_name],
                                                                                                   k_name,
                                                                                                   
                                                                                                   [[[[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_Price] objectAtIndex:0] valueForKey:k_priceValue],
                                                                                                   k_priceValue,
                                                                                                   
                                                                                                   string_singleSKUUniqueID, k_singleSKUUniqueID,
                                                                                                   [[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_shortDescription],
                                                                                                   k_shortDescription,
                                                                                                   
                                                                                                   [[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_longDescription],
                                                                                                   k_longDescription,
                                                                                                   
                                                                                                   [[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_fullImage],
                                                                                                   k_fullImage,
                                                                                                   
                                                                                                   [[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_thumbnail],
                                                                                                   k_thumbnail,
                                                                                                   
                                                                                                   nil]];
            
            
            
            
            DLog(@"%@", [[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_uniqueID]);
            DLog(@"%@", [[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_name]);
            DLog(@"%@", [[[[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_Price] objectAtIndex:0] valueForKey:k_priceValue]);
            DLog(@"%@", [[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_shortDescription]);
            DLog(@"%@", [[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_longDescription]);
            DLog(@"%@", [[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_fullImage]);
            DLog(@"%@", [[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_thumbnail]);
            
            
//            DLog(@"%@", [self method_getProductDetail_:json]);
//            
//            
//            DLog(@"%@", [NSDictionary dictionaryWithObjectsAndKeys:
//                         array_dataModel,@"dataModel",
//                         [self method_getProductDetail_:json], k_ProductInfo,
//                         
//                         [[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_uniqueID], k_uniqueID,
//                         [[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_name], k_name,
//                         [[[[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_Price] objectAtIndex:0] valueForKey:k_priceValue], k_priceValue,
//                         string_singleSKUUniqueID, k_singleSKUUniqueID,
//                         [[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_shortDescription], k_shortDescription,
//                         [[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_longDescription], k_longDescription,
//                         [[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_fullImage], k_fullImage,
//                         [[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_thumbnail], k_thumbnail,
//                         nil]);


            
            return [NSDictionary dictionaryWithObjectsAndKeys:
                    array_dataModel,@"dataModel",
                    [self method_getProductDetail_:json], k_ProductInfo,
                    
                    [[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_uniqueID], k_uniqueID,
                    [[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_name], k_name,
                    [[[[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_Price] objectAtIndex:0] valueForKey:k_priceValue], k_priceValue,
                    string_singleSKUUniqueID, k_singleSKUUniqueID,
                    [[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_shortDescription], k_shortDescription,
                    [[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_longDescription], k_longDescription,
                    [[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_fullImage], k_fullImage,
                    [[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_thumbnail], k_thumbnail,
                    nil];
        }
        
//////////////////////////////////////////////////////////////////////////////// it means multiple selection of this product exists
        
    }
    
    
    DLog(@"%@", array_independentObjects);
    return nil;
}



-(NSMutableArray *)method_getProductDetail_:(id)json
{
    //////////////////////////////////////////////////////////////////////////////// Product Information
    
    __block NSArray *array_temp = [[[json valueForKey:k_CatalogEntryView] objectAtIndex:0] valueForKey:k_Attributes];
    NSArray *array_attributes = [[NSArray alloc] initWithArray:array_temp copyItems:YES];
    array_temp = nil;
    
    
    NSMutableArray *array_productDetail = [[NSMutableArray alloc] initWithCapacity:[array_attributes count]];
    
    
    [array_attributes enumerateObjectsUsingBlock:^(id attribute, NSUInteger idxOne, BOOL *stop) {
        
//        DLog(@"%@", attribute);
        [attribute valueForKey:k_name];
        
        NSArray *array_Values = [attribute valueForKey:k_Values];
        
        __block NSMutableString *str_temp = [[NSMutableString alloc] init];
        
        [array_Values enumerateObjectsUsingBlock:^(id Value, NSUInteger idxTwo, BOOL *stop) {
            
//            DLog(@"%@", Value);

            
            if ([array_Values count] == 1)
            {
                str_temp = (NSMutableString *)[str_temp stringByAppendingString:[NSString stringWithFormat:@"%@", [Value valueForKey:k_values]]];
                
                [array_productDetail addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                                [attribute valueForKey:k_name], k_name,
                                                str_temp, k_values, nil]];
                
                str_temp = nil;
                
                str_temp = [[NSMutableString alloc] init];
            }
            
            else
            {
                if (idxTwo == [array_Values count] -1)
                {
                    str_temp = (NSMutableString *)[str_temp stringByAppendingString:[NSString stringWithFormat:@" and %@", [Value valueForKey:k_values]]];
                    
                    
                    
                    
                    [array_productDetail addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                                    [attribute valueForKey:k_name], k_name,
                                                    str_temp, k_values, nil]];
                    
                    str_temp = nil;
                    
                    str_temp = [[NSMutableString alloc] init];
                    
                    
                }
                else if (idxTwo == 0)
                {
                    str_temp = (NSMutableString *)[str_temp stringByAppendingString:[NSString stringWithFormat:@"%@", [Value valueForKey:k_values]]];
                }
                else
                {
                    str_temp = (NSMutableString *)[str_temp stringByAppendingString:[NSString stringWithFormat:@", %@", [Value valueForKey:k_values]]];
                }
                
//                DLog(@"%@", str_temp);
            }
        }];
    }];

    
    
    NSLog(@"%@", array_productDetail);
    
    return array_productDetail;

    
    //////////////////////////////////////////////////////////////////////////////// Product Information
}

@end