
#import <MKNetworkKit/MKNetworkKit.h>

typedef void (^IDBlock)(id object);

@interface WebServiceEngine : MKNetworkEngine


-(id) initWithDefaultSettings;
-(void) basicAuthTest;
-(void) digestAuthTest;

-(MKNetworkOperation*) downloadFatAssFileFrom:(NSString*) remoteURL toFile:(NSString*) filePath;

-(MKNetworkOperation*) uploadImageFromFile:(NSString*) file
                         completionHandler:(IDBlock) completionBlock
                              errorHandler:(MKNKErrorBlock) errorBlock;



//-(MKNetworkOperation*) method_getProduct:(NSString*) path
//                         completionHandler:(IDBlock) completionBlock
//                              errorHandler:(MKNKErrorBlock) errorBlock;


-(MKNetworkOperation*) method_getTopCategories:(NSString*) id
completionHandler:(IDBlock) completionBlock
errorHandler:(MKNKErrorBlock) errorBlock;

-(MKNetworkOperation*) method_getSubCategories:(NSString*) id
completionHandler:(IDBlock) completionBlock
errorHandler:(MKNKErrorBlock) errorBlock;

-(MKNetworkOperation*) method_getProductsBySearch:(NSString*) id
completionHandler:(IDBlock) completionBlock
errorHandler:(MKNKErrorBlock) errorBlock;

-(MKNetworkOperation*) method_getProductsByCatgeory:(NSString*)categoryId
                                  completionHandler:(IDBlock) completionBlock
                                       errorHandler:(MKNKErrorBlock) errorBlock;


-(MKNetworkOperation*)webService_getProdDetail:(void(^)(id parameters, BOOL cacheResponse))completionBlock
                           onError:(MKNKErrorBlock) errorBlock;

-(MKNetworkOperation*) method_getProductDetail:(NSString*)id_
completionHandler:(IDBlock) completionBlock
                                  errorHandler:(MKNKErrorBlock) errorBlock;

-(MKNetworkOperation*) method_autocompleteAddress:(NSString*)input
                             completionHandler:(IDBlock) completionBlock
                                  errorHandler:(MKNKErrorBlock) errorBlock;

-(MKNetworkOperation*) method_getZipcodeViaLocation:(NSString*)input
completionHandler:(IDBlock) completionBlock
                                       errorHandler:(MKNKErrorBlock) errorBlock;

-(MKNetworkOperation*) method_fetchLocationByZipcode_Latitude:(NSString *)lat Longitude:(NSString *)lon
completionHandler:(IDBlock) completionBlock
                                                 errorHandler:(MKNKErrorBlock) errorBlock;

-(MKNetworkOperation*) method_fetchLocationByCityStateNRadius:(NSString *)id_ City:(NSString *)city State:(NSString *)state Miles:(NSString *)miles
completionHandler:(IDBlock) completionBlock
                                                 errorHandler:(MKNKErrorBlock) errorBlock;

-(MKNetworkOperation*) method_editShoppingCart:(NSDictionary *)dic
                             completionHandler:(IDBlock) completionBlock
                                  errorHandler:(MKNKErrorBlock) errorBlock;

-(MKNetworkOperation*) method_removeShoppingCart:(NSDictionary *)dic
completionHandler:(IDBlock) completionBlock
                                    errorHandler:(MKNKErrorBlock) errorBlock;

-(MKNetworkOperation*) method_guestLogin:(NSDictionary *)dic
completionHandler:(IDBlock) completionBlock
                                    errorHandler:(MKNKErrorBlock) errorBlock;

-(MKNetworkOperation*) method_solarSearch:(id)text_
                      completionHandler:(IDBlock) completionBlock
                           errorHandler:(MKNKErrorBlock) errorBlock;

@end