#import <UIKit/UIKit.h>

@protocol homeScrollerProtocolDelegate <NSObject>

-(void)method_catalog;
-(void)method_barcode;
-(void)method_locateStore;
-(void)method_loyalityPoints;

@end

@interface scroller_home : UIScrollView<UIScrollViewDelegate>
{
    NSTimer *timer_;
}

@property(nonatomic, strong)id <homeScrollerProtocolDelegate> delegate_;

-(void)method_startScroller;
-(void)method_stopScroller;

@end