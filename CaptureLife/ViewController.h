#import <UIKit/UIKit.h>

#import "DMSDelegateProtocol.h"
#import "DMResolver.h"
#import "PayloadToPayoffResolver.h"
#import "DMSAudioMicAQSource.h"
#import "DMSImageCameraSource.h"


@class DMSImageCameraSource;
@class DMSAudioMicAQSource;

@interface warningItem : NSObject
@property NSDate* date;
@property NSString* source;
@property NSString* message;
@end



@interface ViewController : UIViewController<
                                    UIAlertViewDelegate,
                                    DMSDelegateProtocol,
                                    PayloadToPayoffResolverDelegate>
{
}

// DMS Resolver & PayoffsHistory
@property (nonatomic, strong) PayloadToPayoffResolver* payloadToPayoffResolver;


@property (nonatomic, strong) DMSManager* dmsManager;
@property (nonatomic, strong) DMResolver* dmResolver;
@property (nonatomic, strong) DMSImageCameraSource* imageCameraSource;
//@property (nonatomic, strong) DMSAudioMicAQSource* audioMicSource;



@property NSInteger viewMode;
@property BOOL cameraViewActive;


@property (retain) IBOutlet UIView* cameraPreviewContainer;


@end