#import "VC_loginIn.h"
#import "VC_loginSelection.h"

@interface VC_loginIn ()<NSURLResponseDelegate>
{
}

@property (strong, nonatomic) MKNetworkOperation *operation;
@property(nonatomic, strong) webService_Post_Put_Delete *nsurl_obj;

@property (weak, nonatomic) IBOutlet UITextField *text_UserName;
@property (weak, nonatomic) IBOutlet UITextField *text_Password;
@property (weak, nonatomic) IBOutlet UIButton *button_login;




- (IBAction)action_login:(id)sender;

- (IBAction)action_continueWithGuest:(id)sender;

- (IBAction)unwindToVC_login:(UIStoryboardSegue *)unwindSegue;

- (IBAction)action_cancel:(id)sender;

- (IBAction)action_register:(id)sender;
@end

@implementation VC_loginIn

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    ApplicationDelegate.single.isGuestCheckOut = false;
    
    self.button_login.layer.cornerRadius = k_cornerRadiusButton;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}


#pragma mark - method_Login
-(void)method_Login:(NSDictionary *)dicloginCredential
{
    NSString *strUrl = [NSString stringWithFormat:@"%@/wcs/resources/store/10001/loginidentity",k_url];
    
    DLog(@"strUrl: %@",strUrl);
    strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    if (!self.nsurl_obj)
    {
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
        
        self.nsurl_obj = [[webService_Post_Put_Delete alloc] initWithURL:strUrl data:dicloginCredential seriveType:@"POST" seriveName:@"login"];
        self.nsurl_obj.delegate = self;
    }
}


#pragma mark - method_guestLogin
-(void)method_guestLogin
{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    
    DLog(@"guest");
    if (![self.operation isExecuting])
    {
        self.operation = [ApplicationDelegate.webServiceEngine method_guestLogin:nil
                                                               completionHandler:^(id response)
                          {
                              [SVProgressHUD dismiss];
                              
                              NSDictionary *result = response;
                              NSLog(@"result:%@",result);
                              if (response != nil)
                              {
                                  if ([result valueForKey:@"WCToken"])
                                  {
                                      ApplicationDelegate.single.isLoggedIn = NO;
                                      ApplicationDelegate.single.string_WCToken = [result valueForKey:@"WCToken"];
                                      ApplicationDelegate.single.string_WCTrustedToken = [result valueForKey:@"WCTrustedToken"];
                                      
                                      DLog(@"WCToken = %@", ApplicationDelegate.single.string_WCToken);
                                      DLog(@"WCTrustedToken = %@", ApplicationDelegate.single.string_WCTrustedToken);

//                                      [self performSegueWithIdentifier:@"segue_home" sender:nil];
//                                      [[NSNotificationCenter defaultCenter] postNotificationName:@"loginPopupClosed" object:self];
//                                      [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
                                      [self dismissViewControllerAnimated:YES completion:nil];
                                  }
                                  else
                                  {
                                      if ([result valueForKey:@"errors"])
                                      {
                                          [[[UIAlertView alloc] initWithTitle:nil message:[[[result valueForKey:@"errors"] objectAtIndex:0] valueForKey:@"errorMessage"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                                      }
                                      else
                                      {
                                          [[[UIAlertView alloc] initWithTitle:nil
                                                                      message:@"Authentication failed"
                                                                     delegate:nil
                                                            cancelButtonTitle:@"Ok"
                                                            otherButtonTitles:nil, nil]
                                           show];
                                      }
                                  }
                              }
                              else
                              {
                                  [[[UIAlertView alloc] initWithTitle:nil message:@"There is something wrong, from the server side,please try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                              }
                          }
                                                                    errorHandler:^(NSError *error)
                          {
                              [SVProgressHUD dismiss];
                              
                              DLog(@"there is something is wrong, please try with valid id and pass");
                              //[self performSegueWithIdentifier:@"segue_ProductList" sender:nil];
                              [[[UIAlertView alloc] initWithTitle:nil
                                                          message:[error localizedDescription]
                                                         delegate:nil
                                                cancelButtonTitle:@"Ok"
                                                otherButtonTitles:nil, nil]
                               show];
                              
                              DLog(@"1:%@\n2:%@\n3:%@\n4:%@",
                                   [error localizedDescription],
                                   [error localizedFailureReason],
                                   [error localizedRecoveryOptions],
                                   [error localizedRecoverySuggestion]);
                          }
                          ];
    }
    
    [self.operation onDownloadProgressChanged:^(double progress)
     {
         DLog(@"download progress = %.2f", progress*100.0);
     }];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_zipcode"] || [segue.identifier isEqualToString:@"segue_register"])
    {
        self.text_UserName.text = @"";
        self.text_Password.text = @"";
    }
    else if ([segue.identifier isEqualToString:@"segue_loginSelection"])
    {
        VC_loginSelection *obj = segue.destinationViewController;
        obj.bool_Login = [sender boolValue];
    }
}


#pragma mark - action_login
- (IBAction)action_login:(id)sender
{
    //[self performSegueWithIdentifier:@"segue_loginSelection" sender:[NSString stringWithFormat:@"%d",1]];
    
    if ([self.text_UserName.text length] && [self.text_Password.text length])
    {
        NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:self.text_UserName.text,@"logonId",self.text_Password.text,@"logonPassword", nil];
        DLog(@"%@",parameters);
        
        [self method_Login:parameters];
        
            //[self performSegueWithIdentifier:@"segue_home" sender:nil];
    }
    else
    {
        if ([self.text_UserName.text length] == 0)
        {
            [[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Please enter user name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Please enter password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
    }
}



#pragma mark - method_GotoHome
-(void)method_GotoHome
{
    [self performSegueWithIdentifier:@"segue_home" sender:nil];
}


#pragma mark - unwindToVC_login
- (IBAction)unwindToVC_login:(UIStoryboardSegue *)unwindSegue
{
    DLog(@"exit and go to home screen");
    [self method_guestLogin];
    //[self performSelector:@selector(method_GotoHome) withObject:nil afterDelay:0.5];
}

#pragma mark - action_cancel
- (IBAction)action_cancel:(id)sender
{
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"loginPopupClosed" object:self];
//    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    //[self dismissViewControllerAnimated:YES completion:nil];
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - action_register
- (IBAction)action_register:(id)sender
{

    [self performSegueWithIdentifier:@"segue_loginSelection" sender:[NSString stringWithFormat:@"%d",0]];
    
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"loginPopupClosed" object:self userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"yes",@"register", nil]];
//    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
}

#pragma mark -
#pragma mark -  POST by NSURL Session
#pragma mark - NSURLResponseDelegate
-(void)delegate_outPut:(NSData *)data urlRes:(NSURLResponse *)urlRes error_:(NSError *)error_ seriveName:(NSString *)serivename
{
    [SVProgressHUD dismiss];
    
    self.nsurl_obj.delegate = nil;
    self.nsurl_obj = nil;
    
    if (error_ == nil)
    {
        // Convert JSON Object into Dictionary
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:data options:
                              NSJSONReadingMutableContainers error:&error_];
        DLog(@"%@",JSON);
        if ([JSON valueForKey:@"WCToken"])
        {
                ApplicationDelegate.single.isLoggedIn = YES;
                ApplicationDelegate.single.string_WCToken = [JSON valueForKey:@"WCToken"];
                ApplicationDelegate.single.string_WCTrustedToken = [JSON valueForKey:@"WCTrustedToken"];
            
                //[self performSegueWithIdentifier:@"segue_home" sender:nil];
            
            DLog(@"WCToken = %@", ApplicationDelegate.single.string_WCToken);
            DLog(@"WCTrustedToken = %@", ApplicationDelegate.single.string_WCTrustedToken);
            
            
            

//            ApplicationDelegate.single.isComingFromLogInScreen = true;
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"loginPopupClosed" object:self];
//            [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
            [self dismissViewControllerAnimated:YES completion:nil];
            
            
        }
        else
        {
            if ([JSON valueForKey:@"errors"])
            {
                [[[UIAlertView alloc] initWithTitle:nil
                                            message:[[[JSON valueForKey:@"errors"] objectAtIndex:0] valueForKey:@"errorMessage"]
                                           delegate:nil
                                  cancelButtonTitle:@"Ok" 
                                  otherButtonTitles:nil, nil]
                 show];
            }
            else
            {
                [[[UIAlertView alloc] initWithTitle:nil
                                            message:@"Authentication failed"
                                           delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles:nil, nil]
                 show];
            }
        }
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:@"There is something wrong, from the server side,please try again"
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
    }
}


#pragma mark - action_continueWithGuest
- (IBAction)action_continueWithGuest:(id)sender
{
    ApplicationDelegate.single.isGuestCheckOut = true;
//    ApplicationDelegate.single.isComingFromLogInScreen = true;
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"loginPopupClosed" object:self];
//    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end