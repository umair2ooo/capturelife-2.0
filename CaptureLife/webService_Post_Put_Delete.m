#import "webService_Post_Put_Delete.h"

@implementation webService_Post_Put_Delete


@synthesize delegate = _delegate;

-(id)initWithURL:(NSString *)url_ data:(NSDictionary *)dic seriveType:(NSString *)serivetype seriveName:(NSString *)serivename
{
    self = [super init];
    if(self)
    {
        [self method_hitWebService:url_ data:dic seriveType:serivetype seriveName:serivename];
    }
    return self;
}

-(void)method_hitWebService:(NSString *)url_ data:(NSDictionary *)dic seriveType:(NSString *)serivetype seriveName:(NSString *)serivename
{
    NSError *error;
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    
    
    DLog(@"strUrl: %@",url_);
    DLog(@"%@", dic);
    
    url_ = [url_ stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSURL *url = [NSURL URLWithString:url_];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue:ApplicationDelegate.single.string_WCToken forHTTPHeaderField:@"WCToken"];
    [request addValue:ApplicationDelegate.single.string_WCTrustedToken forHTTPHeaderField:@"WCTrustedToken"];
    
    
    NSString *postbody = @"body=myBodyText&type=text";
    [request setHTTPBody:[postbody dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:TRUE]];
    
    [request setHTTPMethod:serivetype];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dic options:0 error:&error];
    [request setHTTPBody:postData];
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  if ([self.delegate respondsToSelector:@selector(delegate_outPut:urlRes:error_:seriveName:)])
                                                  {
                                                      
                                                      DLog(@"%@", request);
                                                      DLog(@"%@", data);
                                                      DLog(@"%@", response);
                                                      DLog(@"%@", [error localizedDescription]);
                                                      
                                                      
                                                      [self.delegate delegate_outPut:data urlRes:response error_:error seriveName:serivename];
                                                  }
                                              });
                                          }];
    
    [postDataTask resume];
}
@end

