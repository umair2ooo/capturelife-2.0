

#import "Singleton.h"
#import <Reachability.h>

@implementation Singleton

@synthesize string_controllerName = _string_controllerName;
@synthesize dic_productDetail = _dic_productDetail;
@synthesize string_WCToken = _string_WCToken;
@synthesize string_WCTrustedToken = _string_WCTrustedToken;
@synthesize string_orderId = _string_orderId;
@synthesize array_cartObjects =_array_cartObjects;
@synthesize isLoggedIn = _isLoggedIn;
@synthesize isGuestCheckOut = _isGuestCheckOut;
@synthesize fromDetail = _fromDetail;


static Singleton *sharedSingleton = nil;

+(Singleton *)retriveSingleton
{
	@synchronized(self)
	{
		if(sharedSingleton ==nil)
		{
			sharedSingleton =[[Singleton alloc]init];
		}
	}
	return sharedSingleton;
}

+(id)allocWithZone:(NSZone *)zone
{
	@synchronized(self)
	{
		if(sharedSingleton ==nil)
		{
			sharedSingleton = [super allocWithZone:zone];
			return sharedSingleton;
		}
	}
	return nil;
}

-(BOOL)method_connectToInternet
{
    Reachability *r = [Reachability reachabilityWithHostname:@"www.google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];    
    BOOL result = NO;
    if (internetStatus == ReachableViaWiFi )
    {
        result = YES;    
    } 
    else if(internetStatus == ReachableViaWWAN)
    {
        result = YES;
    }
    return result;
}

#pragma mark - is email address validity?
-(BOOL)method_NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

#pragma mark - are all spaces?
-(BOOL)method_checkOnlySpaces:(NSString *)str
{
    BOOL isValid = false;
    
    NSString *rawString = str;
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmed = [rawString stringByTrimmingCharactersInSet:whitespace];
    
    if ([trimmed length] > 0)
    {
        isValid = true;
    }
    
    return isValid;
}
#pragma mark - phone number validation
-(BOOL)method_phoneNumberValidation:(NSString *)phoneNumber
{
    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    return [phoneTest evaluateWithObject:phoneNumber];
}



#pragma mark - UILable variable height
-(float) getHeightForText:(NSString*) text withFont:(UIFont*) font andWidth:(float) width
{
    CGSize constraint = CGSizeMake(width , 20000.0f);
    CGSize title_size;
    float totalHeight;
    
    SEL selector = @selector(boundingRectWithSize:options:attributes:context:);
    if ([text respondsToSelector:selector]) {
        title_size = [text boundingRectWithSize:constraint
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{ NSFontAttributeName : font }
                                        context:nil].size;
        
        totalHeight = ceil(title_size.height);
    } else {
        title_size = [text sizeWithFont:font
                      constrainedToSize:constraint
                          lineBreakMode:NSLineBreakByWordWrapping];
        totalHeight = title_size.height ;
    }
    
    CGFloat height = MAX(totalHeight, 40.0f);
    return height;
}


@end