//
//  VC_SubofSubCategory.m
//  CaptureLife
//
//  Created by Fahim Bilwani on 4/27/15.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import "VC_SubofSubCategory.h"
#import "cell_Category.h"
#import "VC_ProductList.h"

@interface VC_SubofSubCategory ()
{
    NSString *productName;
}

@property (weak, nonatomic) IBOutlet UITableView *tableview_;
@property (strong, nonatomic) MKNetworkOperation *operation;
@end

@implementation VC_SubofSubCategory

#pragma mark - set heading
-(void)method_setHeading
{
    UILabel *label_heading = (UILabel *)[self.navigationController.view viewWithTag:999];
    
    [label_heading setText:@"Browse by Type"];
}

#pragma mark - cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    DLog(@"%@",self.array_categories);
    [self.tableview_ reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self method_setHeading];
    [self method_setAddToCartCounterLabel];
}

#pragma mark - method_setAddToCartCounterLabel
-(void)method_setAddToCartCounterLabel
{
    UIView *view_counter;
    if (ApplicationDelegate.single.array_cartObjects)
    {
        if ([ApplicationDelegate.single.array_cartObjects count])
        {
            view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
            UILabel *label_counter = (UILabel *)[self.navigationController.view viewWithTag:888];
            [view_counter setHidden:NO];
            label_counter.text = [NSString stringWithFormat:@"%lu",(unsigned long)[ApplicationDelegate.single.array_cartObjects count]];
        }
        else
        {
            view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
            [view_counter setHidden:YES];
        }
        
    }
    else
    {
        view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
        [view_counter setHidden:YES];
    }
}


-(void)viewWillDisappear:(BOOL)animated
{
    if(self.operation)
    {
        [self.operation cancel];
        self.operation = nil;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - tableview dataSource and delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.array_categories count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell_Category *cell = (cell_Category *)[tableView dequeueReusableCellWithIdentifier:@"cell_Category"];
    
    if (cell == nil)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"cell_Category" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    [cell method_setCategoryName:[[self.array_categories objectAtIndex:indexPath.row] valueForKey:@"name"]];
//    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    productName = [[self.array_categories objectAtIndex:indexPath.row] valueForKey:@"name"];
    [self method_getproductListByCategory:[[self.array_categories objectAtIndex:indexPath.row] valueForKey:@"uniqueID"]];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_SubCatSubProductList"])
    {
        VC_ProductList *obj = segue.destinationViewController;
        obj.productName = productName;
        obj.array_products = [NSMutableArray arrayWithArray:(NSMutableArray *)sender];
    }
}

#pragma mark - method_getSubCategories
-(void)method_getproductListByCategory:(NSString *)categoryID
{
    if (![self.operation isExecuting])
    {
        self.operation = [ApplicationDelegate.webServiceEngine method_getProductsByCatgeory:categoryID
                                                                          completionHandler:^(id response)
                          {
                              NSDictionary *result = response;
                              NSLog(@"result:%@",result);
                              if (response != nil)
                              {
                                  if ([result valueForKey:@"CatalogEntryView"])
                                  {
                                      VC_ProductList *objProductList = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_ProductList"];
                                      objProductList.array_products = [[NSMutableArray alloc] init];
                                      [(NSArray *)[result valueForKey:@"CatalogEntryView"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                          [objProductList.array_products addObject:[NSDictionary dictionaryWithObjectsAndKeys:[obj valueForKey:@"name"],@"name",[obj valueForKey:@"uniqueID"],@"uniqueID",[obj valueForKey:@"thumbnail"],@"thumbnail",[obj valueForKey:@"fullImage"],@"fullImage",[[[obj valueForKey:@"Price"] objectAtIndex:0] valueForKey:@"priceValue"],@"Price", nil]];
                                      }];
                                      NSLog(@"Array: %@", objProductList.array_products);
                                      [self performSegueWithIdentifier:@"segue_SubCatSubProductList" sender:objProductList.array_products];
                                  }
                                  else
                                  {
                                      [[[UIAlertView alloc] initWithTitle:nil message:@"Products not found" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                                  }
                                  
                              }
                              else
                              {
                                  [[[UIAlertView alloc] initWithTitle:nil message:@"There is something wrong, from the server side,please try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                              }
                          }
                                                                               errorHandler:^(NSError *error)
                          {
                              DLog(@"there is something is wrong, please try with valid id and pass");
                              DLog(@"1:%@\n2:%@\n3:%@\n4:%@",
                                   [error localizedDescription],
                                   [error localizedFailureReason],
                                   [error localizedRecoveryOptions],
                                   [error localizedRecoverySuggestion]);
                          }
                          ];
    }
    
    [self.operation onDownloadProgressChanged:^(double progress)
     {
         DLog(@"download progress = %.2f", progress*100.0);
     }];
}


@end
