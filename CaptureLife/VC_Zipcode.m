//
//  VC_Zipcode.m
//  CaptureLife
//
//  Created by Fahim Bilwani on 4/27/15.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import "VC_Zipcode.h"
#import <MapKit/MapKit.h>

@interface VC_Zipcode ()
@property (weak, nonatomic) IBOutlet UITextField *text_ZipCode;
@property (weak, nonatomic) IBOutlet UILabel *label_LocationMsg;
@property (weak, nonatomic) IBOutlet UIImageView *image_locationIcon;
- (IBAction)action_ZipCode:(id)sender;
- (IBAction)action_Cancel:(id)sender;
@end

@implementation VC_Zipcode

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (![CLLocationManager locationServicesEnabled])
    {
        [self.label_LocationMsg setHidden:NO];
        [self.image_locationIcon setHidden:NO];
    }
    else
    {
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied)
        {
            [self.label_LocationMsg setHidden:NO];
            [self.image_locationIcon setHidden:NO];
        }
        else
        {
            [self.label_LocationMsg setHidden:YES];
            [self.image_locationIcon setHidden:YES];
        }
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)action_ZipCode:(id)sender
{
    if ([self.text_ZipCode.text length] >= 3)
    {
//        [self performSegueWithIdentifier:@"segue_exitToLoginViaRegister" sender:nil];
        NSUserDefaults *loginDetails = [NSUserDefaults standardUserDefaults];
        [loginDetails setObject:@"YES" forKey:@"isZipcodeEntered"];
        [loginDetails synchronize];
//
//        [self performSegueWithIdentifier:@"segue_homeViaZip" sender:nil];

        [[NSNotificationCenter defaultCenter] postNotificationName:@"zipCodePopupClosed" object:[NSNumber numberWithBool:NO]];
        [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Failure"
                                    message:@"Invalid zip code."
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
    }
}

- (IBAction)action_Cancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}




@end
