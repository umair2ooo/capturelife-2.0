

@interface dataModel_SKUsLevelOne : NSObject

@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *identifierTwo;
@property (nonatomic, assign) NSString *name;
@property (nonatomic, strong) NSString *uniqueID;
@property (nonatomic, strong) NSString *usage;


@end