#import "cell_ProductList.h"
#import <UIKit/UIKit.h>

@implementation cell_ProductList

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)method_setValues:(NSDictionary *)dic
{
//    [self.image_product setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",k_url,[dic valueForKey:@"image"]]]
//                                 placeholderImage:[UIImage imageNamed:k_placeHolder]
//                      usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    [self.image_product sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",k_url,[dic valueForKey:@"image"]]]
                          placeholderImage:[UIImage imageNamed:k_placeHolder]];

    self.label_name.text = [dic valueForKey:@"name"];
    if ([dic valueForKey:@"price"] != nil)
    {
        self.lable_Price.text = [NSString stringWithFormat:@"$ %@0/each",[dic valueForKey:@"price"]];
    }
    else
    {
        self.lable_Price.text = @"-";
    }
}
@end