//
//  PayoffItem.m
//  DMSDemo
//
//  Created by Sinclair, Eoin on 2/13/14.
//  Copyright (c) 2014 Digimarc. All rights reserved.
//

#import "PayoffItem.h"

@implementation PayoffItem

-(id)init {
    return [self initWithImageData:nil
                              date:[NSDate date]
                           payload:nil
                             title:nil
                          subtitle:nil
                    thumbnailImage:nil
                           content:nil
                        actionType:nil
                       actionToken:nil
                    correlationKey:nil
                synthesizedContent:YES];
}

-(id)initWithImageData:(NSData*)data
                  date:(NSDate*)date
               payload:(DMPayload*)payload
                 title:(NSString *)title
              subtitle:(NSString *)subtitle
        thumbnailImage:(NSString*)thumbnailImage
               content:(NSString*)content
            actionType:(NSString*)actionType
           actionToken:(NSString *)token
        correlationKey:(NSString*)key
    synthesizedContent:(BOOL)synthesized {
    
    if (self = [super init]) {
        self.imageData      = data;
        self.date           = date;
        self.payload        = payload;
        self.title          = title;
        self.subtitle       = subtitle;
        self.thumbnailImage = thumbnailImage;
        self.content        = content;
        self.actionType     = actionType;
        self.actionToken    = token;
        self.correlationKey = key;
        self.synthesizedContent = synthesized;
        if (!self.imageData && !self.date && !self.payload && !self.title) {
            NSLog(@"Empty history item allocated; returning nil.");
            return nil;
        }
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    NSData *tempImageData       = [aDecoder decodeObjectForKey:kImageDataKey];
    NSDate *tempDate            = [aDecoder decodeObjectForKey:kDateKey];
    DMPayload *tempPayload      = [aDecoder decodeObjectForKey:kPayloadKey];
    NSString *tempTitle         = [aDecoder decodeObjectForKey:kTitleKey];
    NSString *tempSubtitle      = [aDecoder decodeObjectForKey:kSubtitleKey];
    NSString *tempThumbnailImage     = [aDecoder decodeObjectForKey:kThumbnailImageKey];
    NSString *tempContent       = [aDecoder decodeObjectForKey:kContentKey];
    NSString *tempActionType    = [aDecoder decodeObjectForKey:kActionTypeKey];
    NSString *tempActionToken   = [aDecoder decodeObjectForKey:kActionTokenKey];
    NSString *tempKey           = [aDecoder decodeObjectForKey:kCorrelationKey];
    NSString *tempSynthesized   = [aDecoder decodeObjectForKey:kSynthesizedContentKey];
    
    return [self initWithImageData:tempImageData
                              date:tempDate
                           payload:tempPayload
                             title:tempTitle
                          subtitle:tempSubtitle
                    thumbnailImage:tempThumbnailImage
                           content:tempContent
                        actionType:tempActionType
                       actionToken:tempActionToken
                               correlationKey:tempKey
                synthesizedContent:(tempSynthesized && [tempSynthesized isEqualToString:@"YES"]) ? YES : NO
            ];
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.imageData     forKey:kImageDataKey];
    [aCoder encodeObject:self.date          forKey:kDateKey];
    [aCoder encodeObject:self.payload       forKey:kPayloadKey];
    [aCoder encodeObject:self.title         forKey:kTitleKey];
    [aCoder encodeObject:self.subtitle      forKey:kSubtitleKey];
    [aCoder encodeObject:self.thumbnailImage forKey:kThumbnailImageKey];
    [aCoder encodeObject:self.content       forKey:kContentKey];
    [aCoder encodeObject:self.actionType    forKey:kActionTypeKey];
    [aCoder encodeObject:self.actionToken   forKey:kActionTokenKey];
    [aCoder encodeObject:self.correlationKey forKey:kCorrelationKey];
    [aCoder encodeObject:self.synthesizedContent ? @"YES" : @"NO" forKey:kSynthesizedContentKey];
}

@end
