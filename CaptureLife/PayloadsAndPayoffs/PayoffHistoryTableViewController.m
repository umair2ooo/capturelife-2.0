//
//  PayoffHistoryTableViewController.m
//  DMSDemo
//
//  Created by Sinclair, Eoin on 2/10/14.
//  Copyright (c) 2014 Digimarc. All rights reserved.
//

#import "PayoffHistoryTableViewController.h"
#import "PayoffDatabase.h"
#import "DMResolverSettings.h"

// Payoff retention policy settings:
//      This app will retain most recent 30 payoffs
//      For up to 72 hours (259200 seconds)
//
#define kPayoffHistoryDepth         30
#define kPayoffTimeoutInSeconds 259200

#define kPayoffHistoryCellIdentifier @"PayoffHistoryCell"

@implementation PayoffHistoryTableViewController

- (id)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.payoffsHistory = [PayoffDatabase loadPayoffDocs];
    [self reorderHistory];

    // Automatically trim the table to contain only recently reported payoffs.
    // Refresh this every 5 seconds while the view remains visible
    //
    NSTimer *t = [NSTimer timerWithTimeInterval:5.0 target:self selector:@selector(reloadTable) userInfo:0 repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:t forMode:NSRunLoopCommonModes];
}

-(void)reloadTable {
    BOOL needsReload = NO;
    
    for (int i=0; i<[self payoffsHistory].count; i++)
        if ([[((PayoffDoc *)self.payoffsHistory[i]).payoffItem date] timeIntervalSinceNow] < -kPayoffTimeoutInSeconds) {
            [((PayoffDoc *)self.payoffsHistory[i]) deleteDoc];
            [self.payoffsHistory removeObjectAtIndex:i];
            needsReload = YES;
        }
    if (needsReload) {
        [self.tableView reloadData];
    }
}

-(void)reorderHistory {
    self.payoffsHistory = [[self.payoffsHistory sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSDate *date1 = [[(PayoffDoc*)obj1 payoffItem] date];
        NSDate *date2 = [[(PayoffDoc*)obj2 payoffItem] date];
        return [date2 compare:date1];
    }] mutableCopy];
    [self reloadTable];
}

-(void)newItemAvailable:(PayoffDoc *)doc {
    for (int i=0; i<[self.payoffsHistory count]; ) {
        PayoffDoc* existingDoc = (PayoffDoc*)self.payoffsHistory[i];
        if ([doc.payoffItem.payload isEqual:existingDoc.payoffItem.payload]) {
            NSLog(@"Incoming payload:%@, found previous item with same payload value in history.  Deleting existing (duplicate) payoff.", doc.payoffItem.payload);
            [existingDoc deleteDoc];
            [self.payoffsHistory removeObjectAtIndex:i];
            existingDoc = nil;
        } else {
            // if not removing an existing doc, then increment iterator
            i++;
        }
    }
    
    [self.payoffsHistory insertObject:doc atIndex:0];
    [doc saveData];
    
    if ([self.payoffsHistory count] > kPayoffHistoryDepth) {
        PayoffDoc *d = [self.payoffsHistory lastObject];
        [d deleteDoc];
        [self.payoffsHistory removeLastObject];
    }
    
    [self.tableView reloadData];
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.payoffsHistory count];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kPayoffHistoryCellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kPayoffHistoryCellIdentifier];
    }
    
    PayoffItem *item = [((PayoffDoc*)self.payoffsHistory[indexPath.row]) payoffItem];
    
    cell.textLabel.text = item.title;
    cell.detailTextLabel.text = item.subtitle;
    cell.imageView.image = [UIImage imageWithData:item.imageData];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(self.delegate && [self.delegate respondsToSelector:@selector(didSelectItemWithTitle:actionToken:content:)]) {
        PayoffItem *item = [((PayoffDoc*)self.payoffsHistory[indexPath.row]) payoffItem];
        [self.delegate didSelectItemWithTitle:item.title actionToken:item.actionToken content:item.content];
    }
}

@end
