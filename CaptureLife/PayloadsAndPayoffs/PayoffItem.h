//
//  PayoffItem.h
//  DMSDemo
//
//  Created by Sinclair, Eoin on 2/13/14.
//  Copyright (c) 2014 Digimarc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DMResolver.h"

#define kImageDataKey       @"DMSDemo_ImageData"
#define kDateKey            @"DMSDemo_Date"
#define kPayloadKey         @"DMSDemo_Payload"
#define kTitleKey           @"DMSDemo_Title"
#define kThumbnailImageKey  @"DMSDemo_ThumbnailImage"
#define kContentKey         @"DMSDemo_Content"
#define kSubtitleKey        @"DMSDemo_Subtitle"
#define kActionTypeKey      @"DMSDemo_ActionType"
#define kActionTokenKey     @"DMSDemo_ActionToken"
#define kCorrelationKey     @"DMSDemo_CorrelationKey"
#define kSynthesizedContentKey @"DMSDemo_SynthesizedContent"

@interface PayoffItem : NSObject <NSCoding>

@property (nonatomic, retain) NSData            *imageData;
@property (nonatomic, retain) NSDate            *date;
@property (nonatomic, retain) DMPayload         *payload;
@property (nonatomic, retain) NSString          *title;
@property (nonatomic, retain) NSString          *subtitle;
@property (nonatomic, retain) NSString          *thumbnailImage;
@property (nonatomic, retain) NSString          *content;
@property (nonatomic, retain) NSString          *actionType;
@property (nonatomic, retain) NSString          *actionToken;
@property (nonatomic, retain) NSString          *correlationKey;
@property (nonatomic) BOOL                      synthesizedContent;

@end
