#import "VC_Cart.h"
#import "cell_Cart.h"
#import "VC_checkOut.h"
#import "VC_loginIn.h"

@interface VC_Cart ()<DetailObject,NSURLSessionDelegate, NSURLResponseDelegate>
{
    int int_indexOfRemovingObject;
    int int_indexOfEditingObject;
    NSMutableDictionary *dicPricedetails;
}

@property(nonatomic, strong) webService_Post_Put_Delete *nsurl_obj;
@property(nonatomic, strong)NSString *string_quantity;
@property (strong, nonatomic) MKNetworkOperation *operation;
@property (strong, nonatomic) WebServiceEngine *webServiceEngine;

@property(nonatomic, readwrite)BOOL bool_goingToLogInScree;

@property (weak, nonatomic) IBOutlet UITableView *tableview_;
@property (weak, nonatomic) IBOutlet UILabel *label_totalPrice;
@property (weak, nonatomic) IBOutlet UIButton *button_Checkout;

- (IBAction)action_cancel:(id)sender;
- (IBAction)action_promotion:(id)sender;

- (IBAction)action_checkOut:(id)sender;

@end

@implementation VC_Cart



#pragma mark - set heading
-(void)method_setHeading
{
    UILabel *label_heading = (UILabel *)[self.navigationController.view viewWithTag:999];
    
    [label_heading setText:@"Shopping Cart"];
}



#pragma mark - cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self method_addViewsToNavController];
    
    [self.tableview_ reloadData];
    [self method_calculateTotal:@""];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self method_setHeading];
    
    [self method_setAddToCartCounterLabel];
    
    if (self.bool_goingToLogInScree == true)
    {
        self.bool_goingToLogInScree = false;
        
        if (ApplicationDelegate.single.isLoggedIn == true)
        {
            [self method_pushSubmitForm];
        }
        else
        {
            if (ApplicationDelegate.single.isGuestCheckOut == true)
            {
                ApplicationDelegate.single.isGuestCheckOut = false;
                
                [self method_pushSubmitForm];
            }
        }
    }
}

#pragma mark - method_setAddToCartCounterLabel
-(void)method_setAddToCartCounterLabel
{
    UIView *view_counter;
    if (ApplicationDelegate.single.array_cartObjects)
    {
        if ([ApplicationDelegate.single.array_cartObjects count])
        {
            view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
            UILabel *label_counter = (UILabel *)[self.navigationController.view viewWithTag:888];
            [view_counter setHidden:NO];
            label_counter.text = [NSString stringWithFormat:@"%lu",(unsigned long)[ApplicationDelegate.single.array_cartObjects count]];
        }
        else
        {
            view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
            [view_counter setHidden:YES];
        }
        
    }
    else
    {
        view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
        [view_counter setHidden:YES];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Navigation

 //In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_checkOut"])
    {
        VC_checkOut *obj = segue.destinationViewController;
        obj.dic_priceDetails = dicPricedetails;
    }
}

#pragma mark - action_cancel
- (IBAction)action_cancel:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}



#pragma mark - action_promotion
- (IBAction)action_promotion:(id)sender
{
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@"Promotion Code"
                              message:@"Please enter promotion code:"
                              delegate:self
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:@"Cancel", nil];
    [alertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
    alertView.tag = 100;
    /* Display a numerical keypad for this text field */
    UITextField *textField = [alertView textFieldAtIndex:0];
    textField.keyboardType = UIKeyboardTypeNumberPad;
    [alertView show];
}


#pragma mark - method_details
-(void)method_details:(int)tag
{
    DLog(@"%d",tag);
}
#pragma mark - method_deleteObject
-(void)method_deleteObject:(int)tag
{
    DLog(@"delete");
    DLog(@"%d", tag);
    
    int_indexOfRemovingObject = tag;

    NSDictionary *dic_temp = [NSDictionary dictionaryWithObjectsAndKeys:
                              [[ApplicationDelegate.single.array_cartObjects objectAtIndex:tag] valueForKey:@"orderItemId"], @"orderItemId", nil];
    
    
    
    NSString *strUrl = [NSString stringWithFormat:@"%@wcs/resources/store/10001/cart/@self/delete_order_item",k_url];
    DLog(@"strUrl: %@",strUrl);
    strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    if (!self.nsurl_obj)
    {
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
        
        self.nsurl_obj = [[webService_Post_Put_Delete alloc] initWithURL:strUrl
                                                               data:[NSDictionary dictionaryWithObjectsAndKeys:ApplicationDelegate.single.string_orderId,@"orderId",[NSArray arrayWithObject:dic_temp], @"orderItem", nil]
                                                         seriveType:@"PUT" seriveName:@"deleteCart"];
        self.nsurl_obj.delegate = self;
    }
}


#pragma mark - method_editObject
-(void)method_editObject:(int)tag
{
    DLog(@"edit");
    DLog(@"%d", tag);
    
    int_indexOfEditingObject = tag;

    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@"Quantity"
                              message:@"Please enter quantity:"
                              delegate:self
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:@"Cancel", nil];
    [alertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
    /* Display a numerical keypad for this text field */
    alertView.tag = 101;
    UITextField *textField = [alertView textFieldAtIndex:0];
    
    textField.text = [[ApplicationDelegate.single.array_cartObjects objectAtIndex:tag] valueForKey:@"quantity"];
    textField.keyboardType = UIKeyboardTypeNumberPad;
    
    [alertView show];
}


#pragma mark- uialert view delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 101)
    {
        //// edit item
        if (buttonIndex==0)
        {
            UITextField *textField = [alertView textFieldAtIndex:0];
            
            if ([textField.text length])
            {
                self.string_quantity = textField.text;
                
                NSDictionary *dic_temp = [NSDictionary dictionaryWithObjectsAndKeys:
                                                      [[ApplicationDelegate.single.array_cartObjects objectAtIndex:int_indexOfEditingObject] valueForKey:@"orderItemId"], @"orderItemId",
                                                      textField.text, @"quantity",  nil];
                
                NSArray *array_temp = [NSArray arrayWithObjects:dic_temp, nil];
                
                NSDictionary *dic_tempTwo = [NSDictionary dictionaryWithObjectsAndKeys:
                                                         array_temp, @"orderItem",
                                                         ApplicationDelegate.single.string_orderId, @"orderId", nil];
                DLog(@"%@", dic_tempTwo);
                
                
                
                NSString *strUrl = [NSString stringWithFormat:@"%@wcs/resources/store/10001/cart/@self",k_url];
                
                DLog(@"strUrl: %@",strUrl);
                strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
                
                if (!self.nsurl_obj)
                {
                    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
                    
                    self.nsurl_obj = [[webService_Post_Put_Delete alloc] initWithURL:strUrl data:dic_tempTwo seriveType:@"PUT" seriveName:@"editCart"];
                    self.nsurl_obj.delegate = self;
                }
            }
        }
    }
    else
    {
        // promotion code
        if (buttonIndex==0)
        {
            UITextField *textField = [alertView textFieldAtIndex:0];
            
            if ([textField.text length])
            {
                if ([textField.text isEqualToString:@"1111"] || [textField.text isEqualToString:@"2222"])
                {
                    [self method_calculateTotal:@"10"];
                }
                else
                {
                    [[[UIAlertView alloc] initWithTitle:nil message:@"Invalid promotion code" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                }
            }
        }

    }
}
#pragma mark - tableview dataSource and delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [ApplicationDelegate.single.array_cartObjects count]?[ApplicationDelegate.single.array_cartObjects count]:0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell_Cart *cell = (cell_Cart *)[tableView dequeueReusableCellWithIdentifier:@"cell_Cart"];
    
    if (cell == nil)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"cell_Cart" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    cell.button_Edit.tag = indexPath.row;
    cell.button_details.tag = indexPath.row;
    cell.button_Remove.tag = indexPath.row;
    cell.delegate = self;
    [cell method_setValues:[NSDictionary dictionaryWithObjectsAndKeys:[[ApplicationDelegate.single.array_cartObjects objectAtIndex:indexPath.row] valueForKey:@"name"],@"name",[[ApplicationDelegate.single.array_cartObjects objectAtIndex:indexPath.row] valueForKey:@"priceValue"],@"price",[[ApplicationDelegate.single.array_cartObjects objectAtIndex:indexPath.row] valueForKey:@"thumbnail"],@"image",[[ApplicationDelegate.single.array_cartObjects objectAtIndex:indexPath.row] valueForKey:@"quantity"],@"quantity", nil]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - calculate total price and number of items
-(void)method_calculateTotal:(NSString *)discount
{
    if ([ApplicationDelegate.single.array_cartObjects count])
    {
        __block int int_totalItems = 0;
        __block float float_totalPrice = 0.0;
        
        
        [ApplicationDelegate.single.array_cartObjects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
         {
             float_totalPrice = float_totalPrice + ([[obj valueForKey:@"priceValue"] floatValue] * [[obj valueForKey:@"quantity"] intValue]);
             int_totalItems = int_totalItems + [[obj valueForKey:@"quantity"] intValue];
             if ([discount length])
             {
                 float_totalPrice = float_totalPrice - 10;
             }
             if (idx == [ApplicationDelegate.single.array_cartObjects count]-1)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     self.label_totalPrice.text = [NSString stringWithFormat:@"%.2f", float_totalPrice];
                     dicPricedetails = [[NSMutableDictionary alloc] initWithObjectsAndKeys:self.label_totalPrice.text,@"total", nil];
                     if ([discount length])
                         [dicPricedetails setValue:@"yes" forKey:@"discount"];
                     else
                         [dicPricedetails setValue:@"no" forKey:@"discount"];
                 });
             }
         }];
    }
    else
    {
        self.label_totalPrice.text = @"0.00";
    }
}


#pragma mark -
#pragma mark -  POST by NSURL Session
#pragma mark - NSURLResponseDelegate
-(void)delegate_outPut:(NSData *)data urlRes:(NSURLResponse *)urlRes error_:(NSError *)error_ seriveName:(NSString *)serivename
{
    [SVProgressHUD dismiss];
    self.nsurl_obj.delegate = nil;
    self.nsurl_obj = nil;
    
    
    if (error_ == nil)
    {
        // Convert JSON Object into Dictionary
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:data options:
                              NSJSONReadingMutableContainers error:&error_];
        DLog(@"%@",JSON);
        if ([serivename isEqualToString:@"editCart"])
        {
            //errors
            if (![JSON valueForKey:@"errors"])
            {
                NSMutableDictionary *dic_temp = [NSMutableDictionary dictionaryWithDictionary:[ApplicationDelegate.single.array_cartObjects objectAtIndex:int_indexOfEditingObject]];
                [dic_temp setValue:self.string_quantity forKey:@"quantity"];
                [ApplicationDelegate.single.array_cartObjects replaceObjectAtIndex:int_indexOfEditingObject withObject:dic_temp];
                DLog(@"%@", [ApplicationDelegate.single.array_cartObjects objectAtIndex:int_indexOfEditingObject]);
                [self method_calculateTotal:@""];
                [self method_setAddToCartCounterLabel];
                [self.tableview_ reloadData];
            }
            else
            {
                [[[UIAlertView alloc] initWithTitle:nil message:[[[JSON valueForKey:@"errors"] objectAtIndex:0] valueForKey:@"errorMessage"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
            }
        }
        else
        {
            if ([JSON valueForKey:@"orderId"])
            {
                [[[UIAlertView alloc] initWithTitle:nil
                message:@"Item removed successfully" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                [ApplicationDelegate.single.array_cartObjects removeObjectAtIndex:int_indexOfRemovingObject];
                [self method_calculateTotal:@""];
                [self method_setAddToCartCounterLabel];
                [self.tableview_ reloadData];
                
                if (![ApplicationDelegate.single.array_cartObjects count])
                {
                    ApplicationDelegate.single.array_cartObjects = nil;
                    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                }
            }
            else
            {
                //errors
                if ([JSON valueForKey:@"errors"])
                {
                    [[[UIAlertView alloc] initWithTitle:nil message:[[[JSON valueForKey:@"errors"] objectAtIndex:0] valueForKey:@"errorMessage"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                }
                else
                {
                    [[[UIAlertView alloc] initWithTitle:nil message:@"There is something wrong, please try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                }
            }
        }
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:nil message:@"There is something wrong, from the server side,please try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
    }
}



#pragma mark - method_addViewsToNavController
-(void)method_addViewsToNavController
{
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"View_topMenuSubCategory"
                                                         owner:self
                                                       options:nil];
    UIView *view_obj = nibContents[0];
    
    CGRect rect_new = CGRectMake(8,
                                 k_topBar_y,
                                 self.view.frame.size.width-16,
                                 view_obj.frame.size.height);
    
    
    
//    CGRect rect_new = CGRectMake(self.view.frame.size.width/2 - view_obj.frame.size.width/2,
//                                 k_topBar_y,
//                                 view_obj.frame.size.width,
//                                 view_obj.frame.size.height);
    
    
    
    view_obj.frame = rect_new;
    
    [self.navigationController.view addSubview:view_obj];
    
    
    
    nibContents = nil;
    
    
    
    nibContents = [[NSBundle mainBundle] loadNibNamed:@"View_menuSubCategory"
                                                owner:self
                                              options:nil];
    view_obj = nibContents[0];
    
    [view_obj setFrame:CGRectMake( self.view.frame.size.width,              //notice this is OFF screen!
                                  0,
                                  self.view.frame.size.width,
                                  self.view.frame.size.height)];
    
//    [view_obj setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
//    [view_obj setBackgroundColor:[UIColor clearColor]];
    
    [self.navigationController.view addSubview:view_obj];
    
    
//    UIView *backgroundView = [[UIView alloc] initWithFrame:view_obj.bounds];
//    
//    
//    UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleLight]];
//    
//    [visualEffectView setFrame:backgroundView.bounds];
//    [backgroundView addSubview:visualEffectView];
//    
//    [view_obj insertSubview:backgroundView atIndex:0];
    
//    UIView *view_subHeader = (UIView *)[self.navigationController.view viewWithTag:420];
//    view_subHeader.layer.cornerRadius = 10;
//    view_subHeader.layer.masksToBounds = YES;
}



#pragma mark - top menu Actions

- (IBAction)action_ShoppingCart:(id)sender
{
//    if (ApplicationDelegate.single.array_cartObjects)
//    {
//        if ([ApplicationDelegate.single.array_cartObjects count])
//        {
//            [self.navigationController presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"Nav_Cart"]
//                                                    animated:YES
//                                                  completion:nil];
//        }
//    }
//    else
//    {
//        [[[UIAlertView alloc] initWithTitle:nil message:@"Cart is empty" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil] show];
//    }
}

- (IBAction)action_home:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)action_menu:(id)sender
{
    UIView *view_temp = [self.navigationController.view viewWithTag:6];
    
    [UIView animateWithDuration:k_menuAnimationTime
                     animations:^{
                         
                         [view_temp setFrame:CGRectMake( 0,
                                                        0.0f,
                                                        self.view.frame.size.width,
                                                        self.view.frame.size.height)];
                         
                     }
                     completion:^(BOOL finished){
                     }
     ];
}

- (IBAction)action_back:(id)sender
{
    DLog(@"%@", [self.navigationController.visibleViewController class]);
    
    id viewController = self.navigationController.visibleViewController;
    if (![viewController isKindOfClass:[VC_Cart class]])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}





#pragma mark - full screen menu
- (IBAction)action_hideMenu:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    
    [UIView animateWithDuration:k_menuAnimationTime
                     animations:^{
                         
                         [btn.superview setFrame:CGRectMake( self.view.frame.size.width,                               //notice this is ON screen!
                                                            0.0f,
                                                            self.view.frame.size.width,
                                                            self.view.frame.size.height)];
                         
                     }
                     completion:^(BOOL finished){
                     }
     ];
}



- (IBAction)action_logOut:(id)sender
{
    ApplicationDelegate.single.array_cartObjects = nil;
    
    ApplicationDelegate.single.string_controllerName = k_logOut;
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)action_storeLocator:(id)sender
{
    ApplicationDelegate.single.string_controllerName = k_storeLocator;
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)action_scan:(id)sender
{
    ApplicationDelegate.single.string_controllerName = k_scan;
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}


- (IBAction)action_viewLoyalPoints:(id)sender
{
    ApplicationDelegate.single.string_controllerName = k_loyalityPoint;
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)action_barCode:(id)sender
{
    ApplicationDelegate.single.string_controllerName = k_barCodeScan;
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)action_signInRegister:(id)sender
{
    ApplicationDelegate.single.string_controllerName = k_Login;
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}


#pragma mark - action_checkOut
- (IBAction)action_checkOut:(id)sender
{
    if (ApplicationDelegate.single.isLoggedIn == false)
    {
        self.bool_goingToLogInScree = true;
        
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleloginPopupClosedNotification) name:@"loginPopupClosed" object:nil];
        UINavigationController *objNC_loginIn = [self.storyboard instantiateViewControllerWithIdentifier:@"NC_loginIn"];
//        [self presentPopupViewController:objVC_loginIn animationType:MJPopupViewAnimationSlideTopBottom];
        
//        VC_loginIn *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_loginIn"];
//        
        [self presentViewController:objNC_loginIn animated:YES completion:nil];
        
        //[self performSegueWithIdentifier:@"" sender:[NSString stringWithFormat:@"%d",0]];
    }
    else
    {
        [self method_pushSubmitForm];
    }
    
    
    
    
//    [[[UIAlertView alloc] initWithTitle:nil
//                                message:@"Here we have to do login first... then will move to the checkOut"
//                               delegate:nil
//                      cancelButtonTitle:@"Ok"
//                      otherButtonTitles:nil, nil]
//     show];
}


-(void)method_pushSubmitForm
{
    VC_checkOut *obj = (VC_checkOut *)[self.storyboard instantiateViewControllerWithIdentifier:@"VC_checkOut"];
    obj.dic_priceDetails = dicPricedetails;
    [self.navigationController pushViewController:obj animated:YES];
}

#pragma mark - handleloginPopupClosedNotification
-(void)handleloginPopupClosedNotification
{
    if (self.bool_goingToLogInScree == true)
    {
        self.bool_goingToLogInScree = false;
        
        if (ApplicationDelegate.single.isLoggedIn == true)
        {
            [self method_pushSubmitForm];
        }
        else
        {
            if (ApplicationDelegate.single.isGuestCheckOut == true)
            {
                ApplicationDelegate.single.isGuestCheckOut = false;
                
                [self method_pushSubmitForm];
            }
        }
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"loginPopupClosed" object:nil];
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}
@end