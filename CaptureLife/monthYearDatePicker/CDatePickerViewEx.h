//#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class CDatePickerViewEx;
@protocol SelectedDate <NSObject, UIPickerViewDelegate>

@required
-(void)method_selectedDate:(NSString *)string_date;

@end // end of delegate protocol


@interface CDatePickerViewEx : UIPickerView <UIPickerViewDelegate, UIPickerViewDataSource>
{
}

@property (nonatomic, strong, readonly) NSDate *date;

@property (nonatomic, weak) id<SelectedDate> delegate_mine;
-(void)selectToday;

@end