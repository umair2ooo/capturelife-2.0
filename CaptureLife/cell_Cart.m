//
//  cell_Cart.m
//  CaptureLife
//
//  Created by Fahim Bilwani on 4/27/15.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import "cell_Cart.h"

@implementation cell_Cart

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)action_details:(id)sender
{
    DLog(@"%ld",[sender tag]);
    [self.delegate method_details:(int)[sender tag]];
}

- (IBAction)action_Edit:(id)sender
{
    [self.delegate method_editObject:(int)[sender tag]];
}

- (IBAction)action_Remove:(id)sender
{
    [self.delegate method_deleteObject:(int)[sender tag]];
}

-(void)method_setValues:(NSDictionary *)dic
{
    [self.image_product sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",k_url,[dic valueForKey:@"image"]]]
                          placeholderImage:[UIImage imageNamed:k_placeHolder]];
    
    self.label_name.text = [dic valueForKey:@"name"];
    self.lable_Price.text = [dic valueForKey:@"price"];
    self.label_Quantity.text = [dic valueForKey:@"quantity"];
    self.label_TotalPrice.text = [NSString stringWithFormat:@"$%@",[self method_calculateTotalPrice]];
}
-(NSString *)method_calculateTotalPrice
{
    return [NSString stringWithFormat:@"%.2f",[self.lable_Price.text floatValue] * [self.label_Quantity.text floatValue]];
}



@end
