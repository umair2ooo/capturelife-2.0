//
//  DemoTableControllerViewController.m
//  FPPopoverDemo
//
//  Created by Alvise Susmel on 4/13/12.
//  Copyright (c) 2012 Fifty Pixels Ltd. All rights reserved.
//

#import "DemoTableController.h"
#import "FPViewController.h"

@interface DemoTableController ()

@end

@implementation DemoTableController
@synthesize delegate=_delegate;
@synthesize array_predictions;
@synthesize delegated=_delegated;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = nil;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 11;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.array_predictions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.textLabel.text = [self.array_predictions objectAtIndex:indexPath.row];
    [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica" size:12.0]];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    return cell;
}


#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if([self.delegate respondsToSelector:@selector(selectedTableRow:)])
    {
        [self.delegate selectedTableRow:indexPath.row];
    }
    [self.delegated selectedValueFromDropDown:[self.array_predictions objectAtIndex:indexPath.row] controller:self];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
-(void)reloadList:(NSArray *)giveData
{
    self.array_predictions = giveData;
    DLog(@"%@",self.array_predictions);
    [self.tableView reloadData];
}
@end
