#import "VC_ProductDetail.h"

#import "UPStackMenu.h"
#import "YCameraViewController.h"
#import <Social/Social.h>
#import "MGInstagram.h"
#import <Pinterest/Pinterest.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "VC_productInformation.h"
#import "VC_pickupStore.h"

#define k_origin_x 0.0
#define k_origin_y 144.0

@interface VC_ProductDetail ()<UPStackMenuDelegate,MFMailComposeViewControllerDelegate, NSURLSessionDelegate, NSURLResponseDelegate, YCameraViewControllerDelegate>
{
    CGFloat ImageHeight;
    CGFloat ImageWidth;
    UPStackMenu *stack;
    Pinterest*  _pinterest;
    NSArray *array_products;
}

@property(nonatomic, strong) webService_Post_Put_Delete *nsurl_obj;

@property (weak, nonatomic) IBOutlet UIButton *button_stock;
@property (weak, nonatomic) IBOutlet UIImageView *imageView_BackZoomer;
@property (weak, nonatomic) IBOutlet UIScrollView *scroller;
@property (weak, nonatomic) IBOutlet UIView *view_scrollerContainer;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView_subScroller;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIButton *button_seeInYourHome;
@property (weak, nonatomic) IBOutlet UIImageView *imagview_iconCross;
@property (strong, nonatomic) WebServiceEngine *webServiceEngine;

#pragma mark - product detail view

@property (weak, nonatomic) IBOutlet UIView *view_productDetail;
@property (weak, nonatomic) IBOutlet UITextField *label_quantity;
@property (weak, nonatomic) IBOutlet UILabel *lable_name;
@property (weak, nonatomic) IBOutlet UILabel *lable_price;
@property (weak, nonatomic) IBOutlet UIButton *button_addToList;
@property (weak, nonatomic) IBOutlet UIButton *button_addToCart;
@property (weak, nonatomic) IBOutlet UILabel *label_storeName;


- (IBAction)action_productInformation:(id)sender;
- (IBAction)action_rating:(id)sender;
- (IBAction)action_addToCart:(id)sender;
- (IBAction)action_chat:(id)sender;
- (IBAction)actionpickUpStore:(id)sender;


#pragma mark - product detail view

@property(nonatomic, strong)dataModel_SKUsLevelThree *SKUsLevelThree;
@property(nonatomic, strong)dataModel_SKUsLevelTwo *SKUsLevelTwo;
@property(nonatomic, strong)dataModel_SKUsLevelOne *SKUsLevelOne;

@property (strong, nonatomic) MKNetworkOperation *operation;

@property(nonatomic, strong)NSMutableArray *pageViews;
@property (weak, nonatomic) IBOutlet UIView *view_Share;

- (IBAction)action_AR:(id)sender;


@end

@implementation VC_ProductDetail


#pragma mark - converting digimarc ID to WCS ID
-(NSString *)method_digimarToWCS:(NSString *)id_
{
    if ([id_ isEqualToString:@"7794"])//shoe
    {
        return  @"10003";
    }
    else if ([id_ isEqualToString:@"6565"])//orange jacket
    {
        return @"10002";
    }
    else if ([id_ isEqualToString:@"6986"])//wrist watch
    {
        return @"10004";
    }
    else if ([id_ isEqualToString:@"78954"])//black jacket
    {
        return @"10005";
    }
    else if ([id_ isEqualToString:@"4325"])//Bed
    {
        return @"10006";
    }
    
    
    
    //////////////////////////              for bed room
    
    //    Beautiful Double Bed
    //    15951
    //
    //    Light Ball
    //    15952
    //
    //    Artframe
    //    15957
    //
    //    Wall Tree
    //    15959
    //
    //    White Flower Vase
    //    15955
    
    
    
    else if ([id_ isEqualToString:@"15951"] || [id_ isEqualToString:@"15952"] || [id_ isEqualToString:@"15955"] || [id_ isEqualToString:@"15957"] || [id_ isEqualToString:@"15959"])
    {
        return id_;
    }
    else
    {
        //        [[[UIAlertView alloc] initWithTitle:nil
        //                                    message:@"Product doesn't exists in store"
        //                                   delegate:nil
        //                          cancelButtonTitle:@"Ok"
        //                          otherButtonTitles:nil, nil]
        //         show];
        
        //for coppel
        return id_;
    }
}


#pragma mark - set heading
-(void)method_setHeading
{
    UILabel *label_heading = (UILabel *)[self.navigationController.view viewWithTag:999];
    
    [label_heading setText:@"Products Detail"];
}

#pragma mark - cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    DLog(@"%@",self.uniqueID);
    
    [self method_getProductDetail];
    
    [self method_imageZoomerSetting];
    
    [self method_setCornerRadius];
    
    // Initialize a Pinterest instance with our client_id
    _pinterest = [[Pinterest alloc] initWithClientId:@"1443279" urlSchemeSuffix:@"prod"];
    
    DLog(@"%@",self.dic_productsList);
}

-(void)viewWillAppear:(BOOL)animated
{
    [self method_setHeading];
    [self method_setAddToCartCounterLabel];
}

-(void)viewWillDisappear:(BOOL)animated
{
    if(self.operation)
    {
        [self.operation cancel];
        self.operation = nil;
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - method_setAddToCartCounterLabel
-(void)method_setAddToCartCounterLabel
{
    UIView *view_counter;
    if (ApplicationDelegate.single.array_cartObjects)
    {
        if ([ApplicationDelegate.single.array_cartObjects count])
        {
            view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
            UILabel *label_counter = (UILabel *)[self.navigationController.view viewWithTag:888];
            [view_counter setHidden:NO];
            label_counter.text = [NSString stringWithFormat:@"%lu",(unsigned long)[ApplicationDelegate.single.array_cartObjects count]];
        }
        else
        {
            view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
            [view_counter setHidden:YES];
        }
        
    }
    else
    {
        view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
        [view_counter setHidden:YES];
    }
}

#pragma mark - method_updateUI
-(void)method_updateUI
{
    self.lable_name.text = [ApplicationDelegate.single.dic_productDetail valueForKey:k_name];
    
    if ([ApplicationDelegate.single.dic_productDetail valueForKey:k_priceValue] != nil)
    {
        self.lable_price.text = [NSString stringWithFormat:@"$ %@0", [ApplicationDelegate.single.dic_productDetail valueForKey:k_priceValue]];
    }
    else
    {
        self.lable_price.text = @"-";
    }
    
    array_products = [[NSArray alloc] initWithObjects:@"http://theluxhome.com/wp-content/uploads/2010/12/modern-comfortable-chair-with-pink-color-4.jpg", nil];
    [self method_settingHorizontalImages:array_products];
    [self method_addShareView];
    DLog(@"%@",[ApplicationDelegate.single.dic_productDetail valueForKey:k_fullImage]);
    
    
    if ([[ApplicationDelegate.single.dic_productDetail valueForKey:k_fullImage] rangeOfString:@".png"].location == NSNotFound)
    {
        [self.button_seeInYourHome setHidden:YES];
    } else {
        [self.button_seeInYourHome setHidden:NO];
    }
    
    if ([[ApplicationDelegate.single.dic_productDetail valueForKey:@"singleSKUUniqueID"] length])
    {
        [self.button_stock setTitle:@"In Stock\nAisle 38" forState:UIControlStateNormal];
    }
    else
    {
        [self.button_stock setBackgroundColor:[UIColor redColor]];
        [self.button_stock setTitle:@"Out of\nstock" forState:UIControlStateNormal];
    }
    
}

#pragma mark - get product detail
-(void)method_getProductDetail
{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    
    if (![self.operation isExecuting])
    {
        DLog(@"uniqueID: %@", self.uniqueID);
        
        self.operation = [ApplicationDelegate.webServiceEngine method_getProductDetail:[self method_digimarToWCS:self.uniqueID]
                                                                     completionHandler:^(id response)
                          {
                              
                              [SVProgressHUD dismiss];
                              
                              
                              if (response != nil)
                              {
                                  [self method_updateUI];
                                  
                                  
                                  DLog(@"%@", response);
//                                  
                                  DLog(@"%@", ApplicationDelegate.single.dic_productDetail);
                                  
                                  if ([[ApplicationDelegate.single.dic_productDetail valueForKey:k_dataModel] count])
                                  {
                                      self.SKUsLevelThree = [[ApplicationDelegate.single.dic_productDetail valueForKey:k_dataModel] objectAtIndex:0];
                                      
//                                      DLog(@"%@", self.SKUsLevelThree.Values);
//                                      DLog(@"%@", self.SKUsLevelThree.index);
//                                      DLog(@"%@", self.SKUsLevelThree.name);
//                                      DLog(@"%@", self.SKUsLevelThree.uniqueID);
//                                      DLog(@"%@", self.SKUsLevelThree.value);
                                      
                                      
                                      
                                      if ([self.SKUsLevelThree.Values count])
                                      {
                                          self.SKUsLevelTwo = [self.SKUsLevelThree.Values objectAtIndex:0];
                                          
//                                          DLog(@"%@", self.SKUsLevelTwo.SKUPriceValue);
//                                          DLog(@"%@", self.SKUsLevelTwo.SKUUniqueID);
//                                          DLog(@"%@", self.SKUsLevelTwo.Values);
                                          
                                          
                                          
                                          
//                                          self.SKUsLevelOne = [self.SKUsLevelTwo.Values objectAtIndex:0];
                                          
//                                          DLog(@"%@", self.SKUsLevelOne.identifier);
//                                          DLog(@"%@", self.SKUsLevelOne.identifierTwo);
//                                          DLog(@"%@", self.SKUsLevelOne.name);
//                                          DLog(@"%@", self.SKUsLevelOne.uniqueID);
//                                          DLog(@"%@", self.SKUsLevelOne.usage);
                                          
                                          
                                          
//                                          DLog(@"%@: %@", self.SKUsLevelThree.name, self.SKUsLevelThree.value);
//                                          
//                                          DLog(@"Price: %@", self.SKUsLevelTwo.SKUPriceValue);
//                                          
//                                          DLog(@"%@: %@", self.SKUsLevelOne.name, self.SKUsLevelOne.identifier);
//                                          
//                                          DLog(@"name: %@", self.SKUsLevelOne.identifierTwo);
                                      }
                                      
                                      
                                      
                                  }
                              }
                              else
                              {
                                  [[[UIAlertView alloc] initWithTitle:nil
                                                              message:@"There is something wrong, from the server side,please try again"
                                                             delegate:nil
                                                    cancelButtonTitle:@"Ok"
                                                    otherButtonTitles:nil, nil]
                                   show];
                              }
                          }
                                                                          errorHandler:^(NSError *error)
                          {
                              [SVProgressHUD dismiss];
                              
                              DLog(@"there is something is wrong, please try with valid id and pass");
                              DLog(@"1:%@\n2:%@\n3:%@\n4:%@",
                                   [error localizedDescription],
                                   [error localizedFailureReason],
                                   [error localizedRecoveryOptions],
                                   [error localizedRecoverySuggestion]);
                              
                              [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                                          message:@"There is something is wrong from server side"
                                                         delegate:nil
                                                cancelButtonTitle:@"Ok"
                                                otherButtonTitles:nil, nil]
                               show];
                          }
                          ];
    }
    
    [self.operation onDownloadProgressChanged:^(double progress)
     {
         DLog(@"download progress = %.2f", progress*100.0);
     }];
}



#pragma mark - image zoomer setting Up
-(void)method_imageZoomerSetting
{
    self.scroller.alwaysBounceVertical = YES;
    
    
    
    self.imageView_BackZoomer.frame = CGRectMake(k_origin_x,
                                                 k_origin_y,
                                                 self.view.frame.size.width,
                                                 self.view.frame.size.width);
    
    
    self.scroller.frame = CGRectMake(k_origin_x,
                                     k_origin_y,
                                     self.view.frame.size.width,
                                     self.view.frame.size.height - k_origin_y);
    
    
    self.view_scrollerContainer.frame = CGRectMake(0,
                                                   0,
                                                   self.view.frame.size.width,
                                                   self.view_scrollerContainer.frame.size.height);
    
    
    [self.scrollView_subScroller setFrame:CGRectMake(0,
                                                     0,
                                                     self.view.frame.size.width,
                                                     self.view.frame.size.width)];
    
    
    self.pageControl.frame = CGRectMake((self.view.frame.size.width/2) - (self.pageControl.frame.size.width/2),
                                        self.scrollView_subScroller.frame.size.height - (self.pageControl.frame.size.height + 10),
                                        self.pageControl.frame.size.width,
                                        self.pageControl.frame.size.width);
    
    
    
    
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"View_productDetail"
                                                         owner:self
                                                       options:nil];
    UIView *view_obj = nibContents[0];
    
    CGRect rect_new = CGRectMake(0,
                                 self.scrollView_subScroller.frame.size.height,
                                 self.view.frame.size.width,
                                 view_obj.frame.size.height);
    
    view_obj.frame = rect_new;
    
    [self.view_scrollerContainer addSubview:view_obj];
    
    [self.scroller setContentSize:CGSizeMake(0,
                                             view_obj.frame.origin.y + view_obj.frame.size.height)];
    
    
    
    
    //    [self.button_seeInYourHome setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.1]];
    //    self.button_seeInYourHome.layer.cornerRadius = 3.0;
    //    self.button_seeInYourHome.layer.borderWidth = 2.0;
    //    [self.button_seeInYourHome.layer setMasksToBounds:YES];
    
    
    
    //    self.view_productDetail.frame =
    
    
    [self.view_Share setFrame:CGRectMake(self.view_Share.frame.origin.x,
                                         self.scrollView_subScroller.frame.size.height,
                                         self.view_Share.frame.size.width,
                                         self.view_Share.frame.size.height)];
    
    //    [self.button_seeInYourHome setFrame:CGRectMake(self.view_Share.frame.origin.x - self.button_seeInYourHome.frame.size.width,
    //                                                   self.scrollView_subScroller.frame.size.height - self.button_seeInYourHome.frame.size.height-3,
    //                                                   self.button_seeInYourHome.frame.size.width,
    //                                                   self.button_seeInYourHome.frame.size.height)];
    
    
    ImageWidth = self.imageView_BackZoomer.frame.size.width;
    ImageHeight = self.imageView_BackZoomer.frame.size.height;
}





#pragma mark - action_AR
- (IBAction)action_AR:(id)sender
{
    YCameraViewController *obj =[[YCameraViewController alloc] initWithNibName:@"YCameraViewController" bundle:nil];
    
    obj.delegate = self;
    
    [self method_isHideHeader_BOOL:YES];

    DLog(@"%@", self.dic_productsList);
    obj.dic_productsList = self.dic_productsList;
    
    
    [self.navigationController pushViewController:obj animated:YES];
}



#pragma mark - method_hideSubScrollerToMaximizeTheVisibleImage
-(void)method_hideSubScrollerToMaximizeTheVisibleImage:(BOOL)isGoingToHide
{
    if (isGoingToHide == true)
    {
        [self.scrollView_subScroller setHidden:YES];
        
        // First find the visible image
        CGFloat pageWidth = self.scrollView_subScroller.frame.size.width;
        NSInteger page = (NSInteger)floor((self.scrollView_subScroller.contentOffset.x * 2.0f + pageWidth) / (pageWidth * 2.0f));
        
        // then pass the visible image to ImageView which is on the back
        
        if ([array_products count])
        {
//            [self.imageView_BackZoomer sd_setImageWithURL:[NSURL URLWithString:[array_products objectAtIndex:page]]
//                                         placeholderImage:[UIImage imageNamed:k_placeHolder]];
            
            NSString *string_imageURL = [NSString stringWithFormat:@"%@%@", k_imageURL, [ApplicationDelegate.single.dic_productDetail valueForKey:k_fullImage]];
            
            [self.imageView_BackZoomer sd_setImageWithURL:[NSURL URLWithString:string_imageURL]
                                         placeholderImage:[UIImage imageNamed:k_placeHolder]];
        }
    }
    else
    {
        [self.scrollView_subScroller setHidden:NO];
        self.imageView_BackZoomer.image =  nil;
    }
}



#pragma mark - scrollView delegates
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ([scrollView tag] == 1)
    {
        CGFloat yOffset   = self.scroller.contentOffset.y;
        
        
        
        if (yOffset < 0)
        {
            DLog(@"yOffset < %f", yOffset);
            
            [self method_hideSubScrollerToMaximizeTheVisibleImage:YES];
            
            
            
            CGFloat factor = ((ABS(yOffset)+ImageHeight)*ImageWidth)/ImageHeight;
            
            CGRect f = CGRectMake(-(factor-ImageWidth)/2,
                                  k_origin_y,
                                  factor,
                                  ImageHeight+ABS(yOffset));
            
            self.imageView_BackZoomer.frame = f;
            
            
        }
        else
        {
            DLog(@"yOffset > %f", yOffset);
            
            [self method_hideSubScrollerToMaximizeTheVisibleImage:NO];
            
            
            CGRect f = self.imageView_BackZoomer.frame;
            f.origin.y = -yOffset;
            self.imageView_BackZoomer.frame = f;
        }
    }
    else if([scrollView tag] == 2)
    {
        // Load the pages which are now on screen
        [self loadVisiblePages];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (self.scrollView_subScroller != scrollView)
        return;
    
    
    [self.button_seeInYourHome setEnabled:NO];
}


-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (self.scrollView_subScroller != scrollView)
        return;
    
//    self.productsDataClass_obj = (ProductsDataClass *)[array_products objectAtIndex:(long)self.pageControl.currentPage];
//    [self method_setValuesOnXIB:[array_products objectAtIndex:self.pageControl.currentPage]];
//    
//    
//    [self.button_seeInYourHome setEnabled:YES];
//    DLog(@"currentPage: %ld", (long)self.pageControl.currentPage);
}


#pragma mark - method_settingHorizontalImages
-(void)method_settingHorizontalImages:(NSArray *)images
{
    self.imageView_BackZoomer.image =  nil;
    
    // Set up the image we want to scroll & zoom and add it to the scroll view
    NSInteger pageCount = array_products.count;
    
    // Set up the page control
    self.pageControl.currentPage = 0;
    self.pageControl.numberOfPages = pageCount;
    
    // Set up the array to hold the views for each page
    self.pageViews = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < pageCount; ++i)
    {
        [self.pageViews addObject:[NSNull null]];
    }
    
    // Set up the content size of the scroll view
    CGSize pagesScrollViewSize = self.scrollView_subScroller.frame.size;
    self.scrollView_subScroller.contentSize = CGSizeMake(pagesScrollViewSize.width * array_products.count, 0);
    
    // Load the initial set of pages that are on screen
    [self loadVisiblePages];
}



#pragma mark -
#pragma mark - horizontal scroller
#pragma mark -
- (void)loadVisiblePages
{
    // First, determine which page is currently visible
    CGFloat pageWidth = self.scrollView_subScroller.frame.size.width;
    NSInteger page = (NSInteger)floor((self.scrollView_subScroller.contentOffset.x * 2.0f + pageWidth) / (pageWidth * 2.0f));
    
    // Update the page control
    self.pageControl.currentPage = page;
    
    //    DLog(@"_________currentPage: %ld", (long)self.pageControl.currentPage);
    
    // Work out which pages we want to load
    NSInteger firstPage = page - 1;
    NSInteger lastPage = page + 1;
    
    // Purge anything before the first page
    for (NSInteger i=0; i<firstPage; i++)
    {
        [self purgePage:i];
    }
    for (NSInteger i=firstPage; i<=lastPage; i++)
    {
        [self loadPage:i];
    }
    for (NSInteger i=lastPage+1; i<array_products.count; i++)
    {
        [self purgePage:i];
    }
}

- (void)loadPage:(NSInteger)page
{
    if (page < 0 || page >= array_products.count)
    {
        // If it's outside the range of what we have to display, then do nothing
        return;
    }
    
    // Load an individual page, first seeing if we've already loaded it
    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView == [NSNull null])
    {
        CGRect frame = self.scrollView_subScroller.bounds;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0.0f;


        UIImageView *imgView_temp = [[UIImageView alloc] initWithFrame:CGRectMake(frame.origin.x,
                                                                                  0,
                                                                                  self.view.frame.size.width,
                                                                                  self.view.frame.size.width)];
        __weak UIImageView *imgView = imgView_temp;
        
        if ([array_products count])
        {
            DLog(@"%@",array_products);
//            [imgView sd_setImageWithURL:[NSURL URLWithString:[array_products objectAtIndex:page]]
//                                         placeholderImage:[UIImage imageNamed:k_placeHolder]];
            
            NSString *string_imageURL = [NSString stringWithFormat:@"%@%@", k_imageURL, [ApplicationDelegate.single.dic_productDetail valueForKey:k_fullImage]];
            
            DLog(@"%@", string_imageURL);
            DLog(@"%@",self.pageViews);
            
            [imgView sd_setImageWithURL:[NSURL URLWithString:string_imageURL]
                       placeholderImage:[UIImage imageNamed:k_placeHolder]];
            
            
            
            
//            //cancel loading previous image for cell
//            [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:imgView];
//            
//            
//            //set placeholder image or cell won't update when image is loaded
//            imgView.image = [UIImage imageNamed:@"Placeholder.png"];
//            
//            //load the image
//            
//            self.productsDataClass_obj = [array_products objectAtIndex:page];
//            
//            imgView.imageURL = self.productsDataClass_obj.p_imageURL;
        }
        
        
        imgView.contentMode = UIViewContentModeScaleAspectFit;
        imgView.frame = CGRectMake(frame.origin.x,
                                   0,
                                   self.view.frame.size.width,
                                   self.view.frame.size.width);
        
        [imgView setBackgroundColor:[UIColor clearColor]];
        [self.scrollView_subScroller addSubview:imgView];
        [self.pageViews replaceObjectAtIndex:page withObject:imgView];
    }
}

- (void)purgePage:(NSInteger)page
{
    if (page < 0 || page >= array_products.count)
    {
        // If it's outside the range of what we have to display, then do nothing
        return;
    }
    
    // Remove a page from the scroll view and reset the container array
    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView != [NSNull null])
    {
        [pageView removeFromSuperview];
        [self.pageViews replaceObjectAtIndex:page withObject:[NSNull null]];
    }
}





#pragma mark - method_addShareView
-(void)method_addShareView
{
    [self.view_Share setBackgroundColor:[UIColor colorWithRed:112./255. green:47./255. blue:168./255. alpha:1.]];
    [self.view_Share.layer setCornerRadius:6.];
    [self changeDemo:nil];
}

-(void) changeDemo:(id)sender
{
    if(stack)
        [stack removeFromSuperview];
    
    stack = [[UPStackMenu alloc] initWithContentView:self.view_Share];
    DLog(@"%f",self.view_scrollerContainer.frame.size.height);
    DLog(@"%f",self.scroller.frame.size.height);
//    [stack setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width-40,
//                                 self.scrollView_subScroller.frame.size.height-self.view_Share.frame.size.height+20)];
    [stack setDelegate:self];
    
    UPStackMenuItem *facebookItem = [[UPStackMenuItem alloc] initWithImage:[UIImage imageNamed:@"facebook"] highlightedImage:nil title:@""];
    UPStackMenuItem *twitterItem = [[UPStackMenuItem alloc] initWithImage:[UIImage imageNamed:@"twitter"] highlightedImage:nil title:@""];
    UPStackMenuItem *instagramItem = [[UPStackMenuItem alloc] initWithImage:[UIImage imageNamed:@"Instagram"] highlightedImage:nil title:@""];
    UPStackMenuItem *pinterestItem = [[UPStackMenuItem alloc] initWithImage:[UIImage imageNamed:@"pinterest"] highlightedImage:nil title:@""];
    UPStackMenuItem *mailItem = [[UPStackMenuItem alloc] initWithImage:[UIImage imageNamed:@"mail"] highlightedImage:nil title:@""];
    NSMutableArray *items = [[NSMutableArray alloc] initWithObjects:facebookItem, twitterItem, instagramItem, pinterestItem, mailItem, nil];
    [items enumerateObjectsUsingBlock:^(UPStackMenuItem *item, NSUInteger idx, BOOL *stop) {
        [item setTitleColor:[UIColor whiteColor]];
    }];
    
    [stack setAnimationType:UPStackMenuAnimationType_progressive];
    [stack setStackPosition:UPStackMenuStackPosition_up];
    [stack setOpenAnimationDuration:.4];
    [stack setCloseAnimationDuration:.4];
    [items enumerateObjectsUsingBlock:^(UPStackMenuItem *item, NSUInteger idx, BOOL *stop) {
        [item setLabelPosition:UPStackMenuItemLabelPosition_right];
        [item setLabelPosition:UPStackMenuItemLabelPosition_left];
    }];

    [stack addItems:items];
    [self.view_scrollerContainer addSubview:stack];
    [self setStackIconClosed:YES];
}

- (void)setStackIconClosed:(BOOL)closed
{
    UIImageView *icon = [[self.view_Share subviews] objectAtIndex:0];
    float angle = closed ? 0 : (M_PI * (135) / 180.0);
    [UIView animateWithDuration:0.3 animations:^{
        [icon.layer setAffineTransform:CGAffineTransformRotate(CGAffineTransformIdentity, angle)];
    }];
}

#pragma mark - UPStackMenuDelegate

- (void)stackMenuWillOpen:(UPStackMenu *)menu
{
    if([[self.view_Share subviews] count] == 0)
        return;
    
    [self setStackIconClosed:NO];
}

- (void)stackMenuWillClose:(UPStackMenu *)menu
{
    if([[self.view_Share subviews] count] == 0)
        return;
    
    [self setStackIconClosed:YES];
}
#pragma mark - action_SocialSharing
- (void)stackMenu:(UPStackMenu *)menu didTouchItem:(UPStackMenuItem *)item atIndex:(NSUInteger)index
{
    NSArray *array_temp = self.scrollView_subScroller.subviews;
    UIImageView *imageView_temp;
    
    for (id x in array_temp)
    {
        if ([x isKindOfClass:[UIImageView class]])
        {
            imageView_temp = x;
        }
    }
    //facebook
    if (index == 0)
    {
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
        {
            SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
            [controller setInitialText:self.lable_name.text];
            [controller addURL:[NSURL URLWithString:@"http://www.royalcyber.com/"]];
            [controller addImage:imageView_temp.image];
            [self presentViewController:controller animated:YES completion:Nil];
        }
        else
        {
            
            [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                        message:@"Facebook account not found. Go to the Settings application to add your Facebook account to this device."
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil, nil]
             show];
        }
    }
    //Twitter
    else if (index == 1)
    {
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
        {
            SLComposeViewController *tweetSheet = [SLComposeViewController
                                                   composeViewControllerForServiceType:SLServiceTypeTwitter];
            [tweetSheet setInitialText:self.lable_name.text];
            [tweetSheet addURL:[NSURL URLWithString:@"http://www.royalcyber.com/"]];
            [tweetSheet addImage:imageView_temp.image];
            [self presentViewController:tweetSheet animated:YES completion:nil];
        }
        else
        {
            
            [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                        message:@"Twitter account not found. Go to the Settings application to add your Twitter account to this device."
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil, nil]
             show];
        }
    }
    //google plus
    else if (index == 2)
    {
        if ([MGInstagram isAppInstalled])
        {
            [MGInstagram postImage:imageView_temp.image withCaption:self.lable_name.text inView:self.view];
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:@"Alert"
                                        message:@"Instagram must be installed on the device in order to post photo"
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil, nil]
             show];
        }
    }
    //pinterest
    else if (index == 3)
    {
        [_pinterest createPinWithImageURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",k_url,[ApplicationDelegate.single.dic_productDetail valueForKey:k_fullImage]]] sourceURL:[NSURL URLWithString:@"http://www.royalcyber.com/"] description:self.lable_name.text];
    }
    //email
    else
    {
        if([MFMailComposeViewController canSendMail])
        {
            [SVProgressHUD show];
            MFMailComposeViewController *emailDialog = [[MFMailComposeViewController alloc] init];
            emailDialog.mailComposeDelegate = self;
            [emailDialog setSubject:self.lable_name.text];
            [emailDialog setMessageBody:self.lable_name.text isHTML:NO];
            UIImage * image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",k_url,[ApplicationDelegate.single.dic_productDetail valueForKey:k_fullImage]]]]];
            [emailDialog addAttachmentData:UIImageJPEGRepresentation(image, 1) mimeType:@"image/jpeg" fileName:@"product.jpeg"];
            
            [self presentViewController:emailDialog animated:YES completion:^(void)
             {
                 [SVProgressHUD dismiss];
                 NSLog(@"composer open");
             }];
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:@"Alert"
                                        message:@"Your Email account is not configured on device. Please configure your email settings in device settings."
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil, nil]
             show];
        }
    }

}
#pragma mark- mailComposer Delegates
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    // Notifies users about errors associated with the interface
    switch (result)
    {
            
        case MFMailComposeResultCancelled:
            NSLog(@"%@",@"Result: canceled") ;
            break;
        case MFMailComposeResultSaved:
            NSLog(@"%@",@"Result: saved");
            break;
        case MFMailComposeResultSent:
        {
            NSLog(@"%@",@"Result: Sent");
            break;
        }
        case MFMailComposeResultFailed:
            NSLog(@"%@",@"Result: Failed");
            break;
        default:
            NSLog(@"%@",@"Result: not sent");
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - product detail view

#pragma mark - action_productInformation
- (IBAction)action_productInformation:(id)sender
{
    [self performSegueWithIdentifier:@"segue_productInfo" sender:nil];
}

#pragma mark - action_rating
- (IBAction)action_rating:(id)sender
{
    [self performSegueWithIdentifier:@"segue_reviews" sender:nil];
}


#pragma mark - action_addToCart
- (IBAction)action_addToCart:(id)sender
{
    if ([self.label_quantity.text length])
    {
    if ([[ApplicationDelegate.single.dic_productDetail valueForKey:@"singleSKUUniqueID"] length])
        {
        //no combination
            NSArray *array_temp = [NSArray arrayWithObjects:
                                   [NSDictionary dictionaryWithObjectsAndKeys:
                                    [ApplicationDelegate.single.dic_productDetail valueForKey:@"singleSKUUniqueID"],@"productId",
                                    self.label_quantity.text,@"quantity", nil], nil];
            
            
            [self method_postByURL:[NSDictionary dictionaryWithObjectsAndKeys:
                                    ApplicationDelegate.single.string_orderId?
                                    ApplicationDelegate.single.string_orderId:@".",@"orderId",
                                    array_temp, @"orderItem",
                                    @"0",@"x_calculateOrder",
                                    @"true",@"x_inventoryValidation",
                                    nil]];
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:nil
                                        message:@"Out of stock"
                                       delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil, nil]
             show];
        }
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:@"Enter Quantity"
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
    }
}




#pragma mark -
#pragma mark -  POST by NSURL Session

-(void)method_postByURL:(NSDictionary *)dic
{
    DLog(@"%@",dic);
    if (ApplicationDelegate.single.string_WCToken)              // it means to check either guest user is logged in?
    {
        NSString *strUrl = [NSString stringWithFormat:@"%@/wcs/resources/store/10001/cart?responseFormat=json",k_url];
        
        DLog(@"strUrl: %@",strUrl);
        DLog(@"%@", dic);
        
        strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        
        if (!self.nsurl_obj)
        {
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
            
            self.nsurl_obj = [[webService_Post_Put_Delete alloc] initWithURL:strUrl
                                                                        data:dic
                                                                  seriveType:@"POST"
                                                                  seriveName:@"addToCart"];
            self.nsurl_obj.delegate = self;
        }
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Sorry for inconvenience"
                                    message:@"Please try later"
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
    }
}




#pragma mark - method_precheckout
-(void)method_precheckout
{
    if (!self.nsurl_obj)
    {
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
        
        
        NSString *strUrl = [NSString stringWithFormat:@"%@/wcs/resources/store/10001/cart/@self/precheckout",k_url];
        // pre checkoout
        DLog(@"strUrl: %@",strUrl);
        
        strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        
        DLog(@"%@",ApplicationDelegate.single.string_orderId);
        self.nsurl_obj = [[webService_Post_Put_Delete alloc] initWithURL:strUrl
                                                                    data:[NSDictionary dictionaryWithObjectsAndKeys:ApplicationDelegate.single.string_orderId,@"orderId", nil]
                                                              seriveType:@"PUT"
                                                              seriveName:@"precheckoout"];
        self.nsurl_obj.delegate = self;
    }
}


#pragma mark - method_checkout
-(void)method_checkout
{
    if (!self.nsurl_obj)
    {
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
        
        
        NSString *strUrl = [NSString stringWithFormat:@"%@/wcs/resources/store/10001/cart/@self/checkout",k_url];
        // pre checkoout
        DLog(@"strUrl: %@",strUrl);
        
        strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        
        
        self.nsurl_obj = [[webService_Post_Put_Delete alloc] initWithURL:strUrl
                                                                    data:[NSDictionary dictionaryWithObjectsAndKeys:ApplicationDelegate.single.string_orderId,@"orderId", nil]
                                                              seriveType:@"POST"
                                                              seriveName:@"checkOut"];
        self.nsurl_obj.delegate = self;
    }
}




#pragma mark - NSURLResponseDelegate
-(void)delegate_outPut:(NSData *)data urlRes:(NSURLResponse *)urlRes error_:(NSError *)error_ seriveName:(NSString *)serivename
{
    self.nsurl_obj.delegate = nil;
    self.nsurl_obj = nil;
    [SVProgressHUD dismiss];
    
    
    if (error_ == nil)
    {
        // Convert JSON Object into Dictionary
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:data options:
                              NSJSONReadingMutableContainers error:&error_];
       
        DLog(@"%@",JSON);
        DLog(@"%@", ApplicationDelegate.single.string_WCToken);
        DLog(@"%@", ApplicationDelegate.single.string_WCTrustedToken);
        DLog(@"%@",ApplicationDelegate.single.dic_productDetail);
//        if ([serivename isEqualToString:@"addToCart"])
//        {
//            DLog(@"%@",JSON);
//        }
//        else if ([serivename isEqualToString:@"precheckoout"])
//        {
//             DLog(@"%@",JSON);
//        }
//        else
//        {
//             DLog(@"%@",JSON);
//        }
//
        
       
        
        
        
        
        
        
        
//        if ([serivename isEqualToString:@"addToCart"])
//        {
            if ([JSON valueForKey:@"orderId"])
            {
                [[[UIAlertView alloc] initWithTitle:nil
                                            message:@"Successfully added"
                                           delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles:nil, nil]
                 show];
                
                if (!ApplicationDelegate.single.string_orderId)
                {
                    ApplicationDelegate.single.string_orderId = [JSON valueForKey:@"orderId"];
                }
                if (![ApplicationDelegate.single.array_cartObjects count])
                {
                    ApplicationDelegate.single.array_cartObjects =[[NSMutableArray alloc] init];
                }
                [ApplicationDelegate.single.array_cartObjects addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:[ApplicationDelegate.single.dic_productDetail valueForKey:@"name"],@"name",self.label_quantity.text,@"quantity",[ApplicationDelegate.single.dic_productDetail valueForKey:@"thumbnail"],@"thumbnail",[ApplicationDelegate.single.dic_productDetail valueForKey:@"priceValue"],@"priceValue",[ApplicationDelegate.single.dic_productDetail valueForKey:@"singleSKUUniqueID"],@"singleSKUUniqueID",[[[JSON valueForKey:@"orderItem"] objectAtIndex:0] valueForKey:@"orderItemId"], @"orderItemId", nil]];
                [self method_setAddToCartCounterLabel];
                
//                 [self method_precheckout];
//                [self performSelector:@selector(method_checkout) withObject:nil afterDelay:2];
                DLog(@"%@",ApplicationDelegate.single.array_cartObjects);
                DLog(@"%@",ApplicationDelegate.single.string_orderId);
            }
            else
            {
                if ([JSON valueForKey:@"errors"])
                {
                    [[[UIAlertView alloc] initWithTitle:nil message:[[[JSON valueForKey:@"errors"] objectAtIndex:0] valueForKey:@"errorMessage"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                }
                else
                {
                    [[[UIAlertView alloc] initWithTitle:nil
                                                message:@"Item does not added, please try later"
                                               delegate:nil
                                      cancelButtonTitle:@"Ok"
                                      otherButtonTitles:nil, nil]
                     show];
                }
            }

        }
  //  }
    else
    {
        DLog(@"%@", error_);
        
        
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:@"Item does not added, please try later"
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
        
    }
}


#pragma mark - NSURL Sessoin delegates
- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error
{
    DLog(@"didBecomeInvalidWithError");
}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
 completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler
{
    DLog(@"didReceiveChallenge");
}


- (void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session NS_AVAILABLE_IOS(7_0)
{
    DLog(@"URLSessionDidFinishEventsForBackgroundURLSession");
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_reviews"])
    {
        VC_productInformation *obj = (VC_productInformation *)segue.destinationViewController;
        obj.bool_isReviews = true;
    }
}


#pragma mark - set corner radius
-(void)method_setCornerRadius
{
    self.button_addToCart.layer.cornerRadius = k_cornerRadiusButton;
    self.button_addToList.layer.cornerRadius = k_cornerRadiusButton;
    self.button_stock.layer.cornerRadius = k_cornerRadiusButton;
}

#pragma mark - resetValuesOnNib
-(void)method_resetValuesOnNib
{
    NSArray *array_temp =[self.scrollView_subScroller subviews];
    
    [array_temp enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
     {
         [obj removeFromSuperview];
     }];
    
    self.imageView_BackZoomer.image = nil;
    self.lable_name.text = @"-";
    self.lable_price.text = @"-";
}

#pragma mark - ARSeletedImageDelegate
-(void)GetARSeletedImageDetail:(NSString *)productId
{
    self.uniqueID = productId;
    [self method_resetValuesOnNib];
    [self method_getProductDetail];
}



#pragma mark - method_hideandShowHeader_BOOL
-(void)method_isHideHeader_BOOL:(BOOL)bool_
{
//    NSArray *array_temp = self.navigationController.view.subviews;
//    
//    [array_temp enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
//        DLog(@"%ld", (long)[obj tag]);
//    }];
    
    

    if ([self.navigationController.view viewWithTag:3])
    {
        [[self.navigationController.view viewWithTag:3] setHidden:bool_];
    }
    
    
    
    

    if ([self.navigationController.view viewWithTag:5])
    {
        [[self.navigationController.view viewWithTag:5] setHidden:bool_];
    }

}

#pragma mark - top menu Actions

- (IBAction)action_ShoppingCart:(id)sender
{
    if (ApplicationDelegate.single.array_cartObjects)
    {
        ApplicationDelegate.single.string_controllerName = k_shoppingCart;
        
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:@"Cart is empty"
                                   delegate:nil
                          cancelButtonTitle:@"ok"
                          otherButtonTitles:nil, nil]
         show];
    }
}



#pragma mark - action_chat
- (IBAction)action_chat:(id)sender
{
    [self performSegueWithIdentifier:@"segue_chat" sender:nil];
}

#pragma mark - actionpickUpStore
- (IBAction)actionpickUpStore:(id)sender
{
    [self performSegueWithIdentifier:@"segue_pickupStore" sender:nil];
}

#pragma mark - unwindToProductDetail
- (IBAction)unwindToProductDetail:(UIStoryboardSegue *)unwindSegue
{
    VC_pickupStore *objVC_pickupStore = (VC_pickupStore *)unwindSegue.sourceViewController;
    DLog(@"%@", objVC_pickupStore.storeName);
    self.label_storeName.text = objVC_pickupStore.storeName;
}


@end