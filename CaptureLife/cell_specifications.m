#import "cell_specifications.h"

@implementation cell_specifications

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)method_setValues:(NSDictionary *)dic
{
    self.label_type.text = [dic valueForKey:k_name];
    self.label_description.text = [dic valueForKey:k_values];
}

@end