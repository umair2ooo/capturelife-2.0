#import "VC_productInformation.h"

#import "DVSwitch.h"
#import "cell_specifications.h"
#import "cell_reviews.h"
#import "NSString+HTML.h"

#define k_numberOfPages 3

@interface VC_productInformation ()<UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate>
{
}

@property (strong, nonatomic) DVSwitch *switcher;
@property(nonatomic, strong)NSMutableArray *pageViews;


@property (weak, nonatomic) IBOutlet UIView *view_topBack;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView_subScroller;
@property (weak, nonatomic) IBOutlet UILabel *lable_name;
@property (weak, nonatomic) IBOutlet UILabel *lable_price;
@property (weak, nonatomic) IBOutlet UIImageView *imageView_productImage;


#pragma mark - view overView
@property (strong, nonatomic) IBOutlet UIView *view_Overview;
@property (weak, nonatomic) IBOutlet UILabel *label_description;
@property (weak, nonatomic) IBOutlet UIScrollView *scroller_overView;


#pragma mark - view specifications
@property (strong, nonatomic) IBOutlet UIView *view_specifications;
@property (weak, nonatomic) IBOutlet UITableView *tableView_specifications;

#pragma mark - view reviews
@property (strong, nonatomic) IBOutlet UIView *view_reviews;
@property (weak, nonatomic) IBOutlet UITableView *tableView_reviews;

@end



@implementation VC_productInformation

@synthesize bool_isReviews = _bool_isReviews;

#pragma mark - add views here
-(void)method_addViewsFromNibs
{
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"View_productInfo_Overview"
                                                         owner:self
                                                       options:nil];
    UIView *view_obj = nibContents[0];
    
    

    
    CGRect rect_new = CGRectMake(0,
                                 0,
                                 self.view.frame.size.width,
                                 view_obj.frame.size.height);
    
    view_obj.frame = rect_new;
    
    [self.scrollView_subScroller addSubview:view_obj];
    
    
    nibContents = nil;
    view_obj = nil;
    
    
    
    
    
    
    
    nibContents = [[NSBundle mainBundle] loadNibNamed:@"View_productInfo_specifications"
                                                         owner:self
                                                       options:nil];
    view_obj = nibContents[0];
    
    rect_new = CGRectMake(self.scrollView_subScroller.frame.size.width,
                          0,
                          self.view.frame.size.width,
                          self.scrollView_subScroller.frame.size.height);
    
    view_obj.frame = rect_new;
    
    [self.scrollView_subScroller addSubview:view_obj];
    
    
    nibContents = nil;
    view_obj = nil;
    
    
    
    
    
    nibContents = [[NSBundle mainBundle] loadNibNamed:@"View_productInfo_reviews"
                                                         owner:self
                                                       options:nil];
    view_obj = nibContents[0];
    
//    rect_new = CGRectMake(self.scrollView_subScroller.frame.size.width*2,
//                          0,
//                          self.view.frame.size.width,
//                          view_obj.frame.size.height);
//    
//    view_obj.frame = rect_new;

    
    rect_new = CGRectMake(self.scrollView_subScroller.frame.size.width*2,
                          0,
                          self.view.frame.size.width,
                          self.scrollView_subScroller.frame.size.height);
    
    view_obj.frame = rect_new;
    
    
    
    [self.scrollView_subScroller addSubview:view_obj];
    
    
    
    
    
    
    [self.scrollView_subScroller setContentSize:CGSizeMake(self.scrollView_subScroller.frame.size.width * 3, 0)];
    
    
    
//    [self method_setVariableHeightOFViews];
}



#pragma mark - add switch to view
-(void)method_addSwitch
{
    NSInteger margin = 20;
    
    self.switcher = [[DVSwitch alloc] initWithStringsArray:@[@"Overview", @"Specifications", @"Reviews"]];
    
    self.switcher.frame = CGRectMake(margin,
                                     self.view_topBack.frame.origin.y+self.view_topBack.frame.size.height + margin,
                                     self.view.frame.size.width - margin * 2,
                                     30);
    
    self.switcher.labelTextColorOutsideSlider = [UIColor blackColor];
    self.switcher.cornerRadius = 2.0;
    self.switcher.labelTextColorInsideSlider = [UIColor whiteColor];
    self.switcher.backgroundColor = [UIColor whiteColor];
    
    self.switcher.sliderColor = k_RGB(k_R, k_G, k_B);
    [self.view addSubview:self.switcher];
    
    
    
    __block UIScrollView *scroller_temp = self.scrollView_subScroller;
    
    
    __weak typeof(self) weakSelf = self;
    
    [self.switcher setPressedHandler:^(NSUInteger index) {
        
        NSLog(@"Did press position on first switch at index: %lu", (unsigned long)index);
        
        switch (index)
        {
            case 0:                                                                                             //  view_Overview
                [scroller_temp setContentOffset:CGPointMake(0, 0) animated:NO];
                
//                [scroller_temp setContentSize:CGSizeMake(0,
//                                                         weakSelf.view_Overview.frame.size.height)];
                break;
                
            case 1:                                                                                             //  view_specifications
                [scroller_temp setContentOffset:CGPointMake(weakSelf.view_specifications.frame.origin.x, 0) animated:NO];
                
//                [scroller_temp setContentSize:CGSizeMake(scroller_temp.frame.size.width - scroller_temp.frame.size.width * 2,
//                                                         weakSelf.view_specifications.frame.size.height)];
                break;
                
            case 2:                                                                                             //  view_reviews
                [scroller_temp setContentOffset:CGPointMake(weakSelf.view_reviews.frame.origin.x, 0) animated:NO];
                
//                [scroller_temp setContentSize:CGSizeMake(scroller_temp.frame.size.width * 2 - scroller_temp.frame.size.width * 3,
//                                                         weakSelf.view_reviews.frame.size.height)];
                break;
                
            default:
                break;
        }
        

        
        

        DLog(@"view_reviews.frame.size.width: %f", weakSelf.view_reviews.frame.size.width*2);
        
        DLog(@"contentOffset.x: %f", scroller_temp.contentOffset.x);
        DLog(@"contentOffset.y: %f", scroller_temp.contentOffset.y);
        
        
    }];
}




#pragma mark - set heading
-(void)method_setHeading
{
    UILabel *label_heading = (UILabel *)[self.navigationController.view viewWithTag:999];
    
    [label_heading setText:@"Products Information"];
}


#pragma mark - cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    [self method_addViewsFromNibs];
    
    [self method_addSwitch];
    
    [self method_setVariableHeightOFViews];
}


-(void)viewWillAppear:(BOOL)animated
{
    if (self.bool_isReviews)
    {
        self.bool_isReviews = false;
        [self.switcher forceSelectedIndex:2 animated:YES];
    }
    
    
    [self method_setHeading];
    [self method_setAddToCartCounterLabel];
}

#pragma mark - method_setAddToCartCounterLabel
-(void)method_setAddToCartCounterLabel
{
    UIView *view_counter;
    if (ApplicationDelegate.single.array_cartObjects)
    {
        if ([ApplicationDelegate.single.array_cartObjects count])
        {
            view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
            UILabel *label_counter = (UILabel *)[self.navigationController.view viewWithTag:888];
            [view_counter setHidden:NO];
            label_counter.text = [NSString stringWithFormat:@"%lu",(unsigned long)[ApplicationDelegate.single.array_cartObjects count]];
        }
        else
        {
            view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
            [view_counter setHidden:YES];
        }
        
    }
    else
    {
        view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
        [view_counter setHidden:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/





#pragma mark - Overview Section
#pragma mark - variable Height of UILable and UIVew
-(void)method_setVariableHeightOFViews
{
    self.lable_name.text = [ApplicationDelegate.single.dic_productDetail valueForKey:k_name];
    self.lable_price.text = [NSString stringWithFormat:@"$ %@0", [ApplicationDelegate.single.dic_productDetail valueForKey:k_priceValue]];
    
    
    NSURL *url_ = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", k_imageURL, [ApplicationDelegate.single.dic_productDetail valueForKey:k_fullImage]]];
    [self.imageView_productImage sd_setImageWithURL:url_ placeholderImage:[UIImage imageNamed:k_placeHolder]];

    
    
    CGRect frame = self.label_description.frame;
    
    
    float height = [ApplicationDelegate.single getHeightForText:[ApplicationDelegate.single.dic_productDetail valueForKey:k_longDescription]
                                                       withFont:self.label_description.font
                                                       andWidth:self.label_description.frame.size.width];
    
    self.label_description.frame = CGRectMake(frame.origin.x,
                                        frame.origin.y,
                                        frame.size.width,
                                        height);
    
    [self.label_description setBackgroundColor:[UIColor clearColor]];
    
    
    self.label_description.numberOfLines = (int)(height/self.label_description.font.leading);
    NSString *string_removeHtmlSpecialTags = [[ApplicationDelegate.single.dic_productDetail valueForKey:k_longDescription]  kv_decodeHTMLCharacterEntities];
//    self.label_description.text = [[ApplicationDelegate.single.dic_productDetail valueForKey:k_longDescription]  kv_decodeHTMLCharacterEntities];
    self.label_description.text = [string_removeHtmlSpecialTags kv_stringByStrippingHTML:string_removeHtmlSpecialTags];
    
    CGRect rect_new = self.view_Overview.frame;
    
    rect_new.size.height = self.label_description.frame.origin.y + self.label_description.frame.size.height;
    
    self.view_Overview.frame = rect_new;
    

    
    
    [self.scroller_overView setContentSize:CGSizeMake(0, self.view.frame.size.height-self.view_Overview.frame.size.height+100)];
    [self.scroller_overView setScrollEnabled:YES];
    
//    [self.scrollView_subScroller setContentSize:CGSizeMake(0, self.view_Overview.frame.size.height)];
}

#pragma mark -stringByStrippingHTML
-(NSString *)stringByStrippingHTML:(NSString*)str
{
    //[str kv_decodeHTMLCharacterEntities];
    NSRange r;
    while ((r = [str rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location     != NSNotFound)
    {
        str = [str stringByReplacingCharactersInRange:r withString:@""];
    }
    
    str = [ str stringByReplacingOccurrencesOfString:@"&amp;" withString: @"&"  ];
    
    str = [ str stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""  ];
    
    str = [ str stringByReplacingOccurrencesOfString:@"&#x27;" withString:@"'"  ];
    
    str = [ str stringByReplacingOccurrencesOfString:@"&#x39;" withString:@"'"  ];
    
    str = [ str stringByReplacingOccurrencesOfString:@"&#x92;" withString:@"'"  ];
    
    str = [ str stringByReplacingOccurrencesOfString:@"&#x96;" withString:@"'"  ];
    
    str = [ str stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"  ];
    
    str = [ str stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"  ];
    
    return str;
}

#pragma mark - Specifications Section
#pragma mark - tableview delegates and data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.tableView_specifications)
    {
        return [[ApplicationDelegate.single.dic_productDetail valueForKey:k_ProductInfo] count];
    }
    else
    {
        return 3;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.tableView_specifications == tableView)
    {
        cell_specifications *cell = (cell_specifications *)[tableView dequeueReusableCellWithIdentifier:@"cell_specifications"];
        
        if (cell == nil)
        {
            // Load the top-level objects from the custom cell XIB.
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"cell_specifications" owner:self options:nil];
            // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
            cell = [topLevelObjects objectAtIndex:0];
        }
        
        if (indexPath.row %2 != 0)
        {
            [cell setBackgroundColor:[k_RGB(k_R, k_G, k_B) colorWithAlphaComponent:k_colorWithAlphaCell]];
        }
        
        
        [cell method_setValues:[[ApplicationDelegate.single.dic_productDetail valueForKey:k_ProductInfo] objectAtIndex:indexPath.row]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        return cell;
    }
    else
    {
        cell_reviews *cell = (cell_reviews *)[tableView dequeueReusableCellWithIdentifier:@"cell_reviews"];
        
        if (cell == nil)
        {
            // Load the top-level objects from the custom cell XIB.
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"cell_reviews" owner:self options:nil];
            // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
            cell = [topLevelObjects objectAtIndex:0];
        }
        
//        if (indexPath.row %2 != 0)
//        {
//            [cell setBackgroundColor:k_RGB(238, 238, 238)];
//        }
//        else
//        {
//            [cell setBackgroundColor:[UIColor clearColor]];
//        }
        
        
        [cell method_setValues:nil];
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        return cell;
    }
}



#pragma mark - scroll view delegates
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (![scrollView isKindOfClass:[UITableView class]])
    {
        int page = scrollView.contentOffset.x / scrollView.frame.size.width;
        DLog(@"page: %d", page);
        
        
        [self.switcher forceSelectedIndex:page animated:YES];
    }
}

@end