@interface JSONParser : NSObject

- (id)initWithJSON:(id)json;

-(NSDictionary *)method_parseJSON:(id)json;
@end