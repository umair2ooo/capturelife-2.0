#import "ViewController.h"
#import "VC_ProductDetail.h"


#import "CrosshairsView.h"
// DMSDK Integration
//
#import "DMSManager.h"
#import "DMPayload.h"
#import "DMPayload+Internals.h"
#import "DMResolver.h"
#import "DMResolverSettings.h"


#import "PayloadItem.h"
#import "PayoffDoc.h"


#define kMyApplicationName          @"DMSDemo"

#define kMyViewMode_Config          0
#define kMyViewMode_Camera          1
#define kMyViewMode_Errors          2
#define kMyViewMode_Warnings        3
#define kMyViewMode_Payoffs         4
#define kMyViewMode_Payloads        5
#define kMyViewMode_Index           6



@class DMSManager;
@class DMSImageCameraSource;
@class DMSAudioMicAQSource;


@implementation warningItem
@end


@interface ViewController ()
{
    int payloadItemNumber;
    
    BOOL bool_viewDisappeared;
}

@property(nonatomic, strong)AVAudioPlayer *audioPlayer;


@property (weak, nonatomic) IBOutlet UIView *view_top;

- (IBAction)action_cancel:(id)sender;

@property (retain) NSString* name;

// DMSDemo main UI view ...
//
@property (retain)  AVCaptureVideoPreviewLayer * previewLayer;

@property (retain)  NSDateFormatter* dateFormatter;

@property (retain)  CrosshairsView *crosshairs;


// DMSManager image/audio profile settings cache, used to save/restore profiles when temporarily setting to Idle for an alert view ...
@property           NSUInteger lastAudioProfile;
@property           NSUInteger lastImageProfile;

// DMSManager interfaces
//
- (void) initializeDms;
- (void) startDMSSources;
- (void) stopDMSSources;


- (IBAction)action_torch:(id)sender;


#pragma mark - top menu actions
@property (weak, nonatomic) IBOutlet UILabel *label_heading;

- (IBAction)action_ShoppingCart:(id)sender;
- (IBAction)action_menu:(id)sender;
- (IBAction)action_hideMenu:(id)sender;
- (IBAction)action_back:(id)sender;
- (IBAction)action_home:(id)sender;

#pragma mark - full menu actions
- (IBAction)action_logOut:(id)sender;
- (IBAction)action_storeLocator:(id)sender;
- (IBAction)action_scan:(id)sender;
- (IBAction)action_barCode:(id)sender;
- (IBAction)action_viewLoyalPoints:(id)sender;
- (IBAction)action_signIn:(id)sender;



@end

@implementation ViewController


#pragma mark - torch light on off
- (void)turnLightOn:(BOOL)on
{
    AVCaptureDevice *flashLight = [self flashLight];
    if ([flashLight lockForConfiguration:nil])
    {
        [flashLight setTorchMode:[flashLight isTorchActive] ? AVCaptureTorchModeOff : AVCaptureTorchModeOn];
        [flashLight unlockForConfiguration];
    }
}

- (AVCaptureDevice *)flashLight
{
    AVCaptureDevice *flashLight = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if (![flashLight isTorchAvailable] ||
        ![flashLight isTorchModeSupported:AVCaptureTorchModeOn])
    {
        flashLight = nil;
    }
    return flashLight;
}

- (IBAction)action_torch:(id)sender
{
    [self turnLightOn:YES];
}



#pragma mark - Misc UI and status methods
- (void) addCameraPreview
{
    self.previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.imageCameraSource.captureSession];
    
    CALayer *viewLayer = [self.cameraPreviewContainer layer];
    
    CGRect bounds = self.cameraPreviewContainer.bounds;
    [self.previewLayer setFrame:bounds];
    [self.previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    [viewLayer addSublayer:self.previewLayer];
}

- (void) removeCameraPreview
{
    if(self.previewLayer) {
        [self.previewLayer removeFromSuperlayer];
        self.previewLayer = nil;
    }
}


-(void) method_performActionsAfterDelay
{
    [self.view bringSubviewToFront:self.view_top];
}


#pragma mark - DMS
- (void) handleWarningFrom:(NSString*)source message:(NSString*)msg
{
    
    warningItem* item = [[warningItem alloc] init];
    item.date = [NSDate date];
    item.source = source;
    item.message = msg;
    
//    [warningItems addObject:item];
}

- (void) handleError:(NSError*)error
{
//    [errorItems addObject:error];
    
    // TODO:  A future SDK distribution will move this error parsing up into DMSManager,
    //        defining a small number of error codes all in the DMSManagerErrorDomain.
    //
    NSString* title = @"Error Reported";
    NSString* message = @"";
    
    if([error.domain isEqualToString:@"NSURLErrorDomain"]) {
        title = @"Network Error";
        switch (error.code) {
            case -1003: message = @"Could not access resolver server.";  break;
            case -1009: message = @"Could not access network.  Please check your network settings and try again.";  break;
        }
    } else if([error.domain isEqualToString:@"DMHttpRequestErrorDomain"]) {
        title = @"Resolver Error";
        switch (error.code) {
            case 401:
            case 403: message = @"Invalid resolver username/password credentials."; break;
            case 404: message = @"Resource not found on this server."; break;
        }
    } else if([error.domain isEqualToString:DigimarcResolverErrorDomain]) {
        switch (error.code) {
            case kDMResolverErrorUnknownPayloadType:
                title = @"Resolver Error";
                message = @"Unknown payload type.";
                break;
                
                // No need to show this error to user, just return
            case kDMResolverErrorRequestCanceled:
                return;
        }
    }
    
    [self setReadersToIdle];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil
                          ];
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [self.dmsManager clearImagePayloadCache];
    [self.dmsManager clearAudioPayloadCache];
    [self setReadersToOn];
}


/****************************************************************************************
 *
 *    DMSManager Interfaces
 *
 ****************************************************************************************/

#pragma mark - Initialize DMSDK and DMResolver

- (void) initializeDms
{
    
    /** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Create and Initialize DMSManager] */
    // create DMSManager
    //
    self.dmsManager = [DMSManager sharedDMSManager];
    self.dmsManager.delegate = self;
    
    // Default cache depth is 5; setting it here to 1 so we will receive new payload notification
    // any time the payload value changes, but not for repeat detections of the same payload value.
    // This pattern works best for the DMSDemo sample UI payoffs history table reordering functionality.
    //
    self.dmsManager.imagePayloadCacheMaxDepth = 1;
    self.dmsManager.audioPayloadCacheMaxDepth = 1;
    
    // Setup the optional reporting and logging criteria
    //
    self.dmsManager.reportOnlyNewDetections = NO;
    self.dmsManager.reportOptionalReaderInfoDiagnostics = YES;
    
    self.dmsManager.logImageReaderDispatches = NO;
    self.dmsManager.logImageReaderDetections = NO;
    self.dmsManager.logImageReaderDetectionReaderInfo = NO;
    
    self.dmsManager.logAudioReaderDispatches = NO;
    self.dmsManager.logAudioReaderDetections = NO;
    self.dmsManager.logAudioReaderDetectionReaderInfo = NO;
    /** - - - - - - - - - - - < < < - - - - - - - - - - - - - [Create and Initialize DMSManager] */
    
    /** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Create and Initialize DMResolver] */
    // Create and Initialize DMResolver
    //
    self.dmResolver = [DMSManager sharedDMResolver];
    if(self.dmResolver) {
        
        //  Create a helper PayloadToPayoffResolver class to manage network availability
        // and handle async DMResolver queries.
        //
        self.payloadToPayoffResolver = [[PayloadToPayoffResolver alloc] init];
        self.payloadToPayoffResolver.delegate = self;
        self.payloadToPayoffResolver.resolver = self.dmResolver;
        [self.payloadToPayoffResolver updateResolverIsAvailable];
        
        // Supply DMResolver credentials
        //
        // NOTE:
        // The customer programmer must acquire Digimarc Resolver API credentials separately and install them
        // in .../DMS/config/DMResolverSettings.h. Otherwise DMRESOLVER_USERNAME remains undefined and
        // the compiler will issue an error here.
        //
        // If DMRESOLVER_USERNAME is defined and non-empty, compile time validation succeeds.
        // But the credentials themselves are not validated with DMResolver until the first attempt
        // to resolve a payload using the DMResolver resolve:completionBlock: method,
        // or with the DMSDemo helper class PayloadToPayoffResolver.
        //
#ifdef DMRESOLVER_USERNAME
        self.dmResolver.userName = DMRESOLVER_USERNAME;
        self.dmResolver.password = DMRESOLVER_PASSWORD;
#else
#error Digimarc Resolver userName and password must be supplied in ../config/DMResolverSettings.h
#endif
        // Optionally override the default DMResolverProductionService to select labs service,
        // or use one of the HTTPS access methods.   See DMResolver.h.
        //
        self.dmResolver.serviceURL = [NSURL URLWithString:DMResolverProductionService];
    }
    /** - - - - - - - - - - - < < < - - - - - - - - - - - - - [Create and Initialize DMResolver] */
    
    /** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Configure Readers Array and Profiles] */
    // Load DMSDK readers configuration from JSON resource file
    //
    NSString* configFile = @"DMSReadersConfig";
    
    NSBundle* bundle = [NSBundle bundleForClass:[self class]];
    NSString* cfg = [bundle pathForResource:configFile ofType:@"json"];
    [self.dmsManager loadReadersConfigFromJSONFile:cfg];
    
    // Set initial profiles
    //
    self.dmsManager.imageReadingProfile = DMSProfileMedium;
    self.dmsManager.audioReadingProfile = DMSProfileMedium;
    /** - - - - - - - - - - - < < < - - - - - - - - - - - - - [Configure Readers Array and Profiles] */
    
    /** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Create Image Source] */
    // Create image media source
    //
    self.imageCameraSource = [[DMSImageCameraSource alloc] init];
    
    // Optional: setup any image source properties or options here ...
    /** - - - - - - - - - - - < < < - - - - - - - - - - - - - [Create Image Source] */
    
    /** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Create Audio Source] */
    // Create audio media source
    //
//    self.audioMicSource = [[DMSAudioMicAQSource alloc] init];
//    
//    // Optional: let DMSAudioMicAQSource manage the app's AVAudioSession
//    //
//    [self.audioMicSource setupAVAudioSession];
//    self.audioMicSource.enableStallDetection = YES;
//    
//    // Hook up the audio visualizer connection
//    //
//    self.audioMicSource.delegate = self;
//    [self.audioMicSource addObserver:self
//                          forKeyPath:@"volume"
//                             options:0
//                             context:@"volumeDidChange"
//     ];
    /** - - - - - - - - - - - < < < - - - - - - - - - - - - - [Create Audio Source] */
}

#pragma mark - Open/Close DMSDK session


/** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Open DMSManager Session 1] */
// Open and close DMSManager session
//
// The DMSDemo application starts a session immediately after initializing the DMSDK,
// and keeps the same session open for the life of the application.   For other application designs,
// you might tie the duration of the DMS session to the life of a particular view or application
// activity.  In this case you would call openDMSSession when the entering the view or activity
// and closeDMSSession when leaving the view or activity.
//
- (void) openDMSSession
{
    //DLog(@"%s",__PRETTY_FUNCTION__);
    
    [self.dmsManager openSessionWithImageSource:self.imageCameraSource audioSource:nil];
    [self startDMSSources];
    /** - - - - - - - - - - - < < < - - - - - - - - - - - - - [Open DMSManager Session 1] */
#if (TARGET_IPHONE_SIMULATOR)
    // When building for the simulator, there is no actual camera device.
    // Simulate a couple of image watermark read notifications to exercise the rest of the DMSDemo UI.
    //
    DMPayload* payload1 = [[DMPayload alloc] initWithCpmPath:@"GC41.KE.WOM1.v5.00002770"];    // 10096
    DMPayload* payload2 = [[DMPayload alloc] initWithCpmPath:@"GC41.KE.WOM1.v5.0000FEE6"];    // 62524
    [self dms:self.dmsManager detectedImageWatermark:payload1 withReaderInfo:nil isNew:YES];
    [self dms:self.dmsManager detectedImageWatermark:payload2 withReaderInfo:nil isNew:YES];
#endif
    /** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Open DMSManager Session 2] */
    
    
    [self.view addSubview:self.crosshairs];
    [self.view bringSubviewToFront:self.crosshairs];
}

- (void) closeDMSSession
{
    // closeSession will automatically stop both the imageSource and audioSource if they are currently running.
    [self.dmsManager closeSession];
}
/** - - - - - - - - - - - < < < - - - - - - - - - - - - - [Open DMSManager Session 2] */


#pragma mark - Start and Stop DMSDK and image/audio media sources on UI view appear/disappear

/** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Start/Stop DMS and Media Sources] */
// Start and stop media sources
//
// These helper functions start and stop both the image and audio media sources.
// The current DMSManager session remains open. This allows the same start/stop methods
// to be used for both initial startup and temporary pause and resume.
//
// This is the recommended pattern when leaving the main view UI (or diagnostic UI overlay)
// to temporarily look at Help, a WebView, or one of the diagnostic UI detail views. This is
// triggered by the viewWillDisappear: notification on leaving, and the viewWillAppear: notification
// on returning.
//
- (void) startDMSSources
{
    DLog(@"DMSDemo GlobalViewController startDMSSources");
    
    if(self.dmsManager && self.dmsManager.currentStatus == DMSStatusOpen) {
        [self.dmsManager startImageSource];
        
        // As of iOS 6.0, UIWebView no longer sets its own audio session category,
        // which can result in muffled audio on websites with some video players.
        // Fix for this issue is to force audio to playback mode when entering webview,
        // and force it back to play and record when returning to the main UI view
        // so that audio watermark detector can use the microphone.
        //
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionMixWithOthers error:nil];
        [[AVAudioSession sharedInstance] setActive:YES error:nil];
        
        [self.dmsManager startAudioSource];
    }
    
    // The image source will have a new AVCaptureSession, so refresh the link to the preview layer
    [self removeCameraPreview];
    [self addCameraPreview];
}

- (void) stopDMSSources
{
    DLog(@"DMSDemo GlobalViewController stopDMSSources");
    
    [self.dmsManager stopImageSource];
    [self.dmsManager stopAudioSource];
}
/** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Start/Stop DMS and Media Sources] */

/** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Pause/Resume DMS While Media Sources Running] */
// Pause and resume media sources
//
// In some cases, the application may want to temporarily suspend DMSDK watermark and barcode detection
// while keeping the media sources, camera preview, and audio visualizer active. This is done by leaving the
// image and audio media sources running while temporarily selecting the idle reader profiles.
//
// The DMSDemo app presents one use case for this capabilitiy by pausing and resuming
// media sources while showing a popup alert on error notifications.
//
-(void)setReadersToIdle
{
    self.lastImageProfile = self.dmsManager.imageReadingProfile;
    self.lastAudioProfile = self.dmsManager.audioReadingProfile;
    [self.dmsManager setImageReadingProfile:DMSProfileIdle];
    [self.dmsManager setAudioReadingProfile:DMSProfileIdle];
}

-(void)setReadersToOn
{
    if (self.lastImageProfile)
        [self.dmsManager setImageReadingProfile:self.lastImageProfile];
    else
        [self.dmsManager setImageReadingProfile:DMSProfileLow];
    
    if (self.lastAudioProfile)
        [self.dmsManager setAudioReadingProfile:self.lastAudioProfile];
    else
        [self.dmsManager setAudioReadingProfile:DMSProfileLow];
}
/** - - - - - - - - - - - < < < - - - - - - - - - - - - - [Pause/Resume DMS While Media Sources Running] */

/** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Automatically Start/Stop DMS] */
// Handle view and app transition notifications
//
// With iOS7, viewWillAppear: and viewWillDisappear: are no longer called on leaving or returning
// to foreground application status. We now need to specifically hook these notifications.
//
- (void) registerNotificationCenterHooks
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidEnterBackgroundNotificationHandler:)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationWillEnterForegroundNotificationHandler:)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
}

- (void) unRegisterNotificationCenterHooks
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - applicationWillEnterForegroundNotificationHandler
- (void) applicationWillEnterForegroundNotificationHandler:(NSNotification*)notification
{
    if(self.dmsManager.currentStatus == DMSStatusOpen) {
        DLog(@"DMSDemo GlobalViewController applicationWillEnterForegroundNotificationHandler");
        [self startDMSSources];
    }
}


#pragma mark - applicationDidEnterBackgroundNotificationHandler
- (void) applicationDidEnterBackgroundNotificationHandler:(NSNotification*)notification
{
    DLog(@"DMSDemo GlobalViewController applicationDidEnterBackgroundNotificationHandler");
    [self stopDMSSources];
}




#pragma mark - DMSDelegate protocol

- (void) dms:(DMSManager*)dms statusChanged:(DMSStatus)status {
    DLog(@"DMSDK Status Changed: %@", [DMSManager stringFromDMSStatus:status]);
    if(status == DMSStatusOpen) {
        [self.imageCameraSource start];
    }
}

- (void) dms:(DMSManager*)dms reportedError:(NSError*)error {
    DLog(@"DMSDK reported error with domain:%@ code:%ld description:%@ reason:%@",
          error.domain, (long)error.code,
          [error.userInfo objectForKey:NSLocalizedDescriptionKey],
          [error.userInfo objectForKey:NSLocalizedFailureReasonErrorKey] );
    
    [self handleError:error];
}

- (void) dms:(DMSManager*)dms reportedWarningFrom:(NSString*)source message:(NSString*)msg {
    DLog(@"DMSDK reported warning with source:%@ message:%@", source, msg);
    
    [self handleWarningFrom:source message:msg];
}

/** - - - - - - - - - - - > > > - - - - - - - - - - - - - [On Watermark Or Barcode Payload Detection] */
// Handle watermark and barcode detection
//
// The DMSDK reports separate notifications for audio watermarks, image watermarks, barcodes, and QR codes.
// For this DMSDemo app, we just redirect all 4 notifications to the same handler to translate payloads to payoffs.
//
- (void) dms:(DMSManager*)dms detectedImageWatermark:(DMPayload*)payload
withReaderInfo:(NSDictionary *)readerInfo
       isNew:(BOOL)isNew {
    
    [self diagnosticsUISavePayload:payload withReaderInfo:readerInfo isNew:isNew];
    
    // If this is a new payload, then start the process of translating it to a payoff.
    //
    if(isNew) {
        [self resolvePayload:payload withReaderInfo:readerInfo context:@"DMSDemo"];
    }
}

- (void) dms:(DMSManager*)dms detectedAudioWatermark:(DMPayload*)payload
withReaderInfo:(NSDictionary *)readerInfo
       isNew:(BOOL)isNew {
    [self dms:dms detectedImageWatermark:payload withReaderInfo:readerInfo isNew:isNew];
}

- (void) dms:(DMSManager*)dms detectedBarcode:(DMPayload*)payload
withReaderInfo:(NSDictionary *)readerInfo
       isNew:(BOOL)isNew {
    
    [self dms:dms detectedImageWatermark:payload withReaderInfo:readerInfo isNew:isNew];
}

- (void) dms:(DMSManager*)dms detectedQRcode:(DMPayload*)payload
withReaderInfo:(NSDictionary *)readerInfo
       isNew:(BOOL)isNew {
    
    [self dms:dms detectedImageWatermark:payload withReaderInfo:readerInfo isNew:isNew];
}
/** - - - - - - - - - - - < < < - - - - - - - - - - - - - [On Watermark Or Barcode Payload Detection] */



#pragma mark - PayloadToPayoffResolver interface

/** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Resolve Payload Identifier to Payoff Content] */
// Resolve payloads to payoffs
//
- (void) resolvePayload:(DMPayload*)payload withReaderInfo:(NSDictionary*)readerInfo context:(NSString*)context {
    if(payload) {
        
        // Verify the user has provided us with DMResolver credentials (see notes in [self initializeDms]).
        //
        DMResolver* resolver = self.dmResolver;
        if((!resolver.userName) || (0 >= resolver.userName.length)
           || (!resolver.password) || (0 >= resolver.password.length)
           || (!resolver.serviceURL)) {
            
            NSString* source = @"Sample Application";
            NSString* msg = @"Bad or missing DMResolver username/password credentials in DMResolverSettings.plist.  "
            "Contact your Digimarc representative to assign unique credentials for your application.  "
            "These are required for all applications using the Digimarc Discover ID Manager Resolver service.";
            
            [self.dmsManager postWarningFrom:source message:msg];
            DLog(@"%@: %@", source, msg);
        } else {
            
            [self method_apertureSound];
            
            // Show the Circle-D spinner branding whenever resolving a payload to payoff,
            // as required by Digimarc Mobile Application Verification Checklist.
            //
//            if (!self.progressHUD && !self.customSpinner && self.tableView.isHidden) {
//                [self showSpinner];
//            }
            
            // If the user has provided invalid DMResolver credentials, the resolver reports an error via either
            // [self payloadResolver:reportedError:] or [self payloadResolver:reportedWarningFrom:message:].
            //
            // Otherwise, the resolver resolves the payload to its associated payoff
            // and returns a resolve result containing both payload and payoff via
            // [self payloadResolver:resolvedNewPayoffWithResult:].
            //
            [self.payloadToPayoffResolver resolvePayload:payload];
        }
    }
}
/** - - - - - - - - - - - < < < - - - - - - - - - - - - - [Resolve Payload Identifier to Payoff Content] */

/** - - - - - - - - - - - > > > - - - - - - - - - - - - - [Handling QR Code Resolver Errors] */
// Handling QR Code Resolver Errors
//

- (void) payloadResolver:(PayloadToPayoffResolver*)p2pr reportedError:(NSError*)error {
    if(error.code == kDMResolverErrorQRCodeDoesNotContainURL && [error.domain isEqualToString:DigimarcResolverErrorDomain]) {
        // Downgrade QRCodes that do not contain a URL errors to warnings
        //
        [self handleWarningFrom:@"DMResolver" message:error.localizedDescription];
    } else {
        [self handleError:error];
    }
}
/** - - - - - - - - - - - < < < - - - - - - - - - - - - - [Handling QR Code Resolver Errors] */

- (void) payloadResolver:(PayloadToPayoffResolver*)p2pr reportedWarningFrom:(NSString*)source message:(NSString*)msg {
    [self handleWarningFrom:source message:msg];
}

- (void) payloadResolver:(PayloadToPayoffResolver*)p2pr resolvedNewPayoffWithResult:(DMResolveResult*)result
{
    self.dmsManager.delegate = nil;
    
    if (bool_viewDisappeared == YES)
    {
        [self method_removeOldDataFromCache];
        return;
    }
    
    
    
    DLog(@"PayloadToPayoffResolver resolvePayload:%@ identified NEW PAYOFF:%@", result.payload, result.payoff.title);
    
    DLog(@"description: %@", result.payoff.description);
    
    // save the payoff info of interest to us in the main panel UI payoff items database
    //
    DMPayload* payload = result.payload;
    DMPayoff* payoff = result.payoff;
    
    __block PayoffDoc *newDoc = [[PayoffDoc alloc] init];
    newDoc.payoffItem.payload = payload;
    newDoc.payoffItem.title = payoff.title;
    newDoc.payoffItem.subtitle = payoff.subtitle;
    
    // convert payoff thumbnailImageURL (if present) back to simple NSString form for UI
    if(payoff.thumbnailImageURL) {
        newDoc.payoffItem.thumbnailImage = [payoff.thumbnailImageURL absoluteString];
    }
    newDoc.payoffItem.content = payoff.content;
    newDoc.payoffItem.actionType = payoff.actionType;
    newDoc.payoffItem.actionToken = payoff.actionToken;
    newDoc.payoffItem.correlationKey = payoff.correlationKey;
    newDoc.payoffItem.synthesizedContent = payoff.synthesizedContent;
    
//    [payoffsTableViewController newItemAvailable:newDoc];
    
    // Do the heavy lifting thumbnail image graphics processing on a background thread
    //
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSData *imageData = NULL;
        if(result.payoff.thumbnailImageURL) {
            imageData = [NSData dataWithContentsOfURL:result.payoff.thumbnailImageURL];
        }
        
        if(imageData) {
            
            // If the payoff specified a thumbnail image URL
            // And if the raw thumbnail image data could be found and downloaded from the URL
            // Then use it as our icon
            //
            UIImage *image = [UIImage imageWithData:imageData];
            CGFloat newWidth = image.size.height < image.size.width ? image.size.height : image.size.width;
            CGImageRef imageRef = CGImageCreateWithImageInRect(image.CGImage, CGRectMake(0, 0, newWidth, newWidth));
            
            CGSize s = CGSizeMake(100, 100);
            UIGraphicsBeginImageContext(s);
            [[UIImage imageWithCGImage:imageRef] drawInRect:CGRectMake(0,0,s.width,s.height)];
            UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            imageData = UIImageJPEGRepresentation(newImage, 1.0);
            
        } else {
            
            // Else use a default icon that varies by payload type
            //
            switch(result.payload.payloadCategory) {
                default:
                case DMPayloadCategoryImage:
                    imageData = UIImageJPEGRepresentation([UIImage imageNamed:@"DefaultImage.png"], 1.0);
                    break;
                    
                case DMPayloadCategoryAudio:
                    imageData = UIImageJPEGRepresentation([UIImage imageNamed:@"AudioImage.png"], 1.0);
                    break;
                    
                case DMPayloadCategoryBarcode:
                    if([result.payload isQRCode]) {
                        imageData = UIImageJPEGRepresentation([UIImage imageNamed:@"QRImage.png"], 1.0);
                    } else {
                        imageData = UIImageJPEGRepresentation([UIImage imageNamed:@"BarImage.png"], 1.0);
                    }
                    break;
            }
        }
        
        // return to the main thread to update the main panel UI
        //
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            [newDoc.payoffItem setImageData:imageData];
            [newDoc saveData];
//            [payoffsTableViewController.tableView reloadData];
            
//            if(self.viewMode == kMyViewMode_Payoffs) {
//                [self.tableView reloadData];
//            }
        });
    });
    
    
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////         Our Work
    
    DLog(@"result.payoff.title: %@", result.payoff.title);
    [self performSegueWithIdentifier:@"segue_productDetail" sender:result.payoff.title];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_productDetail"])
    {
        VC_ProductDetail *obj = segue.destinationViewController;
        obj.uniqueID = sender;
    }
}




#pragma mark - Diagnostics UI

- (void) diagnosticsUISavePayload:(DMPayload*)payload withReaderInfo:(NSDictionary*)readerInfo isNew:(BOOL)isNew
{
    // Save the incoming DMPayload info of interest to us for the diagnostics UI
    //
    PayloadItem* item = [PayloadItem new];
    item.itemNumber = payloadItemNumber++;
    item.reportedDate = [NSDate date];
    item.payload = payload;
    item.readerInfo = readerInfo;
}

- (void) diagnosticsUISimulatePayloadsFromPayoffsHistory
{
    
    // walk payoffs history from back to front
    // inserting each item into front of diagnostics UI tables
    // result should be same sequence order in both main and diagnostic UI views
    //
//    for(NSInteger i = payoffsTableViewController.payoffsHistory.count; i > 0; i--) {
//        PayoffDoc* doc = payoffsTableViewController.payoffsHistory[i - 1];
//        PayoffItem* payoffItem = doc.payoffItem;
//        
//        // Inject a matching payload item into diagnostics UI payloads list
//        //
//        PayloadItem* payloadItem = [PayloadItem new];
//        payloadItem.itemNumber = payloadItemNumber++;
//        payloadItem.reportedDate = payoffItem.date;
//        payloadItem.payload = payoffItem.payload;
//        payloadItem.readerInfo = nil;
//        
//        [payloadItems insertObject:payloadItem atIndex:0];
    
        // The diagnostics UI payoffs list works directly from the main panel UI payoffsTableViewController.payoffsHistory data
        // so no need to inject anything for payoffs.
//    }
}


//#pragma mark - get product detail
//-(void)method_getProductDetail
//{
//    [ApplicationDelegate.webServiceEngine method_getProduct:nil completionHandler:^(id object) {
//        
//        DLog(@"%@", object);
//    } errorHandler:^(NSError *error) {
//        
//        [UIAlertView showWithError:error];
//        
//    }];
//    
//    
//    
////    [ApplicationDelegate.webServiceEngine uploadImageFromFile:nil
////                                       completionHandler:^(id twitPicURL){
////                                           
////                                           [[[UIAlertView alloc] initWithTitle:@"Uploaded to"
////                                                                      message:(NSString*) twitPicURL
////                                                                     delegate:nil
////                                                            cancelButtonTitle:NSLocalizedString(@"Dismiss", @"")
////                                                            otherButtonTitles:nil] show];
////                                       }
////     
////     
////                                                 errorHandler:^(NSError* error){
////                                                [UIAlertView showWithError:error];
////                                            }];
//    
//    
////    [self.uploadOperation onUploadProgressChanged:^(double progress) {
////        
////        DLog(@"%.2f", progress*100.0);
////        self.uploadProgessBar.progress = progress;
////    }];
//}

#pragma mark - set heading
-(void)method_setHeading
{
    UILabel *label_heading = (UILabel *)[self.navigationController.view viewWithTag:999];
    
    [label_heading setText:@"Scan"];
}




#pragma mark - cycle
- (void)viewWillAppear:(BOOL)animated
{
    DLog(@"DMSDemo GlobalViewController viewWillAppear");
    [super viewWillAppear:animated];
    
    [self method_setHeading];
    
    bool_viewDisappeared = NO;
    
    if (self.dmsManager)
    {
        self.dmsManager.delegate = self;
    }
    
    
    
    
    // clear any selections or stale state in dignostics UI on return from a detail view
    
    // start or resume the image and audio sources and resume processing incoming media data
    //
    if(self.dmsManager.currentStatus == DMSStatusOpen) {
        [self startDMSSources];
    }
    
    [self method_setAddToCartCounterLabel];
}
#pragma mark - method_setAddToCartCounterLabel
-(void)method_setAddToCartCounterLabel
{
    UIView *view_counter;
    if (ApplicationDelegate.single.array_cartObjects)
    {
        if ([ApplicationDelegate.single.array_cartObjects count])
        {
            view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
            UILabel *label_counter = (UILabel *)[self.navigationController.view viewWithTag:888];
            [view_counter setHidden:NO];
            label_counter.text = [NSString stringWithFormat:@"%lu",(unsigned long)[ApplicationDelegate.single.array_cartObjects count]];
        }
        else
        {
            view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
            [view_counter setHidden:YES];
        }
        
    }
    else
    {
        view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
        [view_counter setHidden:YES];
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    [self method_getProductDetail];
    
    self.cameraViewActive = YES;
    self.cameraPreviewContainer.hidden = NO;
    
    
    self.crosshairs = [[CrosshairsView alloc] init];
//    self.crosshairs.translatesAutoresizingMaskIntoConstraints = NO;
    self.crosshairs.center = self.view.center;

    
    
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
    [self.dateFormatter setDateStyle:NSDateFormatterNoStyle];
    
#if 0
    // [TK-2035] Remove the tap-tap gesture recognizer from DMSDemo
    // This has been replaced by the (+) button in lower left corner of the camera preview window.
    
    // Install the knock knock gesture recognizer to reveal the list of lists diagnostics view (overlay)
    //
    UITapGestureRecognizer *secretKnock = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleShowDiagnosticsOverlay)];
    secretKnock.numberOfTapsRequired = 5;
#if (TARGET_IPHONE_SIMULATOR)
    secretKnock.numberOfTouchesRequired = 1;
#else
    secretKnock.numberOfTouchesRequired = 3;
#endif
    [self.view addGestureRecognizer:secretKnock];
#endif
    
    // At startup, the main panel UI will have archived payoffs data to show
    // but the diagnostics UI has not yet collected anything for the current session.
    //
    // Reduce user confusion over this issue by simulating one payload and one payoff
    // into diagnostics UI from the main panel UI data
    //
    [self performSelector:@selector(diagnosticsUISimulatePayloadsFromPayoffsHistory) withObject:nil afterDelay:0.1];
    
    // **************************************************************************************************
    //
    // Setup the DMSManager and app-defined image and audio sources...
    //
    [self initializeDms];
    [self registerNotificationCenterHooks];
    
    // This version of DMSDemo application automatically opens a new DMSDK session on startup and keeps it open for the life of the application.
    // Then as the view comes and goes, the session remains open, but the image/audio sources might be stopped and restarted, or the
    // image/audio profiles might be temporarily set to idle to suspend detection.
    //
    // Other applications might choose to open a DMS session modally during some activities, then close DMS session and release nearly all
    // DMSDK resources while the application moves on to other activities.
    //
    [self performSelector:@selector(openDMSSession) withObject:nil afterDelay:0.25];
    
//    [self performSelector:@selector(method_performActionsAfterDelay) withObject:nil afterDelay:0.5];
    [self method_addViewsToNavController];
}

-(void)viewWillDisappear:(BOOL)animated
{
    bool_viewDisappeared = YES;
    
    AVCaptureDevice *flashLight = [self flashLight];
    if ([flashLight lockForConfiguration:nil])
    {
        [flashLight setTorchMode:AVCaptureTorchModeOff];
        [flashLight unlockForConfiguration];
    }
    
    [self method_removeOldDataFromCache];
}



- (void) viewDidDisappear:(BOOL)animated
{
    DLog(@"DMSDemo GlobalViewController viewDidDisappear");
    
    // stop or pause the image and audio sources and suspend processing
    // incoming media data until we return to the capture activity.
    //
    
    
    
    [self stopDMSSources];
    [super viewDidDisappear:animated];
}


-(void)method_removeOldDataFromCache
{
    payloadItemNumber = 0;
    
    [self.dmsManager clearAudioPayloadCache];
    [self.dmsManager clearImagePayloadCache];
    [self.dmsManager clearAudioBuffers];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - action_cancel
- (IBAction)action_cancel:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - add Views To Nav Controller
-(void)method_addViewsToNavController
{
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"View_topMenuScanner"
                                                         owner:self
                                                       options:nil];
    UIView *view_obj = nibContents[0];
    
    CGRect rect_new = CGRectMake(8,
                                 k_topBar_y,
                                 self.view.frame.size.width-16,
                                 view_obj.frame.size.height);
    
    //add torch button in header
    UIButton *button_torch = [[UIButton alloc] initWithFrame:CGRectMake(rect_new.size.width - 50, 75, 20, 28)];
    [button_torch setBackgroundImage:[UIImage imageNamed:@"torch-icon"] forState:UIControlStateNormal];
    [button_torch setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button_torch.titleLabel setFont:[UIFont systemFontOfSize:17]];
    [button_torch addTarget:self action:@selector(action_torch:) forControlEvents:UIControlEventTouchUpInside];
    [view_obj addSubview:button_torch];
    
//    CGRect rect_new = CGRectMake(self.view.frame.size.width/2 - view_obj.frame.size.width/2,
//                                 k_topBar_y,
//                                 view_obj.frame.size.width,
//                                 view_obj.frame.size.height);
    
    
    
    view_obj.frame = rect_new;
    
    [self.navigationController.view addSubview:view_obj];
    
    
    
    nibContents = nil;
    
    
    
    nibContents = [[NSBundle mainBundle] loadNibNamed:@"View_menuScanner"
                                                owner:self
                                              options:nil];
    view_obj = nibContents[0];
    
    
    [view_obj setFrame:CGRectMake(self.view.frame.size.width,              //notice this is OFF screen!
                                  0,
                                  self.view.frame.size.width,
                                  self.view.frame.size.height)];
    
//    [view_obj setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
//    [view_obj setBackgroundColor:[UIColor clearColor]];
    
    
    [self.navigationController.view addSubview:view_obj];
    
    
    
// Blur effect BG
//    UIView *backgroundView = [[UIView alloc] initWithFrame:view_obj.bounds];
//    UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleLight]];
//    [visualEffectView setFrame:backgroundView.bounds];
//    [backgroundView addSubview:visualEffectView];
//    [view_obj insertSubview:backgroundView atIndex:0];
    
//    UIView *view_subHeader = (UIView *)[self.navigationController.view viewWithTag:420];
//    view_subHeader.layer.cornerRadius = 10;
//    view_subHeader.layer.masksToBounds = YES;
}






#pragma mark - top menu Actions
- (IBAction)action_ShoppingCart:(id)sender
{
    if (ApplicationDelegate.single.array_cartObjects)
    {        
        ApplicationDelegate.single.string_controllerName = k_shoppingCart;
    
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:nil message:@"Cart is empty" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil] show];
    }
}

- (IBAction)action_back:(id)sender
{
    DLog(@"%@", [self.navigationController.visibleViewController class]);
    
    id viewController_ = self.navigationController.visibleViewController;
    if (![viewController_ isKindOfClass:[ViewController class]])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction)action_home:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)action_menu:(id)sender
{
    UIView *view_temp = [self.navigationController.view viewWithTag:4];
    
    [UIView animateWithDuration:k_menuAnimationTime
                     animations:^{
                         
                         [view_temp setFrame:CGRectMake(0,
                                                        0.0f,
                                                        self.view.frame.size.width,
                                                        self.view.frame.size.height)];
                         
                     }
                     completion:^(BOOL finished){
                     }
     ];
}

#pragma mark - full screen menu
- (IBAction)action_hideMenu:(UIButton *)sender
{
    
    [UIView animateWithDuration:k_menuAnimationTime
                     animations:^{
                         
                         [sender.superview setFrame:CGRectMake(self.view.frame.size.width,                               //notice this is ON screen!
                                                            0.0f,
                                                            self.view.frame.size.width,
                                                            self.view.frame.size.height)];
                         
                     }
                     completion:^(BOOL finished){
                     }
     ];
}

- (IBAction)action_logOut:(id)sender
{
    ApplicationDelegate.single.array_cartObjects = nil;
    
    ApplicationDelegate.single.string_controllerName = k_logOut;
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)action_storeLocator:(id)sender
{
    ApplicationDelegate.single.string_controllerName = k_storeLocator;
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)action_scan:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    [self action_hideMenu:btn.superview];
}

- (IBAction)action_barCode:(id)sender
{
    ApplicationDelegate.single.string_controllerName = k_barCodeScan;
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)action_viewLoyalPoints:(id)sender
{
    ApplicationDelegate.single.string_controllerName = k_loyalityPoint;

    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)action_signIn:(id)sender
{
    ApplicationDelegate.single.string_controllerName = k_Login;
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];

}




#pragma mark - aperture sound
- (void) configureAVAudioSession
{
    //get your app's audioSession singleton object
    AVAudioSession* session = [AVAudioSession sharedInstance];

    //error handling
    BOOL success;
    NSError* error;
    
    //set the audioSession category.
    //Needs to be Record or PlayAndRecord to use audioRouteOverride:
    
    success = [session setCategory:AVAudioSessionCategoryPlayAndRecord
                             error:&error];
    
    if (!success)  NSLog(@"AVAudioSession error setting category:%@",error);
    
    //set the audioSession override
    success = [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker
                                         error:&error];
    if (!success)  NSLog(@"AVAudioSession error overrideOutputAudioPort:%@",error);
    
    //activate the audio session
    success = [session setActive:YES error:&error];
    if (!success) NSLog(@"AVAudioSession error activating: %@",error);
    else NSLog(@"audioSession active");
}


-(void)method_apertureSound
{
    [self configureAVAudioSession];
    
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"camera-shutter-click-01" ofType:@"mp3"]];
    NSError *error;
    
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    //    self.audioPlayer.currentTime = 0.0;
    self.audioPlayer.volume = 1.0;
    url = nil;
    
    if (error)
    {
        NSLog(@"Error in audioPlayer: %@", [error localizedDescription]);
    }
    else
    {
        [self.audioPlayer  play];
    }
    
    
    //    obj = nil;
}
    
@end