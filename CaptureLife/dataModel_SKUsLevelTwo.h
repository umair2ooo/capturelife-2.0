@interface dataModel_SKUsLevelTwo : NSObject

@property (nonatomic, strong) NSString *SKUPriceValue;
@property (nonatomic, strong) NSString *SKUUniqueID;
@property (nonatomic, strong) NSArray *Values;


@end