#import "Cell_directionOnMap.h"

@implementation Cell_directionOnMap

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)method_setValues:(NSDictionary *)dic
{
    DLog(@"dic: %@", dic);
}


- (IBAction)action_DirectionToHere:(id)sender
{
    DLog(@"%ld",[sender tag]);
    [self.delegate method_direction:(int)[sender tag]];
}


@end