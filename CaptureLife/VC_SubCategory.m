#import "VC_SubCategory.h"

#import "cell_Category.h"
#import "VC_ProductList.h"
#import "VC_SubofSubCategory.h"



@interface VC_SubCategory ()
{
    NSString *catID;
    NSString *productName;
}

@property (weak, nonatomic) IBOutlet UITableView *tableview_;

@property (strong, nonatomic) MKNetworkOperation *operation;



#pragma mark - top menu actions
- (IBAction)action_ShoppingCart:(id)sender;
- (IBAction)action_menu:(id)sender;
- (IBAction)action_hideMenu:(id)sender;
- (IBAction)action_back:(id)sender;
- (IBAction)action_home:(id)sender;


#pragma mark - full menu actions
- (IBAction)action_logOut:(id)sender;
- (IBAction)action_storeLocator:(id)sender;
- (IBAction)action_scan:(id)sender;
- (IBAction)action_viewLoyalPoints:(id)sender;
- (IBAction)action_barCode:(id)sender;
- (IBAction)action_signInRegister:(id)sender;



@end

@implementation VC_SubCategory

#pragma mark - set heading
-(void)method_setHeading
{
    UILabel *label_heading = (UILabel *)[self.navigationController.view viewWithTag:999];
    
    [label_heading setText:@"Browse by Department"];

}



#pragma mark - cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self method_addViewsToNavController];

    DLog(@"%@",self.array_categories);
    
    [self.tableview_ reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self method_setHeading];
    [self method_setAddToCartCounterLabel];
}

#pragma mark - method_setAddToCartCounterLabel
-(void)method_setAddToCartCounterLabel
{
    UIView *view_counter;
    if (ApplicationDelegate.single.array_cartObjects)
    {
        if ([ApplicationDelegate.single.array_cartObjects count])
        {
            view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
            UILabel *label_counter = (UILabel *)[self.navigationController.view viewWithTag:888];
            [view_counter setHidden:NO];
            label_counter.text = [NSString stringWithFormat:@"%lu",(unsigned long)[ApplicationDelegate.single.array_cartObjects count]];
        }
        else
        {
            view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
            [view_counter setHidden:YES];
        }
        
    }
    else
    {
        view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
        [view_counter setHidden:YES];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    if(self.operation)
    {
        [self.operation cancel];
        self.operation = nil;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableview dataSource and delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.array_categories count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell_Category *cell = (cell_Category *)[tableView dequeueReusableCellWithIdentifier:@"cell_Category"];
    
    if (cell == nil)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"cell_Category" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    [cell method_setCategoryName:[[self.array_categories objectAtIndex:indexPath.row] valueForKey:@"name"]];
//    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    catID = [[self.array_categories objectAtIndex:indexPath.row] valueForKey:@"uniqueID"];
    productName = [[self.array_categories objectAtIndex:indexPath.row] valueForKey:@"name"];
    [self method_getSubCategories:[[self.array_categories objectAtIndex:indexPath.row] valueForKey:@"uniqueID"]];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_SubCatProductList"])
    {
        VC_ProductList *obj = segue.destinationViewController;
        obj.productName = productName;
        obj.array_products = [NSMutableArray arrayWithArray:(NSMutableArray *)sender];
    }
    else if ([segue.identifier isEqualToString:@"segue_SubofSubCategory"])
    {
        //UINavigationController *nav = segue.destinationViewController;
        VC_SubCategory *obj = segue.destinationViewController;
        obj.array_categories = [NSMutableArray arrayWithArray:(NSMutableArray *)sender];
    }
}


#pragma mark - method_getSubCategories
-(void)method_getSubCategories:(NSString *)categoryID
{
    if (![self.operation isExecuting])
    {
        self.operation = [ApplicationDelegate.webServiceEngine method_getSubCategories:categoryID
                                                                     completionHandler:^(id response)
                          {
                              NSDictionary *result = response;
                              NSLog(@"result:%@",result);
                              if (response != nil)
                              {
                                  if ([result valueForKey:@"CatalogGroupView"])
                                  {
                                      VC_SubofSubCategory *objSubCategory = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_SubofSubCategory"];
                                      objSubCategory.array_categories = [[NSMutableArray alloc] init];
                                      [(NSArray *)[result valueForKey:@"CatalogGroupView"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                          //name
                                          [objSubCategory.array_categories addObject:[NSDictionary dictionaryWithObjectsAndKeys:[obj valueForKey:@"name"],@"name",[obj valueForKey:@"uniqueID"],@"uniqueID", nil]];
                                      }];
                                      NSLog(@"Array: %@", objSubCategory.array_categories);
                                      [self performSegueWithIdentifier:@"segue_SubofSubCategory" sender:objSubCategory.array_categories];
                                  }
                                  else
                                  {
                                      [self method_getproductListByCategory:catID];
                                      //[self performSegueWithIdentifier:@"segue_ProductList" sender:nil];
                                  }
                                  
                              }
                              else
                              {
                                  [[[UIAlertView alloc] initWithTitle:nil message:@"There is something wrong, from the server side,please try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                              }
                          }
                                                                          errorHandler:^(NSError *error)
                          {
                              [self method_getproductListByCategory:catID];
                              DLog(@"there is something is wrong, please try with valid id and pass");
                              //[self performSegueWithIdentifier:@"segue_ProductList" sender:nil];
                              DLog(@"1:%@\n2:%@\n3:%@\n4:%@",
                                   [error localizedDescription],
                                   [error localizedFailureReason],
                                   [error localizedRecoveryOptions],
                                   [error localizedRecoverySuggestion]);
                          }
                          ];
    }
    
    [self.operation onDownloadProgressChanged:^(double progress)
     {
         DLog(@"download progress = %.2f", progress*100.0);
     }];
}


#pragma mark - method_getSubCategories
-(void)method_getproductListByCategory:(NSString *)categoryID
{
    if (![self.operation isExecuting])
    {
        self.operation = [ApplicationDelegate.webServiceEngine method_getProductsByCatgeory:categoryID
                                                                          completionHandler:^(id response)
                          {
                              NSDictionary *result = response;
                              NSLog(@"result:%@",result);
                              if (response != nil)
                              {
                                  if ([result valueForKey:@"CatalogEntryView"])
                                  {
                                      VC_ProductList *objProductList = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_ProductList"];
                                      objProductList.array_products = [[NSMutableArray alloc] init];
                                      [(NSArray *)[result valueForKey:@"CatalogEntryView"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                          [objProductList.array_products addObject:[NSDictionary dictionaryWithObjectsAndKeys:[obj valueForKey:@"name"],@"name",[obj valueForKey:@"uniqueID"],@"uniqueID",[obj valueForKey:@"thumbnail"],@"thumbnail",[obj valueForKey:@"fullImage"],@"fullImage",[[[obj valueForKey:@"Price"] objectAtIndex:0] valueForKey:@"priceValue"],@"Price", nil]];
                                      }];
                                      NSLog(@"Array: %@", objProductList.array_products);
                                      [self performSegueWithIdentifier:@"segue_SubCatProductList" sender:objProductList.array_products];
                                  }
                                  else
                                  {
                                      [[[UIAlertView alloc] initWithTitle:nil message:@"Products not found" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                                  }
                                  
                              }
                              else
                              {
                                  [[[UIAlertView alloc] initWithTitle:nil message:@"There is something wrong, from the server side,please try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                              }
                          }
                                                                               errorHandler:^(NSError *error)
                          {
                              DLog(@"there is something is wrong, please try with valid id and pass");
                              DLog(@"1:%@\n2:%@\n3:%@\n4:%@",
                                   [error localizedDescription],
                                   [error localizedFailureReason],
                                   [error localizedRecoveryOptions],
                                   [error localizedRecoverySuggestion]);
                          }
                          ];
    }
    
    [self.operation onDownloadProgressChanged:^(double progress)
     {
         DLog(@"download progress = %.2f", progress*100.0);
     }];
}



#pragma mark - method_addViewsToNavController
-(void)method_addViewsToNavController
{
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"View_topMenuSubCategory"
                                                         owner:self
                                                       options:nil];
    UIView *view_obj = nibContents[0];
    
    CGRect rect_new = CGRectMake(8,
                                 k_topBar_y,
                                 self.view.frame.size.width-16,
                                 view_obj.frame.size.height);
    
    
//    CGRect rect_new = CGRectMake(self.view.frame.size.width/2 - view_obj.frame.size.width/2,
//                                 k_topBar_y,
//                                 view_obj.frame.size.width,
//                                 view_obj.frame.size.height);
    
    
    
    view_obj.frame = rect_new;
    
    [self.navigationController.view addSubview:view_obj];
    
    
    
    nibContents = nil;
    
    
    
    nibContents = [[NSBundle mainBundle] loadNibNamed:@"View_menuSubCategory"
                                                owner:self
                                              options:nil];
    view_obj = nibContents[0];
    
    [view_obj setFrame:CGRectMake( self.view.frame.size.width,              //notice this is OFF screen!
                                  0,
                                  self.view.frame.size.width,
                                  self.view.frame.size.height)];
    
//    [view_obj setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
//    [view_obj setBackgroundColor:[UIColor clearColor]];
    
    [self.navigationController.view addSubview:view_obj];
    
    
    
// Blur effect BG
//    UIView *backgroundView = [[UIView alloc] initWithFrame:view_obj.bounds];
//    
//    UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleLight]];
//    
//    [visualEffectView setFrame:backgroundView.bounds];
//    [backgroundView addSubview:visualEffectView];
//    
//    [view_obj insertSubview:backgroundView atIndex:0];
    
    
//    UIView *view_subHeader = (UIView *)[self.navigationController.view viewWithTag:420];
//    view_subHeader.layer.cornerRadius = 10;
//    view_subHeader.layer.masksToBounds = YES;
}



#pragma mark - top menu Actions

- (IBAction)action_ShoppingCart:(id)sender
{
    if (ApplicationDelegate.single.array_cartObjects)
    {
        
        ApplicationDelegate.single.string_controllerName = k_shoppingCart;
        
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        
//        [self.navigationController presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"Nav_Cart"]
//                                            animated:YES
//                                          completion:nil];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:nil message:@"Cart is empty" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil] show];
    }
}

- (IBAction)action_menu:(id)sender
{
    UIView *view_temp = [self.navigationController.view viewWithTag:6];
    
    [UIView animateWithDuration:k_menuAnimationTime
                     animations:^{
                         
                         [view_temp setFrame:CGRectMake( 0,
                                                        0.0f,
                                                        self.view.frame.size.width,
                                                        self.view.frame.size.height)];
                         
                     }
                     completion:^(BOOL finished){
                     }
     ];
}

- (IBAction)action_back:(id)sender
{
    DLog(@"%@", [self.navigationController.visibleViewController class]);
    
    id viewController = self.navigationController.visibleViewController;
    if (![viewController isKindOfClass:[VC_SubCategory class]])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction)action_home:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}


#pragma mark - full screen menu
- (IBAction)action_hideMenu:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    
    [UIView animateWithDuration:k_menuAnimationTime
                     animations:^{
                         
                         [btn.superview setFrame:CGRectMake( self.view.frame.size.width,                               //notice this is ON screen!
                                                            0.0f,
                                                            self.view.frame.size.width,
                                                            self.view.frame.size.height)];
                         
                     }
                     completion:^(BOOL finished){
                     }
     ];
}



- (IBAction)action_logOut:(id)sender
{
    ApplicationDelegate.single.array_cartObjects = nil;
    
    ApplicationDelegate.single.string_controllerName = k_logOut;
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)action_storeLocator:(id)sender
{
    ApplicationDelegate.single.string_controllerName = k_storeLocator;
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)action_scan:(id)sender
{
    ApplicationDelegate.single.string_controllerName = k_scan;
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)action_barCode:(id)sender
{
    ApplicationDelegate.single.string_controllerName = k_barCodeScan;
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)action_signInRegister:(id)sender
{
    ApplicationDelegate.single.string_controllerName = k_Login;
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)action_viewLoyalPoints:(id)sender
{
    ApplicationDelegate.single.string_controllerName = k_loyalityPoint;
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}




@end