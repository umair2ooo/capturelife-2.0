#import "VC_ProductList.h"
#import "VC_ProductDetail.h"
#import "cell_ProductList.h"

@interface VC_ProductList ()
{
    NSString *uniqueID;
}

@property (weak, nonatomic) IBOutlet UILabel *label_ProductName;
@property (weak, nonatomic) IBOutlet UILabel *label_ProductCount;
@property (weak, nonatomic) IBOutlet UITableView *tableview_;



#pragma mark - top menu actions
- (IBAction)action_ShoppingCart:(id)sender;
- (IBAction)action_menu:(id)sender;
- (IBAction)action_hideMenu:(id)sender;
- (IBAction)action_back:(id)sender;

#pragma mark - full menu actions
- (IBAction)action_logOut:(id)sender;
- (IBAction)action_storeLocator:(id)sender;
- (IBAction)action_scan:(id)sender;
- (IBAction)action_viewLoyalPoints:(id)sender;



- (IBAction)action_Back:(id)sender;


@end

@implementation VC_ProductList


#pragma mark - set heading
-(void)method_setHeading
{
    UILabel *label_heading = (UILabel *)[self.navigationController.view viewWithTag:999];
    
    [label_heading setText:@"Products list"];
}



#pragma mark - cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self.navigationController.viewControllers count]<=1)
    {
        [self method_addViewsToNavController];
    }
    
    
    self.label_ProductName.text = [NSString stringWithFormat:@"Searched: %@", self.productName];
    self.label_ProductCount.text = [NSString stringWithFormat:@"Count: %lu",(unsigned long)[self.array_products count]];
}


-(void)viewWillAppear:(BOOL)animated
{
//    UIView *view_temp = [self.navigationController.view viewWithTag:1];
//    CGRect rectNew = CGRectMake(0, 0, self.view.frame.size.width, 114);
//    [view_temp setFrame:rectNew];

    [self method_setHeading];
    [self method_setAddToCartCounterLabel];
}

#pragma mark - method_setAddToCartCounterLabel
-(void)method_setAddToCartCounterLabel
{
    UIView *view_counter;
    if (ApplicationDelegate.single.array_cartObjects)
    {
        if ([ApplicationDelegate.single.array_cartObjects count])
        {
            view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
            UILabel *label_counter = (UILabel *)[self.navigationController.view viewWithTag:888];
            [view_counter setHidden:NO];
            label_counter.text = [NSString stringWithFormat:@"%lu",(unsigned long)[ApplicationDelegate.single.array_cartObjects count]];
        }
        else
        {
            view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
            [view_counter setHidden:YES];
        }
        
    }
    else
    {
        view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
        [view_counter setHidden:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"segue_productDetaiviaList"])
    {
        VC_ProductDetail *obj = segue.destinationViewController;
        
        obj.dic_productsList = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                self.array_products,k_productList,
                                [[self.array_products objectAtIndex:[sender intValue]] valueForKey:k_uniqueID],k_uniqueID, nil];
    
        
        obj.uniqueID = uniqueID;
    }
}

#pragma mark - tableview delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.array_products count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell_ProductList *cell = (cell_ProductList *)[tableView dequeueReusableCellWithIdentifier:@"cell_ProductList"];
    
    if (cell == nil)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"cell_ProductList" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    [cell method_setValues:[NSDictionary dictionaryWithObjectsAndKeys:[[self.array_products objectAtIndex:indexPath.row] valueForKey:@"name"],@"name",[[self.array_products objectAtIndex:indexPath.row] valueForKey:@"thumbnail"],@"image",[[self.array_products objectAtIndex:indexPath.row] valueForKey:@"Price"],@"price",nil]];
//    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    uniqueID = [[self.array_products objectAtIndex:indexPath.row] valueForKey:@"uniqueID"];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self performSegueWithIdentifier:@"segue_productDetaiviaList" sender:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
}

#pragma mark - action_Back
- (IBAction)action_Back:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}




#pragma mark - method_addViewsToNavController
-(void)method_addViewsToNavController
{
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"View_topMenuSubCategory"
                                                         owner:self
                                                       options:nil];
    UIView *view_obj = nibContents[0];
    
    CGRect rect_new = CGRectMake(8,
                                 k_topBar_y,
                                 self.view.frame.size.width-16,
                                 view_obj.frame.size.height);
    
    
//    CGRect rect_new = CGRectMake(self.view.frame.size.width/2 - view_obj.frame.size.width/2,
//                                 k_topBar_y,
//                                 view_obj.frame.size.width,
//                                 view_obj.frame.size.height);
    
    
    view_obj.frame = rect_new;
    
    [self.navigationController.view addSubview:view_obj];
    
    
    
    nibContents = nil;
    //view_obj = nil;
    
    
    
    nibContents = [[NSBundle mainBundle] loadNibNamed:@"View_menuSubCategory"
                                                owner:self
                                              options:nil];
    view_obj = nibContents[0];

    [view_obj setFrame:CGRectMake( self.view.frame.size.width,              //notice this is OFF screen!
                                  0,
                                  self.view.frame.size.width,
                                  self.view.frame.size.height)];
    
//    [view_obj setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
//    [view_obj setBackgroundColor:[UIColor clearColor]];
    
    [self.navigationController.view addSubview:view_obj];
    

// Blur effect BG
//    UIView *backgroundView = [[UIView alloc] initWithFrame:view_obj.bounds];
//    
//    UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleLight]];
//    
//    [visualEffectView setFrame:backgroundView.bounds];
//    [backgroundView addSubview:visualEffectView];
//    
//    [view_obj insertSubview:backgroundView atIndex:0];
    
//    UIView *view_subHeader = (UIView *)[self.navigationController.view viewWithTag:420];
//    view_subHeader.layer.cornerRadius = 10;
//    view_subHeader.layer.masksToBounds = YES;
}


#pragma mark - top menu Actions

- (IBAction)action_ShoppingCart:(id)sender
{
    if (ApplicationDelegate.single.array_cartObjects)
    {
        ApplicationDelegate.single.string_controllerName = k_shoppingCart;
    
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:nil message:@"Cart is empty" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil] show];
    }
}

- (IBAction)action_home:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)action_menu:(id)sender
{
    UIView *view_temp = [self.navigationController.view viewWithTag:6];
    
    [UIView animateWithDuration:k_menuAnimationTime
                     animations:^{
                         
                         [view_temp setFrame:CGRectMake( 0,
                                                        0.0f,
                                                        self.view.frame.size.width,
                                                        self.view.frame.size.height)];
                         
                     }
                     completion:^(BOOL finished){
                     }
     ];
}

- (IBAction)action_back:(id)sender
{
    DLog(@"%@", [self.navigationController.visibleViewController class]);
    
    id viewController = self.navigationController.visibleViewController;
    if (![viewController isKindOfClass:[VC_ProductList class]])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}





#pragma mark - full screen menu
- (IBAction)action_hideMenu:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    
    [UIView animateWithDuration:k_menuAnimationTime
                     animations:^{
                         
                         [btn.superview setFrame:CGRectMake( self.view.frame.size.width,                               //notice this is ON screen!
                                                            0.0f,
                                                            self.view.frame.size.width,
                                                            self.view.frame.size.height)];
                         
                     }
                     completion:^(BOOL finished){
                     }
     ];
}



- (IBAction)action_logOut:(id)sender
{
    ApplicationDelegate.single.array_cartObjects = nil;
    
    ApplicationDelegate.single.string_controllerName = k_logOut;
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)action_storeLocator:(id)sender
{
    ApplicationDelegate.single.string_controllerName = k_storeLocator;
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)action_scan:(id)sender
{
    ApplicationDelegate.single.string_controllerName = k_scan;
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)action_barCode:(id)sender
{
    ApplicationDelegate.single.string_controllerName = k_barCodeScan;
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)action_signInRegister:(id)sender
{
    ApplicationDelegate.single.string_controllerName = k_Login;
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)action_viewLoyalPoints:(id)sender
{
    ApplicationDelegate.single.string_controllerName = k_loyalityPoint;
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

@end