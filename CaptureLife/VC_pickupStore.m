//
//  VC_pickupStore.m
//  CaptureLife
//
//  Created by Fahim Bilwani on 11/06/2015.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import "VC_pickupStore.h"
#import "cell_Category.h"

@interface VC_pickupStore ()
{
    NSArray *array_stores;
}
@end

@implementation VC_pickupStore

#pragma mark - cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    array_stores = [[NSArray alloc] initWithObjects:@"Naperville, IL",@"Bolingbrook, IL",@"Joliet, IL", nil];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self method_setHeading];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    array_stores = nil;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - set heading
-(void)method_setHeading
{
    UILabel *label_heading = (UILabel *)[self.navigationController.view viewWithTag:999];
    
    [label_heading setText:@"Pick up Store"];
}

#pragma mark - tableview dataSource and delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [array_stores count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell_Category *cell = (cell_Category *)[tableView dequeueReusableCellWithIdentifier:@"cell_Category"];
    
    if (cell == nil)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"cell_Category" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    [cell method_setCategoryName:[array_stores objectAtIndex:indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.storeName = [array_stores objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"segue_exitToProductDetail" sender:self];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
