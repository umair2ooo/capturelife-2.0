#import <UIKit/UIKit.h>

#import "WebServiceEngine.h"
#import "Singleton.h"
//commit
#define ApplicationDelegate ((AppDelegate *)[UIApplication sharedApplication].delegate)


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) WebServiceEngine *webServiceEngine;
@property (nonatomic, strong) Singleton *single;

@end