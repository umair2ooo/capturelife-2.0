#import "VC_loyalityPoints.h"

@interface VC_loyalityPoints ()
{
    NSTimer *timer_Points;
}
@property (strong, nonatomic) IBOutlet MACircleProgressIndicator *smallProgressIndicator;
@property (strong, nonatomic) IBOutlet MACircleProgressIndicator *largeProgressIndicator;
@property (weak, nonatomic) IBOutlet UITableView *tableview_;
@end

@implementation VC_loyalityPoints

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self method_addViewsToNavController];
    
    
    MACircleProgressIndicator *appearance = [MACircleProgressIndicator appearance];
    
    // The color property sets the actual color of the procress circle (how
    // suprising ;) )
    appearance.color = [UIColor orangeColor];
    timer_Points = [NSTimer scheduledTimerWithTimeInterval:0.2f target:self selector:@selector(method_createCircle) userInfo:nil repeats:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    [self method_setHeading];
    [self method_setAddToCartCounterLabel];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
#pragma mark - method_setAddToCartCounterLabel
-(void)method_setAddToCartCounterLabel
{
    UIView *view_counter;
    if (ApplicationDelegate.single.array_cartObjects)
    {
        if ([ApplicationDelegate.single.array_cartObjects count])
        {
            view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
            UILabel *label_counter = (UILabel *)[self.navigationController.view viewWithTag:888];
            [view_counter setHidden:NO];
            label_counter.text = [NSString stringWithFormat:@"%lu",(unsigned long)[ApplicationDelegate.single.array_cartObjects count]];
        }
        else
        {
            view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
            [view_counter setHidden:YES];
        }
        
    }
    else
    {
        view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
        [view_counter setHidden:YES];
    }
}


#pragma mark - set heading
-(void)method_setHeading
{
    UILabel *label_heading = (UILabel *)[self.navigationController.view viewWithTag:999];
    
    [label_heading setText:@"Loyality Points"];
}

-(void)method_createCircle
{
    if (self.smallProgressIndicator.value <= 0.600000)
    {
        float newValue = self.smallProgressIndicator.value + 0.1;
        self.smallProgressIndicator.value = newValue;
    }
    else
    {
        [timer_Points invalidate];
    }
}

#pragma mark - method_addViewsToNavController
-(void)method_addViewsToNavController
{
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"View_topMenuSubCategory"
                                                         owner:self
                                                       options:nil];
    UIView *view_obj = nibContents[0];
    
    CGRect rect_new = CGRectMake(8,
                                 k_topBar_y,
                                 self.view.frame.size.width-16,
                                 view_obj.frame.size.height);

    
    
    
//    CGRect rect_new = CGRectMake(self.view.frame.size.width/2 - view_obj.frame.size.width/2,
//                                 k_topBar_y,
//                                 view_obj.frame.size.width,
//                                 view_obj.frame.size.height);
    
    
    view_obj.frame = rect_new;
    
    [self.navigationController.view addSubview:view_obj];
    
    
    
    nibContents = nil;
    
    
    
    nibContents = [[NSBundle mainBundle] loadNibNamed:@"View_menuSubCategory"
                                                owner:self
                                              options:nil];
    view_obj = nibContents[0];
    
    [view_obj setFrame:CGRectMake( self.view.frame.size.width,              //notice this is OFF screen!
                                  0,
                                  self.view.frame.size.width,
                                  self.view.frame.size.height)];
    
//    [view_obj setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
//    [view_obj setBackgroundColor:[UIColor clearColor]];
    
    [self.navigationController.view addSubview:view_obj];
    
    
    
//    // Blur effect BG
//    UIView *backgroundView = [[UIView alloc] initWithFrame:view_obj.bounds];
//    
//    UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleLight]];
//    
//    [visualEffectView setFrame:backgroundView.bounds];
//    [backgroundView addSubview:visualEffectView];
//    
//    [view_obj insertSubview:backgroundView atIndex:0];
    
//    UIView *view_subHeader = (UIView *)[self.navigationController.view viewWithTag:420];
//    view_subHeader.layer.cornerRadius = 10;
//    view_subHeader.layer.masksToBounds = YES;
}
#pragma mark - top menu Actions

- (IBAction)action_ShoppingCart:(id)sender
{
    if (ApplicationDelegate.single.array_cartObjects)
    {
        
        ApplicationDelegate.single.string_controllerName = k_shoppingCart;
        
        [self.navigationController dismissViewControllerAnimated:YES completion:^{
        }];
        
//        [self.navigationController presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"Nav_Cart"]
//                                            animated:YES
//                                          completion:nil];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:nil message:@"Cart is empty" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil] show];
    }
}

- (IBAction)action_home:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)action_menu:(id)sender
{
    UIView *view_temp = [self.navigationController.view viewWithTag:6];
    
    [UIView animateWithDuration:k_menuAnimationTime
                     animations:^{
                         
                         [view_temp setFrame:CGRectMake( 0,
                                                        0.0f,
                                                        self.view.frame.size.width,
                                                        self.view.frame.size.height)];
                         
                     }
                     completion:^(BOOL finished){
                     }
     ];
}

- (IBAction)action_back:(id)sender
{
    DLog(@"%@", [self.navigationController.visibleViewController class]);
    
    id viewController = self.navigationController.visibleViewController;
    if (![viewController isKindOfClass:[VC_loyalityPoints class]])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}





#pragma mark - full screen menu
- (IBAction)action_hideMenu:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    [UIView animateWithDuration:k_menuAnimationTime
                     animations:^{
                         
                         [btn.superview setFrame:CGRectMake( self.view.frame.size.width,                               //notice this is ON screen!
                                                            0.0f,
                                                            self.view.frame.size.width,
                                                            self.view.frame.size.height)];
                         
                     }
                     completion:^(BOOL finished){
                     }
     ];
}



- (IBAction)action_logOut:(id)sender
{
    ApplicationDelegate.single.array_cartObjects = nil;
    
    ApplicationDelegate.single.string_controllerName = k_logOut;
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)action_storeLocator:(id)sender
{
    ApplicationDelegate.single.string_controllerName = k_storeLocator;
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)action_scan:(id)sender
{
    ApplicationDelegate.single.string_controllerName = k_scan;
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)action_viewLoyalPoints:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    [self action_hideMenu:btn.superview];
}

- (IBAction)action_barCode:(id)sender
{
    ApplicationDelegate.single.string_controllerName = k_barCodeScan;
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)action_signInRegister:(id)sender
{
    ApplicationDelegate.single.string_controllerName = k_Login;
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}



@end