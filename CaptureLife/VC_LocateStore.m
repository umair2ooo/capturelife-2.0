#import "VC_LocateStore.h"
#import <MapKit/MapKit.h>
#import "Cell_directionOnMap.h"

#define k_gap 5.0

@interface VC_LocateStore ()<CLLocationManagerDelegate, MKMapViewDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,DeleteObject>
{
    CLLocationDistance minimumDistance;
    NSString *currentLat;
    NSString *currentLong;
}

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong)CLLocation *location_current;
@property (strong, nonatomic) MKNetworkOperation *operation;
@property(nonatomic, strong)NSArray *array_store;
@property(nonatomic, strong)NSMutableArray *array_stores;



@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UITableView *tableView_;
@property (weak, nonatomic) IBOutlet UITextField *text_Miles;
@property (weak, nonatomic) IBOutlet UITextField *text_City;
@property (weak, nonatomic) IBOutlet UITextField *text_State;
@property (weak, nonatomic) IBOutlet UITextField *text_ZipCode;
@property (weak, nonatomic) IBOutlet UIView *view_textFields;
@property (weak, nonatomic) IBOutlet UILabel *label_counter;




- (IBAction)action_Search:(id)sender;


@end

@implementation VC_LocateStore

#pragma mark - set heading
-(void)method_setHeading
{
    UILabel *label_heading = (UILabel *)[self.view viewWithTag:999];
    
    [label_heading setText:@"Search Store"];
}


#pragma mark - cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self method_addViewsToNavController];
    
    [self.view_textFields setHidden:YES];
    
    
    if (![CLLocationManager locationServicesEnabled])
    {
        [[[UIAlertView alloc] initWithTitle:@""
                                    message:@"Turn ON Location Service to Allow CaptureLife to Determine Your Location"
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil, nil]
         show];
    }
    else
    {
        [self method_currentLocation];
    }
}


-(void)viewWillAppear:(BOOL)animated
{
    [self method_setHeading];
    [self method_setAddToCartCounterLabel];
}


-(void)viewWillDisappear:(BOOL)animated
{
    self.tableView_.delegate = nil;
    self.tableView_.dataSource = nil;
    
    self.mapView.delegate = nil;
    
    self.array_stores = nil;
    
    [self.locationManager stopUpdatingLocation];
    self.locationManager.delegate = nil;
    self.locationManager = nil;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - method_setAddToCartCounterLabel
-(void)method_setAddToCartCounterLabel
{
    UIView *view_counter;
    if (ApplicationDelegate.single.array_cartObjects)
    {
        if ([ApplicationDelegate.single.array_cartObjects count])
        {
            view_counter = (UIView *)[self.view viewWithTag:891];
            UILabel *label_counter = (UILabel *)[self.view viewWithTag:888];
            [view_counter setHidden:NO];
            label_counter.text = [NSString stringWithFormat:@"%lu",(unsigned long)[ApplicationDelegate.single.array_cartObjects count]];
        }
        else
        {
            view_counter = (UIView *)[self.view viewWithTag:891];
            [view_counter setHidden:YES];
        }
        
    }
    else
    {
        DLog(@"%@",(UIView *)[self.view viewWithTag:891]);
        view_counter = (UIView *)[self.view viewWithTag:891];
        [view_counter setHidden:YES];
    }
}



#pragma mark - DropPins
-(void)DropPin
{
    MKMapRect zoomRect = MKMapRectNull;
    for (int i = 0; i<[self.array_stores count]; i++)
    {
        MKPointAnnotation *mapAnnotation = [[MKPointAnnotation alloc] init];
        mapAnnotation.title = [[self.array_stores objectAtIndex:i] valueForKey:@"name"];
        // mapAnnotation.subtitle = [[restaurants objectAtIndex:i] valueForKey:@"address"];
        CLLocationCoordinate2D cordinate;
        cordinate.latitude = [[[self.array_stores objectAtIndex:i] valueForKey:@"lat"] floatValue];
        cordinate.longitude = [[[self.array_stores objectAtIndex:i] valueForKey:@"lon"] floatValue];
        mapAnnotation.coordinate = cordinate;
        [self.mapView addAnnotation:mapAnnotation];
        MKMapPoint annotationPoint = MKMapPointForCoordinate(mapAnnotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
        zoomRect = MKMapRectUnion(zoomRect, pointRect);
    }
    if ([self.array_stores count])
    {
        [self.mapView setVisibleMapRect:zoomRect animated:YES];
    }
}


#pragma mark - creating map and drawing path
-(void)method_createMapAndDrawPath
{
    [self.mapView setFrame:CGRectMake(k_gap, 53, self.view.frame.size.width-(k_gap*2), self.view.frame.size.height/3*2 -150)];
    
//    if (label_counter == nil)
//    {
//        label_counter = [[UILabel alloc] initWithFrame:CGRectMake(k_gap,
//                                                                  self.mapView.frame.origin.y+self.mapView.frame.size.height,
//                                                                  200,
//                                                                  30)];
//    }

//    [self.label_counter setBackgroundColor:[UIColor clearColor]];
//    [self.label_counter setTextColor:[UIColor blackColor]];
    self.label_counter.text = [NSString stringWithFormat:@"Total Locations: %lu", (unsigned long)[self.array_stores count]];

    
//    [self.view addSubview:self.label_counter];
    
    
    
    
//    if (self.tableView_ == nil)
//    {
//        self.tableView_ = [[UITableView alloc] initWithFrame:CGRectMake(k_gap,
//                                                                        label_counter.frame.origin.y + label_counter.frame.size.height+k_gap,
//                                                                        self.view.frame.size.width-(k_gap*2),
//                                                                        self.view.frame.size.height -
//                                                                        (label_counter.frame .origin.y + label_counter.frame.size.height))
//                                                       style:UITableViewStylePlain];
//    }
//    
//    self.tableView_.delegate = self;
//    self.tableView_.dataSource = self;
//    [self.view addSubview:self.tableView_];
    
    
    UINib *nib = [UINib nibWithNibName:@"Cell_directionOnMap" bundle:nil];
    [[self tableView_] registerNib:nib forCellReuseIdentifier:@"Cell_directionOnMap"];
    
    
    [self.tableView_ selectRowAtIndexPath:0
                                 animated:YES
                           scrollPosition:UITableViewScrollPositionMiddle];
    
    
    [self.tableView_ reloadData];
    [self DropPin];
}


#pragma mark - current location
-(void)method_currentLocation
{
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        //for background and foreground
        //[self.locationManager requestAlwaysAuthorization]
        //foreground
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    [self.locationManager startUpdatingLocation];
    self.mapView.showsUserLocation = YES;
}

#pragma mark - action_goBack
-(void)action_goBack
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark- uitextfield delegate

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == self.text_ZipCode)
    {
        [self.view_textFields setHidden:YES];
    }
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;
{
    if ([self.text_ZipCode isEditing])
    {
        self.text_City.text = @"";
        self.text_Miles.text = @"";
        self.text_State.text = @"";
        [self.view_textFields setHidden:NO];
    }
    else
    {
        [self.view_textFields setHidden:YES];
        self.text_ZipCode.text = @"";
    }
    return YES;
}

#pragma mark - table view delegate and dataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.array_stores count]?[self.array_stores count]:0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Cell_directionOnMap *cell = (Cell_directionOnMap *)[tableView dequeueReusableCellWithIdentifier:@"Cell_directionOnMap"];
    if (cell == nil) {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"Cell_directionOnMap" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    cell.delegate = self;
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    cell.label_storeName.text = [[self.array_stores objectAtIndex:indexPath.row] valueForKey:@"name"];
    cell.label_storeDesc.text = [[self.array_stores objectAtIndex:indexPath.row] valueForKey:@"address"];
    cell.button_Direction.tag = indexPath.row;
    cell.label_storeDistance.text = [NSString stringWithFormat:@"%.0f mi",[self method_calculateDistance:[[self.array_stores objectAtIndex:indexPath.row] valueForKey:@"latitude"] lon:[[self.array_stores objectAtIndex:indexPath.row] valueForKey:@"longitude"]]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 110.0;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - didUpdateToLocation
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *newLocation = [locations lastObject];
    CLLocation *oldLocation;
    if (locations.count > 1)
    {
        oldLocation = [locations objectAtIndex:locations.count-2];
    }
    else
    {
        oldLocation = nil;
    }

    if (!self.location_current)
    {
        NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
        NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
        
        
        self.location_current = newLocation;
        
        currentLat = [NSString stringWithFormat:@"%.8f", self.location_current.coordinate.latitude];
        currentLong = [NSString stringWithFormat:@"%.8f", self.location_current.coordinate.longitude];
        
        
        //        for (unsigned i =0; i<[self.array_stores count]; i++)
        //        {
        //            [self method_findNearestStore_destinationDic:[self.array_stores objectAtIndex:i]];
        //        }
        
        //[self method_createMapAndDrawPath];
    }
    
    newLocation = nil;
    oldLocation = nil;
}
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    //[self.mapView setCenterCoordinate:userLocation.coordinate animated:YES];
}

#pragma mark - action_Search
- (IBAction)action_Search:(id)sender
{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    [self.view endEditing:YES];
    if ([self.text_ZipCode.text length])
    {
        //[self.view_textFields setHidden:YES];
        if (![self.operation isExecuting])
        {
            self.operation = [ApplicationDelegate.webServiceEngine method_getZipcodeViaLocation:self.text_ZipCode.text
                                                                         completionHandler:^(id response)
                              {
                                  [SVProgressHUD dismiss];
                                  NSDictionary *result = response;
                                  NSLog(@"result:%@",result);
                                  if (response != nil)
                                  {
                                      NSLog(@"Array: %@", result);
                                      if ([result valueForKey:@"results"])
                                      {
                                          if ([[result valueForKey:@"status"] isEqualToString:@"ZERO_RESULTS"])
                                          {
                                              [[[UIAlertView alloc] initWithTitle:@"Failure"
                                                                          message:@"Invalid zip code."
                                                                         delegate:nil
                                                                cancelButtonTitle:@"Ok"
                                                                otherButtonTitles:nil, nil]
                                               show];
                                              [self method_failureZipCodeResponse];
                                          }
                                          else
                                          {
                                              if ([[result valueForKey:@"results"] count] >= 1)
                                              {
                                                  if ([[[result valueForKey:@"results"] objectAtIndex:0] valueForKey:@"geometry"])
                                                  {
                                                      [self method_fetchLocationByZipCode_latitude:[[[[[result valueForKey:@"results"] objectAtIndex:0] valueForKey:@"geometry"] valueForKey:@"location"] valueForKey:@"lat"] longitute:[[[[[result valueForKey:@"results"] objectAtIndex:0] valueForKey:@"geometry"] valueForKey:@"location"] valueForKey:@"lng"]];
                                                  }
                                                  else
                                                  {
                                                      [self method_failureZipCodeResponse];
                                                      [[[UIAlertView alloc] initWithTitle:@"Failure"
                                                                                  message:@"Invalid zip code."
                                                                                 delegate:nil
                                                                        cancelButtonTitle:@"Ok"
                                                                        otherButtonTitles:nil, nil]
                                                       show];
                                                      
                                                  }
                                              }
                                              else
                                              {
                                                  [self method_failureZipCodeResponse];
                                                  [[[UIAlertView alloc] initWithTitle:@"Failure"
                                                                              message:@"Invalid zip code."
                                                                             delegate:nil
                                                                    cancelButtonTitle:@"Ok"
                                                                    otherButtonTitles:nil, nil]
                                                   show];
                                                  
                                              }
                                          }
                                      }
                                      else
                                      {
                                          [self method_failureZipCodeResponse];
                                          [[[UIAlertView alloc] initWithTitle:@"Failure"
                                                                      message:@"Invalid zip code."
                                                                     delegate:nil
                                                            cancelButtonTitle:@"Ok"
                                                            otherButtonTitles:nil, nil]
                                           show];
                                      }
                                  }
                                  else
                                  {
                                      [[[UIAlertView alloc] initWithTitle:nil message:@"There is something wrong, from the server side,please try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                                  }
                              }
                                                                              errorHandler:^(NSError *error)
                              {
                                  [SVProgressHUD dismiss];
                                  DLog(@"there is something is wrong, please try with valid id and pass");
                                  //[self performSegueWithIdentifier:@"segue_ProductList" sender:nil];
                                  DLog(@"1:%@\n2:%@\n3:%@\n4:%@",
                                       [error localizedDescription],
                                       [error localizedFailureReason],
                                       [error localizedRecoveryOptions],
                                       [error localizedRecoverySuggestion]);
                              }
                              ];
        }
        
        [self.operation onDownloadProgressChanged:^(double progress)
         {
             DLog(@"download progress = %.2f", progress*100.0);
         }];
    }
    else
    {
        //if we pass city empty then also web service returns stores
        if (![self.operation isExecuting])
        {
            self.operation = [ApplicationDelegate.webServiceEngine method_fetchLocationByCityStateNRadius:@"product_id" City:self.text_City.text State:self.text_State.text Miles:self.text_Miles.text
                                                                              completionHandler:^(id response)
                              {
                                  [SVProgressHUD dismiss];
                                  NSDictionary *result = response;
                                  NSLog(@"result:%@",result);
                                  if (response != nil)
                                  {
                                      if ([result valueForKey:@"PhysicalStore"])
                                      {
                                          self.array_stores = [[NSMutableArray alloc] init];
                                          [(NSArray *)[result valueForKey:@"PhysicalStore"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
                                           {
                                               
                                               [self.array_stores addObject:[[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                                                             [obj valueForKey:@"storeName"],@"name",[obj valueForKey:@"latitude"],@"latitude",[obj valueForKey:@"longitude"],@"longitude",[[obj valueForKey:@"addressLine"] objectAtIndex:0],@"address", nil]];
                                           }];
                                          DLog(@"%@",self.array_stores);
                                          [self method_createMapAndDrawPath];
                                          //self.array_stores = nil;
                                      }
                                      else
                                      {
                                          self.array_stores = nil;
                                          [self method_createMapAndDrawPath ];
                                          [[[UIAlertView alloc] initWithTitle:@"Failure" message:@"Data not found." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                                          [self.mapView removeAnnotations:[self.mapView annotations]];
                                      }
                                  }
                                  else
                                  {
                                      [[[UIAlertView alloc] initWithTitle:nil message:@"There is something wrong, from the server side,please try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                                  }
                              }
                                                                                   errorHandler:^(NSError *error)
                              {
                                  [SVProgressHUD dismiss];
                                  DLog(@"there is something is wrong, please try with valid id and pass");
                                  //[self performSegueWithIdentifier:@"segue_ProductList" sender:nil];
                                  DLog(@"1:%@\n2:%@\n3:%@\n4:%@",
                                       [error localizedDescription],
                                       [error localizedFailureReason],
                                       [error localizedRecoveryOptions],
                                       [error localizedRecoverySuggestion]);
                              }
                              ];
        }
        
        [self.operation onDownloadProgressChanged:^(double progress)
         {
             DLog(@"download progress = %.2f", progress*100.0);
         }];
    }
}


-(void)method_fetchLocationByZipCode_latitude:(NSString *)lat longitute:(NSString *)lon
{
    if (![self.operation isExecuting])
    {
        self.operation = [ApplicationDelegate.webServiceEngine method_fetchLocationByZipcode_Latitude:lat Longitude:lon
                                                                          completionHandler:^(id response)
                          {
                              NSDictionary *result = response;
                              NSLog(@"result:%@",result);
                              if (response != nil)
                              {
                                  if ([result valueForKey:@"PhysicalStore"])
                                  {
                                      self.array_stores = [[NSMutableArray alloc] init];
                                      [(NSArray *)[result valueForKey:@"PhysicalStore"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
                                      {
//                                          StoreLocationDataClass *obj_ = [[StoreLocationDataClass alloc] init];
//                                          [obj_ setStore_name:[obj valueForKey:k_storeLocName]];
//                                          [obj_ setStore_latitude:[obj valueForKey:k_latitude]];
//                                          [obj_ setStore_longitude:[obj valueForKey:k_longitude]];
//                                          [obj_ setStore_address:[[obj valueForKey:k_address] objectAtIndex:0]];
                                          [self.array_stores addObject:[[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                                                        [obj valueForKey:@"storeName"],@"name",[obj valueForKey:@"latitude"],@"latitude",[obj valueForKey:@"longitude"],@"longitude",[[obj valueForKey:@"addressLine"] objectAtIndex:0],@"address", nil]];
                                      }];
                                      DLog(@"%@",self.array_stores);
                                      [self method_createMapAndDrawPath];
                                      //self.array_stores = nil;

                                  }
                                  else
                                  {
                                      self.array_stores = nil;
                                      [self method_createMapAndDrawPath ];
                                      [[[UIAlertView alloc] initWithTitle:@"Failure" message:@"Data not found." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                                      [self.mapView removeAnnotations:[self.mapView annotations]];
                                  }

                              }
                              else
                              {
                                  [[[UIAlertView alloc] initWithTitle:nil message:@"There is something wrong, from the server side,please try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                              }
                          }
                                                                               errorHandler:^(NSError *error)
                          {
                              DLog(@"there is something is wrong, please try with valid id and pass");
                              //[self performSegueWithIdentifier:@"segue_ProductList" sender:nil];
                              DLog(@"1:%@\n2:%@\n3:%@\n4:%@",
                                   [error localizedDescription],
                                   [error localizedFailureReason],
                                   [error localizedRecoveryOptions],
                                   [error localizedRecoverySuggestion]);
                          }
                          ];
    }
    
    [self.operation onDownloadProgressChanged:^(double progress)
     {
         DLog(@"download progress = %.2f", progress*100.0);
     }];
}



#pragma mark - method_direction
-(void)method_direction:(int)tag
{
    DLog(@"%d", tag);
    NSString* url = [NSString stringWithFormat: @"http://%@/maps?saddr=%@,%@&daddr=%@,%@",@"maps.apple.com",currentLat,currentLong,[[self.array_stores objectAtIndex:tag] valueForKey:@"latitude"],[[self.array_stores objectAtIndex:tag] valueForKey:@"longitude"]];
    DLog(@"url:%@", url);
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
}
#pragma mark - method_calculateDistance
-(float)method_calculateDistance:(NSString *)lat lon:(NSString *)lon
{
    CLLocation *location1 = [[CLLocation alloc] initWithLatitude:[currentLat floatValue] longitude:[currentLong floatValue]];
    CLLocation *location2 = [[CLLocation alloc] initWithLatitude:[lat floatValue] longitude:[lon floatValue]];
    // now calculating the distance using inbuild method distanceFromLocation:
    float distInMeter = [location1 distanceFromLocation:location2]; // which returns in meters
    //converting meters to mile
    float distInMile = 0.000621371192 * distInMeter;
    DLog(@"Actual Distance : %0f",distInMile);
    return distInMile;
}

#pragma mark - method_validateZipcode
-(BOOL)method_validateZipcode
{
    NSString *postcodeRegex = @"^(\\d{5}(-\\d{4})?|[a-z]\\d[a-z][- ]*\\d[a-z]\\d)$";//for canada and US only
    NSPredicate *postcodeValidate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", postcodeRegex];
    
    if ([postcodeValidate evaluateWithObject:self.text_ZipCode.text] == YES)
    {
        NSLog (@"Postcode is Valid");
        return YES;
    } else {
        NSLog (@"Postcode is Invalid");
        return NO;
    }
}


#pragma mark - failureGoogleApiResponse
-(void)method_failureZipCodeResponse
{
    self.array_stores = nil;
    [self method_createMapAndDrawPath ];
}





#pragma mark - method_addViewsToNavController
-(void)method_addViewsToNavController
{
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"View_topMenuSubCategory"
                                                         owner:self
                                                       options:nil];
    UIView *view_obj = nibContents[0];
    
    CGRect rect_new = CGRectMake(8,
                                 k_topBar_y,
                                 self.view.frame.size.width-16,
                                 view_obj.frame.size.height);
    
//    CGRect rect_new = CGRectMake(self.view.frame.size.width/2 - view_obj.frame.size.width/2,
//                                 k_topBar_y,
//                                 view_obj.frame.size.width,
//                                 view_obj.frame.size.height);
    
    
    
    
    view_obj.frame = rect_new;
    
    [self.view addSubview:view_obj];
    
    
    
    nibContents = nil;
    
    
    
    nibContents = [[NSBundle mainBundle] loadNibNamed:@"View_menuSubCategory"
                                                owner:self
                                              options:nil];
    view_obj = nibContents[0];
    
    [view_obj setFrame:CGRectMake( self.view.frame.size.width,              //notice this is OFF screen!
                                  0,
                                  self.view.frame.size.width,
                                  self.view.frame.size.height)];
    
//    [view_obj setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
//    [view_obj setBackgroundColor:[UIColor clearColor]];
    
    [self.view addSubview:view_obj];
    
//    UIView *backgroundView = [[UIView alloc] initWithFrame:view_obj.bounds];
//    
//    
//    UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleLight]];
//    
//    [visualEffectView setFrame:backgroundView.bounds];
//    [backgroundView addSubview:visualEffectView];
//    
//    [view_obj insertSubview:backgroundView atIndex:0];
    
//    UIView *view_subHeader = (UIView *)[self.navigationController.view viewWithTag:420];
//    view_subHeader.layer.cornerRadius = 10;
//    view_subHeader.layer.masksToBounds = YES;
}



#pragma mark - top menu Actions

- (IBAction)action_ShoppingCart:(id)sender
{
    if (ApplicationDelegate.single.array_cartObjects)
    {        
        ApplicationDelegate.single.string_controllerName = k_shoppingCart;
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:nil message:@"Cart is empty" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil] show];
    }
}

- (IBAction)action_home:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)action_menu:(id)sender
{
    UIView *view_temp = [self.view viewWithTag:6];
    
    [UIView animateWithDuration:k_menuAnimationTime                                                         // Show menu
                     animations:^{
                         
                         [view_temp setFrame:CGRectMake( 0,
                                                        0.0f,
                                                        self.view.frame.size.width,
                                                        self.view.frame.size.height)];
                         
                     }
                     completion:^(BOOL finished){
                     }
     ];
}

- (IBAction)action_back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}





#pragma mark - full screen menu
- (IBAction)action_hideMenu:(id)sender
{
        UIButton *btn = (UIButton *)sender;
    
    [UIView animateWithDuration:k_menuAnimationTime                                                         // Hide menu
                     animations:^{
                         
                         [btn.superview setFrame:CGRectMake( self.view.frame.size.width,                               //notice this is ON screen!
                                                            0.0f,
                                                            self.view.frame.size.width,
                                                            self.view.frame.size.height)];
                         
                     }
                     completion:^(BOOL finished){
                     }
     ];
}



- (IBAction)action_logOut:(id)sender
{
    ApplicationDelegate.single.array_cartObjects = nil;
    
    ApplicationDelegate.single.string_controllerName = k_logOut;
    
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}


- (IBAction)action_storeLocator:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    [self action_hideMenu:btn.superview];
}


- (IBAction)action_scan:(id)sender
{
    ApplicationDelegate.single.string_controllerName = k_scan;
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)action_viewLoyalPoints:(id)sender
{
    ApplicationDelegate.single.string_controllerName = k_loyalityPoint;
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)action_barCode:(id)sender
{
    ApplicationDelegate.single.string_controllerName = k_barCodeScan;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)action_signInRegister:(id)sender
{
    ApplicationDelegate.single.string_controllerName = k_Login;
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end