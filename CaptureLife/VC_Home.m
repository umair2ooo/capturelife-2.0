#import "VC_Home.h"

#import "cell_Category.h"
#import <Wit/Wit.h>
#import "VC_ProductList.h"
#import "VC_SubCategory.h"
#import "scroller_home.h"
#import "VC_Zipcode.h"
#import "VC_test.h"
#import "EAIntroView.h"
#import "EARestrictedScrollView.h"


@interface VC_Home ()<WitDelegate,UISearchBarDelegate, DemoTableControllerDelegate, FPPopoverControllerDelegate, homeScrollerProtocolDelegate,EAIntroDelegate>
{
    NSString *catID;
    NSString *productName;
    UIAlertView *alert;
    NSMutableArray *array_predictions;
    BOOL bool_isSpeechDelegateCall;
    
    FPPopoverController *popover;
    DemoTableController *controller;
    
    UIButton *button_scroller;
    EAIntroView * intro;
}

//@property(nonatomic, strong)MKNetworkEngine *engine_solarSearch;

@property (strong, nonatomic) MKNetworkOperation *operation;
@property(nonatomic, strong)NSMutableArray *array_categories;

@property (weak, nonatomic) IBOutlet UIView *view_pageScroller;
@property (weak, nonatomic) IBOutlet WITMicButton *witButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scroller_;
@property (weak, nonatomic) IBOutlet UIView *view_categoryBackView;
@property (weak, nonatomic) IBOutlet UITableView *tableview_;
@property (weak, nonatomic) IBOutlet UIView *view_searchBarBg;
@property (weak, nonatomic) IBOutlet UITextField *textfield_search;

#pragma mark - top menu actions
- (IBAction)action_ShoppingCart:(id)sender;
- (IBAction)action_menu:(id)sender;
- (IBAction)action_back:(id)sender;
- (IBAction)action_home:(id)sender;

#pragma mark - full menu actions
- (IBAction)action_hideMenu:(id)sender;
- (IBAction)action_logOut:(id)sender;
- (IBAction)action_storeLocator:(id)sender;
- (IBAction)action_scan:(id)sender;
- (IBAction)action_viewLoyalPoints:(id)sender;
- (IBAction)action_barCode:(id)sender;
- (IBAction)action_topCategories:(id)sender;
- (IBAction)action_signInRegister:(id)sender;


- (IBAction)action_hideCategories:(id)sender;
- (IBAction)action_QRcodeScan:(id)sender;

@end

@implementation VC_Home



#pragma mark - method_setAddToCartCounterLabel
-(void)method_setAddToCartCounterLabel
{
    UIView *view_counter;
    if (ApplicationDelegate.single.array_cartObjects)
    {
        if ([ApplicationDelegate.single.array_cartObjects count])
        {
            view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
            UILabel *label_counter = (UILabel *)[self.navigationController.view viewWithTag:888];
            [view_counter setHidden:NO];
            label_counter.text = [NSString stringWithFormat:@"%lu",(unsigned long)[ApplicationDelegate.single.array_cartObjects count]];
        }
        else
        {
            view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
            [view_counter setHidden:YES];
        }
        
    }
    else
    {
        view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
        [view_counter setHidden:YES];
    }
}

#pragma mark - method_setHeaderLabel
-(void)method_setHeaderLabel:(BOOL)hide
{
    DLog(@"%d",hide);
    UIView *view_header = (UIView *)[self.navigationController.view viewWithTag:890];
    [view_header setHidden:hide];
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"zipCodePopupClosed" object:nil];
//    UILabel *label_header1 = (UILabel *)[self.navigationController.view viewWithTag:890];
//    [label_header1 setHidden:hide];
}



#pragma mark - set up FP pop over
-(void)method_setupFPPopOver
{
    if (popover == nil)
    {
        controller = [[DemoTableController alloc] init];
        controller.delegated = self;
        controller.title = nil;
        popover = [[FPPopoverController alloc] initWithViewController:controller];
        popover.delegate = self;
        popover.border = YES;
        popover.arrowDirection = FPPopoverArrowDirectionDown;
        popover.contentSize = CGSizeMake(270,210);
    }
}


#pragma mark - method_zipCodeAsOverlay
-(void)method_zipCodeAsOverlay
{
    VC_Zipcode *objVC_Zipcode = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_Zipcode"];
    [self presentPopupViewController:objVC_Zipcode animationType:MJPopupViewAnimationSlideTopBottom];
}

#pragma mark - setupPageScroller
- (void)showIntroWithCrossDissolve
{
    
//    if (IS_IPHONE_4_OR_LESS || IS_IPHONE_5)
//    {
//        page1.bgImage = [UIImage imageNamed:@"homeBG1"];
//        UIImage *image = page1.bgImage;
//        DLog(@"%f, %f",image.size.width,image.size.height);
//    }
//    else
//    {
//        page1.bgImage = [UIImage imageNamed:@"homeBG1"];   
//    }
    
    
    
    
    
//    if (IS_IPHONE_4_OR_LESS || IS_IPHONE_5)
//    {
//        page2.bgImage = [UIImage imageNamed:@"homeBG2@2x~568h"];
//    }
//    else
//    {
//        page2.bgImage = [UIImage imageNamed:@"homeBG2"];
//    }
    
    
    
    
    
    
    
//    if (IS_IPHONE_4_OR_LESS || IS_IPHONE_5)
//    {
//        page3.bgImage = [UIImage imageNamed:@"homeBG3@2x~568h"];
//    }
//    else
//    {
//        page3.bgImage = [UIImage imageNamed:@"homeBG3"];
//    }
    
    EAIntroPage *page1 = [EAIntroPage page];
    page1.bgImage = [UIImage imageNamed:@"homeBG1"];
    
    
    EAIntroPage *page2 = [EAIntroPage page];
    page2.bgImage = [UIImage imageNamed:@"homeBG2"];
    
    
    EAIntroPage *page3 = [EAIntroPage page];
    page3.bgImage = [UIImage imageNamed:@"homeBG3"];

    
    
    
    intro = [[EAIntroView alloc] initWithFrame:self.view_pageScroller.bounds andPages:@[page1,page2,page3]];
    [intro setDelegate:self];
    intro.skipButton.hidden = YES;
    
    intro.swipeToExit = NO;
    
    button_scroller = [UIButton buttonWithType:UIButtonTypeCustom];
    [button_scroller.layer setBorderWidth:1.0];
    [button_scroller setTintAdjustmentMode:UIViewTintAdjustmentModeDimmed];
    [button_scroller setReversesTitleShadowWhenHighlighted:YES];
    button_scroller.titleLabel.adjustsFontSizeToFitWidth = YES;
    [button_scroller setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [intro showInView:self.view_pageScroller animateDuration:0.3];
    [self.view_pageScroller addSubview:button_scroller];
}


#pragma mark - cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self method_guestLogin];
    
//    self.scroller_.delegate_ = self;
    
    [self method_GetTopCategories];
    
    
    // set the WitDelegate object
    [Wit sharedInstance].delegate = self;
    
    
    [self method_addViewsToNavController];
    
    [self method_setupFPPopOver];
    
//    self.imageView_searchBg.layer.cornerRadius = 30;
//    self.imageView_searchBg.layer.cornerRadius = YES;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    //[self.scroller_ method_startScroller];
    [self showIntroWithCrossDissolve];
    [self method_setAddToCartCounterLabel];
    
    NSUserDefaults *loginDetails = [NSUserDefaults standardUserDefaults];
    if(![[loginDetails stringForKey:@"isZipcodeEntered"] isEqualToString:@"YES"])
    {
        [self method_setHeaderLabel:YES];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(method_setHeaderLabel:) name:@"zipCodePopupClosed" object:nil];
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(method_zipCodeAsOverlay) userInfo:nil repeats:NO];
    }
    else
    {
        [self method_setHeaderLabel:NO];
    }
}



-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (ApplicationDelegate.single.string_controllerName)
    {
        if ([ApplicationDelegate.single.string_controllerName isEqualToString:k_shoppingCart])
        {
            [self.navigationController presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"Nav_Cart"]
                                                    animated:YES
                                                  completion:nil];
        }
        
        if ([ApplicationDelegate.single.string_controllerName isEqualToString:k_barCodeScan])
        {
            [self.navigationController presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"Nav_barCode"]
                                                    animated:YES
                                                  completion:nil];
        }
        
        
        if ([ApplicationDelegate.single.string_controllerName isEqualToString:k_loyalityPoint])
        {
            [self.navigationController presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"nav_loyalityPoints"]
                                                    animated:YES
                                                  completion:nil];
        }
        
        
        if ([ApplicationDelegate.single.string_controllerName isEqualToString:k_logOut])
        {
            [self.navigationController dismissViewControllerAnimated:YES
                                                          completion:nil];
        }
        
        
        if ([ApplicationDelegate.single.string_controllerName isEqualToString:k_storeLocator])
        {
            [self performSegueWithIdentifier:@"segue_locateStore" sender:nil];
        }

        if ([ApplicationDelegate.single.string_controllerName isEqualToString:k_scan])
        {
            [self performSegueWithIdentifier:@"segue_scan" sender:nil];
        }
        
        if ([ApplicationDelegate.single.string_controllerName isEqualToString:k_Login])
        {
//            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleloginPopupClosedNotification:) name:@"loginPopupClosed" object:nil];
//            VC_loginIn *objVC_loginIn = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_loginIn"];
//            [self presentPopupViewController:objVC_loginIn animationType:MJPopupViewAnimationSlideBottomTop];
            [self performSegueWithIdentifier:@"segue_Login" sender:nil];
        }
        
        ApplicationDelegate.single.string_controllerName = nil;
    }
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillDisappear:(BOOL)animated
{
//    [self.scroller_ method_stopScroller];
    
    if(self.operation)
    {
        [self.operation cancel];
        self.operation = nil;
    }
}


#pragma mark - method_GetTopCategories
-(void)method_GetTopCategories
{
    if (![self.operation isExecuting])
    {
        self.operation = [ApplicationDelegate.webServiceEngine method_getTopCategories:@""
                                                                     completionHandler:^(id response)
                          {
                              NSDictionary *result = response;
                              NSLog(@"result:%@",result);
                              if (response != nil)
                              {
                                  if ([result valueForKey:@"CatalogGroupView"])
                                  {
                                      self.array_categories = [[NSMutableArray alloc] init];
                                      [(NSArray *)[result valueForKey:@"CatalogGroupView"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                          //name
                                          [self.array_categories addObject:[NSDictionary dictionaryWithObjectsAndKeys:[obj valueForKey:@"name"],@"name",[obj valueForKey:@"uniqueID"],@"uniqueID", nil]];
                                      }];
                                      NSLog(@"Array: %@", self.array_categories);
                                      [self.tableview_ reloadData];
                                  }
                                  else
                                  {
                                      self.array_categories = nil;
                                      [self.tableview_ reloadData];
                                      [[[UIAlertView alloc] initWithTitle:@"Failure" message:@"Categories not found." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                                  }
                                  
                              }
                              else
                              {
                                  [[[UIAlertView alloc] initWithTitle:nil message:@"There is something wrong, from the server side,please try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                              }
                          }
                                                                          errorHandler:^(NSError *error)
                          {
                              DLog(@"there is something is wrong, please try with valid id and pass");
                              [[[UIAlertView alloc] initWithTitle:nil message:[error localizedDescription]
                                                         delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]
                               show];
                              DLog(@"1:%@\n2:%@\n3:%@\n4:%@",
                                   [error localizedDescription],
                                   [error localizedFailureReason],
                                   [error localizedRecoveryOptions],
                                   [error localizedRecoverySuggestion]);
                          }
                          ];
    }
    
    [self.operation onDownloadProgressChanged:^(double progress)
     {
         DLog(@"download progress = %.2f", progress*100.0);
     }];
}

#pragma mark - tableview dataSource and delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.array_categories count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell_Category *cell = (cell_Category *)[tableView dequeueReusableCellWithIdentifier:@"cell_Category"];
    
    if (cell == nil)
    {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"cell_Category" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    [cell method_setCategoryName:[[self.array_categories objectAtIndex:indexPath.row] valueForKey:@"name"]];
    
    [cell.contentView.layer setCornerRadius:k_cornerRadiusCell];

   // [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    catID = [[self.array_categories objectAtIndex:indexPath.row] valueForKey:@"uniqueID"];
    DLog(@"%@",catID);
    productName = [[self.array_categories objectAtIndex:indexPath.row] valueForKey:@"name"];
    [self method_getSubCategories:[[self.array_categories objectAtIndex:indexPath.row] valueForKey:@"uniqueID"]];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
#pragma mark - method_getSubCategories
-(void)method_getSubCategories:(NSString *)categoryID
{
    if (![self.operation isExecuting])
    {
        self.operation = [ApplicationDelegate.webServiceEngine method_getSubCategories:categoryID
                                                                     completionHandler:^(id response)
                          {
                              NSDictionary *result = response;
                              NSLog(@"result:%@",result);
                              if (response != nil)
                              {
                                  if ([result valueForKey:@"CatalogGroupView"])
                                  {
                                      VC_SubCategory *objSubCategory = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_SubCategory"];
                                      objSubCategory.array_categories = [[NSMutableArray alloc] init];
                                      [(NSArray *)[result valueForKey:@"CatalogGroupView"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                          //name
                                          [objSubCategory.array_categories addObject:[NSDictionary dictionaryWithObjectsAndKeys:[obj valueForKey:@"name"],@"name",[obj valueForKey:@"uniqueID"],@"uniqueID", nil]];
                                      }];
                                      NSLog(@"Array: %@", objSubCategory.array_categories);
                                      [self performSegueWithIdentifier:@"segue_SubCategory" sender:objSubCategory.array_categories];
                                  }
                                  else
                                  {
                                      [self method_getproductListByCategory:catID];
                                  }
                                  
                              }
                              else
                              {
                                  [[[UIAlertView alloc] initWithTitle:nil message:@"There is something wrong, from the server side,please try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                              }
                          }
                                                                          errorHandler:^(NSError *error)
                          {
                              [self method_getproductListByCategory:catID];
                              DLog(@"there is something is wrong, please try with valid id and pass");
                              DLog(@"1:%@\n2:%@\n3:%@\n4:%@",
                                   [error localizedDescription],
                                   [error localizedFailureReason],
                                   [error localizedRecoveryOptions],
                                   [error localizedRecoverySuggestion]);
                          }
                          ];
    }
    
    [self.operation onDownloadProgressChanged:^(double progress)
     {
         DLog(@"download progress = %.2f", progress*100.0);
     }];
}
#pragma mark - method_getproductListByCategory
-(void)method_getproductListByCategory:(NSString *)categoryID
{
    if (![self.operation isExecuting])
    {
        self.operation = [ApplicationDelegate.webServiceEngine method_getProductsByCatgeory:categoryID
                                                                     completionHandler:^(id response)
                          {
                              NSDictionary *result = response;
                              NSLog(@"result:%@",result);
                              if (response != nil)
                              {
                                  if ([result valueForKey:@"CatalogEntryView"])
                                  {
                                      VC_ProductList *objProductList = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_ProductList"];
                                      objProductList.array_products = [[NSMutableArray alloc] init];
                                      [(NSArray *)[result valueForKey:@"CatalogEntryView"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                          [objProductList.array_products addObject:[NSDictionary dictionaryWithObjectsAndKeys:[obj valueForKey:@"name"],@"name",[obj valueForKey:@"uniqueID"],@"uniqueID",[obj valueForKey:@"thumbnail"],@"thumbnail",[obj valueForKey:@"fullImage"],@"fullImage",[[[obj valueForKey:@"Price"] objectAtIndex:0] valueForKey:@"priceValue"],@"Price", nil]];
                                      }];
                                      NSLog(@"Array: %@", objProductList.array_products);
                                      [self performSegueWithIdentifier:@"segue_ProductList" sender:objProductList.array_products];
                                  }
                                  else
                                  {
                                      [[[UIAlertView alloc] initWithTitle:nil message:@"Products not found" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                                  }
                                  
                              }
                              else
                              {
                                  [[[UIAlertView alloc] initWithTitle:nil message:@"There is something wrong, from the server side,please try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                              }
                          }
                                                                          errorHandler:^(NSError *error)
                          {
                              DLog(@"there is something is wrong, please try with valid id and pass");
                              DLog(@"1:%@\n2:%@\n3:%@\n4:%@",
                                   [error localizedDescription],
                                   [error localizedFailureReason],
                                   [error localizedRecoveryOptions],
                                   [error localizedRecoverySuggestion]);
                          }
                          ];
    }
    
    [self.operation onDownloadProgressChanged:^(double progress)
     {
         DLog(@"download progress = %.2f", progress*100.0);
     }];
}




#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_ProductList"])
    {
        UINavigationController *nav = segue.destinationViewController;
        VC_ProductList *obj =  (VC_ProductList *)[nav topViewController];
        DLog(@"product name:%@",productName);
        obj.productName = productName;
        obj.array_products = [NSMutableArray arrayWithArray:(NSMutableArray *)sender];
    }
    else if ([segue.identifier isEqualToString:@"segue_SubCategory"])
    {
        UINavigationController *nav = segue.destinationViewController;
        VC_SubCategory *obj = (VC_SubCategory *)[nav topViewController];
        obj.array_categories = [NSMutableArray arrayWithArray:(NSMutableArray *)sender];
    }
}

#pragma mark - method_addViewsToNavController
-(void)method_addViewsToNavController
{
    [self.view_categoryBackView setFrame:CGRectMake(-self.view_categoryBackView.frame.size.width,
                                                    self.view_categoryBackView.frame.origin.y,
                                                    self.view_categoryBackView.frame.size.width,
                                                    self.view_categoryBackView.frame.size.height)];
    
    
    
    
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"View_topMenuHome"
                                                         owner:self
                                                       options:nil];
    UIView *view_obj = nibContents[0];
    
    
//    CGRect rect_new = CGRectMake(self.view.frame.size.width/2 - view_obj.frame.size.width/2,
//                                 k_topBar_y,
//                                 view_obj.frame.size.width,
//                                 view_obj.frame.size.height);
    CGRect rect_new;
    rect_new = CGRectMake(8,k_topBar_y,
                              self.view.frame.size.width-16,
                              view_obj.frame.size.height);

    
    view_obj.frame = rect_new;
    
    
    
    [self.view_searchBarBg setFrame:CGRectMake(view_obj.frame.origin.x,
                                               view_obj.frame.origin.y+view_obj.frame.size.height,
                                               view_obj.frame.size.width,
                                               self.view_searchBarBg.frame.size.height)];
    
    
    
    
    

    [self.navigationController.view addSubview:view_obj];


    nibContents = nil;



    nibContents = [[NSBundle mainBundle] loadNibNamed:@"View_menu"
                                                owner:self
                                              options:nil];
    view_obj = nibContents[0];


    [view_obj setFrame:CGRectMake( self.view.frame.size.width,              //notice this is OFF screen!
                                  0,
                                  self.view.frame.size.width,
                                  self.view.frame.size.height)];
    
    //[view_obj setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:1]];
    //[view_obj setBackgroundColor:[UIColor clearColor]];
    
    [self.navigationController.view addSubview:view_obj];
    
    
    //effect
//    UIView *backgroundView = [[UIView alloc] initWithFrame:view_obj.bounds];
//    
//    
//    UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleLight]];
//    
//    [visualEffectView setFrame:backgroundView.bounds];
//    [backgroundView addSubview:visualEffectView];
//    
//    [view_obj insertSubview:backgroundView atIndex:0];
    
    
    
//    UIImageView *imageView_blackAplha = [[UIImageView alloc] ini];
}



#pragma mark - top menu Actions

- (IBAction)action_ShoppingCart:(id)sender
{
    if (ApplicationDelegate.single.array_cartObjects)
    {
        if ([ApplicationDelegate.single.array_cartObjects count])
        {
            [self.navigationController presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"Nav_Cart"]
                                                    animated:YES
                                                  completion:nil];
        }
    }

    else
    {
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:@"Cart is empty"
                                   delegate:nil
                          cancelButtonTitle:@"ok"
                          otherButtonTitles:nil, nil]
         show];
    }
}

- (IBAction)action_menu:(id)sender
{
    UIView *view_temp = [self.navigationController.view viewWithTag:2];
    
    [UIView animateWithDuration:k_menuAnimationTime
                     animations:^{
                         
                         [view_temp setFrame:CGRectMake( 0,
                                                       0.0f,
                                                       self.view.frame.size.width,
                                                       self.view.frame.size.height)];
                         
                     }
                     completion:^(BOOL finished){
                     }
     ];
}


- (IBAction)action_back:(id)sender
{
    DLog(@"%@", [self.navigationController.visibleViewController class]);
    
    id viewController = self.navigationController.visibleViewController;
    if (![viewController isKindOfClass:[VC_Home class]])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction)action_home:(id)sender
{
    
}





#pragma mark - full screen menu
- (IBAction)action_hideMenu:(id)sender
{
    UIButton *button_temp = (UIButton *)sender;
    
    
    [UIView animateWithDuration:k_menuAnimationTime
                     animations:^{
                         
                         [button_temp.superview setFrame:CGRectMake(self.view.frame.size.width,                               //notice this is ON screen!
                                                            0.0f,
                                                            self.view.frame.size.width,
                                                            self.view.frame.size.height)];
                         
                     }
                     completion:^(BOOL finished){
                     }
     ];
}



- (IBAction)action_logOut:(id)sender
{
    ApplicationDelegate.single.array_cartObjects = nil;
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)action_storeLocator:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    [self action_hideMenu:btn.superview];
    
    [self performSegueWithIdentifier:@"segue_locateStore" sender:nil];
}

- (IBAction)action_scan:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    [self action_hideMenu:btn.superview];
    
    [self performSegueWithIdentifier:@"segue_scan" sender:nil];
}


- (IBAction)action_barCode:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    [self action_hideMenu:btn.superview];
    
    [self performSegueWithIdentifier:@"segue_QR" sender:nil];
}


- (IBAction)action_viewLoyalPoints:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    [self action_hideMenu:btn.superview];
    
    [self performSegueWithIdentifier:@"segue_loyality" sender:nil];
}


- (IBAction)action_topCategories:(id)sender
{
    // Only show capture life categories
    UIButton *btn = (UIButton *)sender;
    
    [self action_hideMenu:btn.superview];
    [self method_getSubCategories:@"12601"];
    
    
    // Show all categories
//    [self action_hideMenu:sender];
//    
/////////////////////////////////show categories///////////////////////////////
//    
//    [UIView animateWithDuration:0.4
//                     animations:^{
//                         
//                         [self.view_categoryBackView setFrame:CGRectMake( 8,
//                                                                         self.view_categoryBackView.frame.origin.y,
//                                                                         self.view_categoryBackView.frame.size.width,
//                                                                         self.view_categoryBackView.frame.size.height)];
//                         
//                     }
//                     completion:^(BOOL finished){
//                     }
//     ];
}

- (IBAction)action_signInRegister:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    [self action_hideMenu:btn.superview];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleloginPopupClosedNotification:) name:@"loginPopupClosed" object:nil];
//    
//    VC_loginIn *objVC_loginIn = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_loginIn"];
//    [self presentPopupViewController:objVC_loginIn animationType:MJPopupViewAnimationSlideBottomTop];
    
    [self performSegueWithIdentifier:@"segue_Login" sender:nil];
}

- (IBAction)action_hideCategories:(id)sender
{
    ///////////////////////////////Hide categories///////////////////////////////
    
    [UIView animateWithDuration:k_menuAnimationTime
                     animations:^{
                         
                         [self.view_categoryBackView setFrame:CGRectMake(-self.view.frame.size.width,
                                                                         self.view_categoryBackView.frame.origin.y,
                                                                         self.view_categoryBackView.frame.size.width,
                                                                         self.view_categoryBackView.frame.size.height)];
                         
                     }
                     completion:^(BOOL finished){
                     }
     ];
}
#pragma mark - action_QRcodeScan
- (IBAction)action_QRcodeScan:(id)sender
{
     [self performSegueWithIdentifier:@"segue_QR" sender:nil];
}

#pragma mark - speech to text delegate
- (void)witDidGraspIntent:(NSArray *)outcomes messageId:(NSString *)messageId customData:(id) customData error:(NSError*)e
{
    bool_isSpeechDelegateCall = YES;
    [SVProgressHUD dismiss];
    if (e)
    {
        NSLog(@"[Wit] error: %@", [e localizedDescription]);
        [[[UIAlertView alloc] initWithTitle:@"Sorry" message:[e localizedDescription] delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil] show];
        return;
    }
    
    NSDictionary *firstOutcome = [outcomes objectAtIndex:0];
    NSLog(@"%@",firstOutcome);
    NSString *intent = [firstOutcome objectForKey:@"_text"];
    self.textfield_search.text = intent;
    DLog(@"%@", [NSString stringWithFormat:@"intent = %@", intent]);
    [self method_GetProductsBySearch:intent];
}

- (void)witDidStopRecording
{
    [self dismissAlertView:alert];
    if (!bool_isSpeechDelegateCall)
    {
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    }
    [self.view endEditing:YES];
}

- (void)witDidGetAudio:(NSData *)chunk
{
    DLog(@"gotSomeAudio:%@",[NSString stringWithUTF8String:[chunk bytes]]);
}
- (void)witActivityDetectorStarted
{
    bool_isSpeechDelegateCall = NO;
    alert = [[UIAlertView alloc] initWithTitle:@"Speak Now" message:@"" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
    alert.tag = 420;
    [alert show];
}

-(void)dismissAlertView:(UIAlertView *)alertView
{
    if (alertView.tag == 420)
    {
        [alertView dismissWithClickedButtonIndex:0 animated:YES];
    }
}

#pragma mark - method_GetProductsBySearch
-(void)method_GetProductsBySearch:(NSString *)searchTerm
{
    if ([self.operation isExecuting])
    {
        [self.operation cancel];
    }
    
    if (![self.operation isExecuting])
    {
        self.operation = [ApplicationDelegate.webServiceEngine method_getProductsBySearch:searchTerm
                                                                        completionHandler:^(id response)
                          {
                              NSDictionary *result = response;
                              NSLog(@"result:%@",result);
                              if (response != nil)
                              {
                                  if ([result valueForKey:@"CatalogEntryView"])
                                  {
                                      VC_ProductList *objProductList = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_ProductList"];
                                      objProductList.array_products = [[NSMutableArray alloc] init];
                                      [(NSArray *)[result valueForKey:@"CatalogEntryView"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                          [objProductList.array_products addObject:[NSDictionary dictionaryWithObjectsAndKeys:[obj valueForKey:@"name"],@"name",[obj valueForKey:@"uniqueID"],@"uniqueID",[obj valueForKey:@"thumbnail"],@"thumbnail",[obj valueForKey:@"fullImage"],@"fullImage",[[[obj valueForKey:@"Price"] objectAtIndex:0] valueForKey:@"priceValue"],@"Price", nil]];
                                      }];
                                      NSLog(@"Array: %@", objProductList.array_products);
                                      productName = searchTerm;
                                      [self performSegueWithIdentifier:@"segue_ProductList" sender:objProductList.array_products];
                                  }
                                  else
                                  {
                                      [[[UIAlertView alloc] initWithTitle:@"Failure" message:@"Products not found." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                                  }
                                  
                              }
                              else
                              {
                                  [[[UIAlertView alloc] initWithTitle:nil message:@"There is something wrong, from the server side,please try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                              }
                          }
                                                                             errorHandler:^(NSError *error)
                          {
                              DLog(@"there is something is wrong, please try with valid id and pass");
                              
                              if ([[error localizedDescription] isEqualToString:@"The operation couldn’t be completed. (NSURLErrorDomain error 404.)"])
                              {
                                  [[[UIAlertView alloc] initWithTitle:nil message:@"Products not found"
                                                             delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]
                                   show];
                              }
                              DLog(@"1:%@\n2:%@\n3:%@\n4:%@",
                                   [error localizedDescription],
                                   [error localizedFailureReason],
                                   [error localizedRecoveryOptions],
                                   [error localizedRecoverySuggestion]);
                          }
                          ];
    }
    
    [self.operation onDownloadProgressChanged:^(double progress)
     {
         DLog(@"download progress = %.2f", progress*100.0);
     }];
}

#pragma mark - textField delegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *text = [textField.text stringByReplacingCharactersInRange:range withString: string];
    if ([text length])
    {
        [self method_solarSearch:text];
    }
    else
    {
        [popover dismissPopoverAnimated:YES];
    }
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self method_GetProductsBySearch:textField.text];
    return NO;
}

#pragma mark - method solar search
-(void)method_solarSearch:(NSString *)textfieldText
{
    if ([self.operation isExecuting])
    {
        [self.operation cancel];
    }
    
    if (![self.operation isExecuting])
    {
        self.operation = [ApplicationDelegate.webServiceEngine method_solarSearch:textfieldText completionHandler:^(id response)
        {
            DLog(@"result:%@",response);
            NSDictionary *result = response;
            NSLog(@"result:%@",result);
            if (response != nil)
            {
                if ([result valueForKey:@"suggestionView"])
                {
                    if ([[result valueForKey:@"suggestionView"] count])
                    {
                        array_predictions = [[NSMutableArray alloc] init];
                        [[[(NSArray *)[result valueForKey:@"suggestionView"] objectAtIndex:0] valueForKey:@"entry"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
                         {
                             [array_predictions addObject:[obj valueForKey:@"term"]];
                         }];
                        if ([array_predictions count])
                        {
                            [controller reloadList:array_predictions];
                            
                            UIScrollView *v = (UIScrollView *) self.view;
                            CGRect rc = [self.textfield_search bounds];
                            rc = [self.textfield_search convertRect:rc toView:v];
                            
                            if (rc.origin.y <= 145)     // it means textfield is near to topBar
                            {
                                popover.arrowDirection = FPPopoverArrowDirectionUp;
                            }
                            else
                            {
                                popover.arrowDirection = FPPopoverArrowDirectionDown;
                            }
                            
                            [popover presentPopoverFromView:self.textfield_search];
                            array_predictions = nil;
                        }
                        else
                        {
                            [popover dismissPopoverAnimated:YES];
                        }
                    }
                }
                else
                {
                    DLog(@"data not found");
                }
            }
        } errorHandler:^(NSError *error) {
            
            DLog(@"result:%@",error);
        }];
    }
    
    [self.operation onDownloadProgressChanged:^(double progress)
     {
         DLog(@"download progress = %.2f", progress*100.0);
     }];
}



#pragma mark - Popup delegate
#pragma mark - SelectPopValue
- (void)selectedValueFromDropDown:(NSString *)selectedValue controller:(DemoTableController *)controller
{
    DLog(@"%@", selectedValue);
    [self method_GetProductsBySearch:selectedValue];
    [popover dismissPopoverAnimated:YES];
}
- (void)presentedNewPopoverController:(FPPopoverController *)newPopoverController
          shouldDismissVisiblePopover:(FPPopoverController*)visiblePopoverController
{
    [visiblePopoverController dismissPopoverAnimated:YES];
}
- (void)popoverControllerDidDismissPopover:(FPPopoverController *)popoverController
{
    //    popover = nil;
}







#pragma mark - home scroller delegates
-(void)method_catalog
{
    [self performSegueWithIdentifier:@"segue_scan" sender:nil];
}
-(void)method_barcode
{
    [self performSegueWithIdentifier:@"segue_QR" sender:nil];
}
-(void)method_locateStore
{
    [self performSegueWithIdentifier:@"segue_locateStore" sender:nil];
}
-(void)method_loyalityPoints
{
    [self performSegueWithIdentifier:@"segue_loyality" sender:nil];
}


#pragma mark - method_guestLogin
-(void)method_guestLogin
{
//    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    
    DLog(@"guest");
    if (![self.operation isExecuting])
    {
        self.operation = [ApplicationDelegate.webServiceEngine method_guestLogin:nil
                                                               completionHandler:^(id response)
                          {
//                              [SVProgressHUD dismiss];
                              
                              NSDictionary *result = response;
                              NSLog(@"result:%@",result);
                              if (response != nil)
                              {
                                  if ([result valueForKey:@"WCToken"])
                                  {
                                      ApplicationDelegate.single.isLoggedIn = NO;
                                      ApplicationDelegate.single.string_WCToken = [result valueForKey:@"WCToken"];
                                      ApplicationDelegate.single.string_WCTrustedToken = [result valueForKey:@"WCTrustedToken"];
                                      
//                                      [self performSegueWithIdentifier:@"segue_home" sender:nil];
                                      DLog(@"WCToken = %@", ApplicationDelegate.single.string_WCToken);
                                      DLog(@"WCTrustedToken = %@", ApplicationDelegate.single.string_WCTrustedToken);
                                      
                                  }
                                  else
                                  {
                                      if ([result valueForKey:@"errors"])
                                      {
                                          [[[UIAlertView alloc] initWithTitle:nil message:[[[result valueForKey:@"errors"] objectAtIndex:0] valueForKey:@"errorMessage"] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                                      }
                                      else
                                      {
                                          [[[UIAlertView alloc] initWithTitle:nil
                                                                      message:@"Authentication failed"
                                                                     delegate:nil
                                                            cancelButtonTitle:@"Ok"
                                                            otherButtonTitles:nil, nil]
                                           show];
                                      }
                                  }
                              }
                              else
                              {
                                  [[[UIAlertView alloc] initWithTitle:nil
                                                              message:@"There is something wrong, from the server side,please try again"
                                                             delegate:nil
                                                    cancelButtonTitle:@"Ok"
                                                    otherButtonTitles:nil, nil]
                                   show];
                              }
                          }
                                                                    errorHandler:^(NSError *error)
                          {
//                              [SVProgressHUD dismiss];
                              
                              DLog(@"there is something is wrong, please try with valid id and pass");
                              //[self performSegueWithIdentifier:@"segue_ProductList" sender:nil];
                              [[[UIAlertView alloc] initWithTitle:nil
                                                          message:[error localizedDescription]
                                                         delegate:nil
                                                cancelButtonTitle:@"Ok"
                                                otherButtonTitles:nil, nil]
                               show];
                              
                              DLog(@"1:%@\n2:%@\n3:%@\n4:%@",
                                   [error localizedDescription],
                                   [error localizedFailureReason],
                                   [error localizedRecoveryOptions],
                                   [error localizedRecoverySuggestion]);
                          }
                          ];
    }
    
    [self.operation onDownloadProgressChanged:^(double progress)
     {
         DLog(@"download progress = %.2f", progress*100.0);
     }];
}
#pragma mark - handleloginPopupClosedNotification
-(void)handleloginPopupClosedNotification:(NSNotification*)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"loginPopupClosed" object:nil];
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    if (notification.userInfo)
    {
        [self performSegueWithIdentifier:@"segue_Register" sender:nil];
    }
}
#pragma mark - EAIntroView delegate
- (void)intro:(EAIntroView *)introView pageAppeared:(EAIntroPage *)page withIndex:(NSUInteger)pageIndex
{
    DLog(@"%lu",(unsigned long)pageIndex);
    
    if (pageIndex == 1)
    {
        button_scroller.hidden = NO;
        [button_scroller setBackgroundImage:[UIImage imageNamed:@"see-catalog-btn"] forState:UIControlStateNormal];
        if (IS_IPHONE_4_OR_LESS)
        {
            [button_scroller setFrame:CGRectMake(10, 220, 155, 50)];
        }
        else if (IS_IPHONE_5)
        {
            [button_scroller setFrame:CGRectMake(10, 220, 110, 35)];
        }
        else if (IS_IPHONE_6P)
        {
            [button_scroller setFrame:CGRectMake(10, 265, 155, 50)];
        }
        else
        {
            [button_scroller setFrame:CGRectMake(10, 235, 155, 50)];
        }
        [button_scroller addTarget:self action:@selector(method_catalog) forControlEvents:UIControlEventTouchUpInside];
    }
    else if (pageIndex == 2)
    {
        button_scroller.hidden = NO;
        [button_scroller setBackgroundImage:[UIImage imageNamed:@"scan-qrcode-btn"] forState:UIControlStateNormal];
        if (IS_IPHONE_4_OR_LESS)
        {
            [button_scroller setFrame:CGRectMake(10, 220, 155, 50)];
        }
        else if (IS_IPHONE_5)
        {
            [button_scroller setFrame:CGRectMake(10, 240, 110, 35)];
        }
        else if (IS_IPHONE_6P)
        {
            [button_scroller setFrame:CGRectMake(10, 285, 155, 50)];
        }
        else
        {
            [button_scroller setFrame:CGRectMake(10, 275, 155, 50)];
        }
        [button_scroller addTarget:self action:@selector(method_barcode) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        button_scroller.hidden = YES;
    }
}
- (void)intro:(EAIntroView *)introView pageStartScrolling:(EAIntroPage *)page withIndex:(NSUInteger)pageIndex
{
//    if (pageIndex == 2)
//    {
//        if (button_scroller.hidden)
//            button_scroller.hidden = NO;
//    }
    button_scroller.hidden = YES;
    
}
- (void)introDidFinish:(EAIntroView *)introView
{
    DLog(@"intro finished");
}

@end