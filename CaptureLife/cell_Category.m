#import "cell_Category.h"

@implementation cell_Category

- (void)awakeFromNib {
    // Initialization code
    
//    [self.contentView setBackgroundColor:[UIColor greenColor]];
    
    
    [self.view_back setBackgroundColor:[k_RGB(k_R, k_G, k_B) colorWithAlphaComponent:k_colorWithAlphaCell]];
    [self.view_back.layer setCornerRadius:k_cornerRadiusCell];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)method_setCategoryName:(NSString *)categoryName
{
    self.label_categoryName.text = categoryName;
}

@end