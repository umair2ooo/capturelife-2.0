//
//  VC_loginFields.m
//  CaptureLife
//
//  Created by Fahim Bilwani on 29/06/2015.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import "VC_loginFields.h"

@interface VC_loginFields ()<NSURLResponseDelegate>

@property (weak, nonatomic) IBOutlet UITextField *text_UserName;
@property (weak, nonatomic) IBOutlet UITextField *text_Password;

- (IBAction)action_back:(id)sender;
- (IBAction)action_login:(id)sender;
- (IBAction)action_cancel:(id)sender;

@property(nonatomic, strong) webService_Post_Put_Delete *nsurl_obj;
@end

@implementation VC_loginFields

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - action_back
- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - action_login
- (IBAction)action_login:(id)sender
{
    if ([self.text_UserName.text length] && [self.text_Password.text length])
    {
        NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:self.text_UserName.text,@"logonId",self.text_Password.text,@"logonPassword", nil];
            DLog(@"%@",parameters);
        [self method_Login:parameters];

                //[self performSegueWithIdentifier:@"segue_home" sender:nil];
    }
    else
    {
        if ([self.text_UserName.text length] == 0)
        {
            [[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Please enter user name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Please enter password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
    }
}

#pragma mark - method_Login
-(void)method_Login:(NSDictionary *)dicloginCredential
{
    NSString *strUrl = [NSString stringWithFormat:@"%@/wcs/resources/store/10001/loginidentity",k_url];
    
    DLog(@"strUrl: %@",strUrl);
    strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    if (!self.nsurl_obj)
    {
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
        
        self.nsurl_obj = [[webService_Post_Put_Delete alloc] initWithURL:strUrl data:dicloginCredential seriveType:@"POST" seriveName:@"login"];
        self.nsurl_obj.delegate = self;
    }
}
#pragma mark -
#pragma mark -  POST by NSURL Session
#pragma mark - NSURLResponseDelegate
-(void)delegate_outPut:(NSData *)data urlRes:(NSURLResponse *)urlRes error_:(NSError *)error_ seriveName:(NSString *)serivename
{
    [SVProgressHUD dismiss];
    
    self.nsurl_obj.delegate = nil;
    self.nsurl_obj = nil;
    
    if (error_ == nil)
    {
        // Convert JSON Object into Dictionary
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:data options:
                              NSJSONReadingMutableContainers error:&error_];
        DLog(@"%@",JSON);
        if ([JSON valueForKey:@"WCToken"])
        {
            ApplicationDelegate.single.isLoggedIn = YES;
            ApplicationDelegate.single.string_WCToken = [JSON valueForKey:@"WCToken"];
            ApplicationDelegate.single.string_WCTrustedToken = [JSON valueForKey:@"WCTrustedToken"];
            DLog(@"WCToken = %@", ApplicationDelegate.single.string_WCToken);
            DLog(@"WCTrustedToken = %@", ApplicationDelegate.single.string_WCTrustedToken);
            
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        else
        {
            if ([JSON valueForKey:@"errors"])
            {
                [[[UIAlertView alloc] initWithTitle:nil
                                            message:[[[JSON valueForKey:@"errors"] objectAtIndex:0] valueForKey:@"errorMessage"]
                                           delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles:nil, nil]
                 show];
            }
            else
            {
                [[[UIAlertView alloc] initWithTitle:nil
                                            message:@"Authentication failed"
                                           delegate:nil
                                  cancelButtonTitle:@"Ok"
                                  otherButtonTitles:nil, nil]
                 show];
            }
        }
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:@"There is something wrong, from the server side,please try again"
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
    }
}

#pragma mark - action_cancel
- (IBAction)action_cancel:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
@end
