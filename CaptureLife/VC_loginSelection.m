//
//  VC_loginSelection.m
//  CaptureLife
//
//  Created by Fahim Bilwani on 29/06/2015.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import "VC_loginSelection.h"

@interface VC_loginSelection ()


@property (weak, nonatomic) IBOutlet UILabel *label_loginText;

- (IBAction)action_back:(id)sender;
- (IBAction)action_ViaEmail:(id)sender;
- (IBAction)action_cancel:(id)sender;
@end

@implementation VC_loginSelection

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.bool_Login)
        self.label_loginText.text = @"Quicly login with";
    else
        self.label_loginText.text = @"Quicly Signup with";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - action_back
- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - action_ViaEmail
- (IBAction)action_ViaEmail:(id)sender
{
    if (self.bool_Login)
     [self performSegueWithIdentifier:@"segue_loginFields" sender:nil];
    else
     [self performSegueWithIdentifier:@"segue_Register" sender:nil];
}

- (IBAction)action_cancel:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
@end
