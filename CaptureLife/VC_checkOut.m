#import "VC_checkOut.h"
#import "CDatePickerViewEx.h"
#import "CardIO.h"
#import <IQKeyboardManager/IQKeyboardManager.h>

@interface VC_checkOut ()<UIPopoverControllerDelegate,FPPopoverControllerDelegate,DemoTableControllerDelegate,SelectedDate,UIPickerViewDelegate,UIPickerViewDataSource,CardIOPaymentViewControllerDelegate>
{
    NSArray *array_cardType;
    FPPopoverController *popover;
    DemoTableController *controller;
    NSMutableArray *array_predictions;
}
@property (weak, nonatomic) IBOutlet UITextField *textField_name;
@property (weak, nonatomic) IBOutlet UITextField *textField_email;
@property (weak, nonatomic) IBOutlet UITextField *textField_contact;
@property (weak, nonatomic) IBOutlet UITextField *textField_address;
@property (weak, nonatomic) IBOutlet UIScrollView *scroller;
@property (weak, nonatomic) IBOutlet UITextField *textField_lastName;

@property (weak, nonatomic) IBOutlet UITextField *textField_creditCardType;
@property (weak, nonatomic) IBOutlet UITextField *textField_creditCardNo;
@property (weak, nonatomic) IBOutlet UITextField *textField_experitationDate;
@property (weak, nonatomic) IBOutlet UITextField *textField_securityCode;
@property (weak, nonatomic) IBOutlet UILabel *label_subtotal;
@property (weak, nonatomic) IBOutlet UILabel *label_discount;
@property (weak, nonatomic) IBOutlet UILabel *label_total;
@property(nonatomic, strong)UIPickerView *pickerView;

@property (strong, nonatomic) MKNetworkOperation *operation;

- (IBAction)action_submit:(id)sender;

@end

@implementation VC_checkOut

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    DLog(@"%@",self.dic_priceDetails);
    
    [self method_setPricedetails];
    
    [self method_addPickerInKeyBoard];
    
    [self.pickerView selectRow:0 inComponent:0 animated:NO];
    
    [self.scroller setContentSize:CGSizeMake(0, self.textField_address.frame.origin.y+300)];
    
    
    if (popover == nil)
    {
        controller = [[DemoTableController alloc] init];
        controller.delegated = self;
        controller.title = nil;
        popover = [[FPPopoverController alloc] initWithViewController:controller];
        popover.delegate = self;
        popover.border = YES;
        popover.arrowDirection = FPPopoverArrowDirectionDown;
        popover.contentSize = CGSizeMake(270,270);
    }
}


#pragma mark - cycle
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [CardIOUtilities preload];
    [self method_setAddToCartCounterLabel];
}
-(void)method_setPricedetails
{
    if ([[self.dic_priceDetails valueForKey:@"discount"] isEqualToString:@"yes"])
    {
        self.label_subtotal.text = [NSString stringWithFormat:@"$ %.2f",[[self.dic_priceDetails valueForKey:@"total"] floatValue]+10.00];
        self.label_discount.text = @"$ 10.00";
    }
    else
    {
       self.label_subtotal.text = [NSString stringWithFormat:@"$ %@",[self.dic_priceDetails valueForKey:@"total"]];
        self.label_discount.text = @"$ 0.00";
    }
     self.label_total.text = [NSString stringWithFormat:@"$ %@",[self.dic_priceDetails valueForKey:@"total"]];
}

#pragma mark - method_setAddToCartCounterLabel
-(void)method_setAddToCartCounterLabel
{
    UIView *view_counter;
    if (ApplicationDelegate.single.array_cartObjects)
    {
        if ([ApplicationDelegate.single.array_cartObjects count])
        {
            view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
            UILabel *label_counter = (UILabel *)[self.navigationController.view viewWithTag:888];
            [view_counter setHidden:NO];
            label_counter.text = [NSString stringWithFormat:@"%lu",(unsigned long)[ApplicationDelegate.single.array_cartObjects count]];
        }
        else
        {
            view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
            [view_counter setHidden:YES];
        }
        
    }
    else
    {
        view_counter = (UIView *)[self.navigationController.view viewWithTag:891];
        [view_counter setHidden:YES];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - action_goBack
-(void)action_goBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - textField delegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.textField_creditCardType ||
        textField == self.textField_creditCardNo ||
        textField == self.textField_securityCode ||
        textField == self.textField_experitationDate)
    {
        if ([self.textField_creditCardType.text length])
        {
            if (textField == self.textField_creditCardNo)
            {
                if ([self.textField_creditCardType.text isEqualToString:@"MasterCard"]||[self.textField_creditCardType.text isEqualToString:@"Visa"])
                {
                    __block NSString *text = [textField text];
                    
                    NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789\b"];
                    string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
                    if ([string rangeOfCharacterFromSet:[characterSet invertedSet]].location != NSNotFound) {
                        return NO;
                    }
                    
                    text = [text stringByReplacingCharactersInRange:range withString:string];
                    text = [text stringByReplacingOccurrencesOfString:@" " withString:@""];
                    
                    NSString *newString = @"";
                    while (text.length > 0) {
                        NSString *subString = [text substringToIndex:MIN(text.length, 4)];
                        newString = [newString stringByAppendingString:subString];
                        if (subString.length == 4) {
                            newString = [newString stringByAppendingString:@" "];
                        }
                        text = [text substringFromIndex:MIN(text.length, 4)];
                    }
                    
                    newString = [newString stringByTrimmingCharactersInSet:[characterSet invertedSet]];
                    
                    if (newString.length >= 20) {
                        return NO;
                    }
                    
                    [textField setText:newString];
                    
                    return NO;
                }
                
                if([self.textField_creditCardType.text isEqualToString:@"American Express"])
                {
                    if (range.location<=16) {
                        
                        if (range.location==4)
                        {
                            NSMutableString *Text = nil;
                            Text = [NSMutableString stringWithFormat:@"%@",textField.text];
                            [Text insertString:@" " atIndex:4];
                            [textField setText:Text];
                        }
                        
                        if (range.location==11)
                        {
                            NSMutableString *Text = nil;
                            Text = [NSMutableString stringWithFormat:@"%@",textField.text];
                            [Text insertString:@" " atIndex:11];
                            [textField setText:Text];
                        }
                        return YES;
                    }
                    return NO;
                }
                return NO;
            }
            
            if (self.textField_securityCode == textField)
            {
                if ([self.textField_creditCardType.text isEqualToString:@"MasterCard"]||[self.textField_creditCardType.text isEqualToString:@"Visa"])
                {
                    if ([textField.text length] > 2 && ![string isEqualToString:@""])
                    {
                        return NO;
                    }
                }
                else
                {
                    if ([textField.text length] > 3 && ![string isEqualToString:@""])
                    {
                        return NO;
                    }
                }
                return YES;
                
            }
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:nil message:@"Select card type first"
                                       delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil, nil]
             show];
            return NO;
        }
    }
    else if (textField == self.textField_address)
    {
        NSString *text = [textField.text stringByReplacingCharactersInRange:range
                                                                 withString: string];
        if ([text isEqualToString:@""] && popover != nil)
        {
            [popover dismissPopoverAnimated:YES];
        }
        else
        {
            
            //complete address service
            [self method_autocompleteAddress:text];
        }
    }
    return YES;
}
#pragma mark - method_getSubCategories
-(void)method_autocompleteAddress:(NSString *)input
{
    
    if([self.operation isExecuting])
    {
        [self.operation cancel];
    }
    
    if (![self.operation isExecuting])
    {
        self.operation = [ApplicationDelegate.webServiceEngine method_autocompleteAddress:input
                                                                     completionHandler:^(id response)
                          {
                              NSDictionary *result = response;
                              NSLog(@"result:%@",result);
                              if (response != nil)
                              {
                                  if ([result valueForKey:@"predictions"])
                                  {
                                       array_predictions = [[NSMutableArray alloc] init];
                                      [(NSArray *)[result valueForKey:@"predictions"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
                                       {
                                           [array_predictions addObject:[obj valueForKey:@"description"]];
                                       }];
                                      if ([array_predictions count])
                                      {
                                          [controller reloadList:array_predictions];
                                          
                                          UIScrollView *v = (UIScrollView *) self.view;
                                          CGRect rc = [self.textField_address bounds];
                                          rc = [self.textField_address convertRect:rc toView:v];
                                          
                                          if (rc.origin.y <= 145)     // it means textfield is near to topBar
                                          {
                                              popover.arrowDirection = FPPopoverArrowDirectionUp;
                                          }
                                          else
                                          {
                                              popover.arrowDirection = FPPopoverArrowDirectionDown;
                                          }

                                          NSLog(@"%@",[[[[UIApplication sharedApplication] keyWindow] subviews] lastObject]) ;
                                          
                                              [popover presentPopoverFromView:self.textField_address];
                                      }
                                      else
                                      {
                                          [popover dismissPopoverAnimated:YES];
                                      }

                                     // array_predictions = nil;
                                  }
                                  else
                                  {
                                      DLog(@"data not found");
                                  }
                                  
                              }
                              else
                              {
                                  [[[UIAlertView alloc] initWithTitle:nil message:@"There is something wrong, from the server side,please try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
                              }
                          }
                                                                          errorHandler:^(NSError *error)
                          {
                              DLog(@"there is something is wrong, please try with valid id and pass");
                              //[self performSegueWithIdentifier:@"segue_ProductList" sender:nil];
                              DLog(@"1:%@\n2:%@\n3:%@\n4:%@",
                                   [error localizedDescription],
                                   [error localizedFailureReason],
                                   [error localizedRecoveryOptions],
                                   [error localizedRecoverySuggestion]);
                          }
                          ];
    }
    
    [self.operation onDownloadProgressChanged:^(double progress)
     {
         DLog(@"download progress = %.2f", progress*100.0);
     }];
}
#pragma mark - action_submit
- (IBAction)action_submit:(id)sender
{
    if (![self.textField_creditCardType.text length])
    {
        [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                    message:@"Enter your Credit Card Type"
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
    }
    else if (![self.textField_creditCardNo.text length])
    {
        [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                    message:@"Enter your Credit Card Number"
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
    }

    else if (![self.textField_experitationDate.text length])
    {
        [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                    message:@"Enter your Credit Card Expiration date"
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
    }
    else if (![self.textField_securityCode.text length])
    {
        [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                    message:@"Enter your Credit Card Security Code"
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
    }

    else if (![self.textField_name.text length])
    {
        [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                    message:@"Please enter your First Name"
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
    }
    else if (![self.textField_lastName.text length])
    {
        [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                    message:@"Please enter your Last Name"
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
    }
    else if (![self.textField_email.text length] || [ApplicationDelegate.single method_NSStringIsValidEmail:self.textField_email.text] == false)
    {
        [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                    message:@"Invalid email address"
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
    }
//    else if ([self.textField_contact.text length] && [ApplicationDelegate.single method_phoneNumberValidation:self.textField_contact.text] == false)
//    {
//        [[[UIAlertView alloc] initWithTitle:@"Sorry"
//                                    message:@"Invalid Contact Number, \n format should be, \n (+ or 00) then 6-14 numbers"
//                                   delegate:nil
//                          cancelButtonTitle:@"Ok"
//                          otherButtonTitles:nil, nil]
//         show];
//    }
    else if (![self.textField_address.text length])
    {
        [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                    message:@"Enter your Address"
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
    }
    else
    {
        
        //[self method_submitForm];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                        message:@"Order submitted successfully"
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil, nil];
        alert.tag = 100;
        [alert show];
    }
}
#pragma mark - action_CreditCardScan
- (IBAction)action_CreditCardScan:(id)sender
{
    CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
    [self presentViewController:scanViewController animated:YES completion:nil];
}
#pragma mark - add picker in text keyBoard
-(void)method_addPickerInKeyBoard
{
    array_cardType = [NSArray arrayWithObjects:@"MasterCard",@"Visa", @"American Express", nil];
    
    
    self.pickerView = [[UIPickerView alloc] init];
    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
    // ... ...
    self.textField_creditCardType.inputView = self.pickerView;
    
    
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                        target:nil
                                                                        action:nil],
                           
                           [[UIBarButtonItem alloc]initWithTitle:@"Next"
                                                           style:UIBarButtonItemStyleDone
                                                          target:self
                                                          action:@selector(action_done)], nil];
    
    [numberToolbar sizeToFit];
    self.textField_creditCardType.inputAccessoryView = numberToolbar;
    
    numberToolbar = nil;
    
    CDatePickerViewEx *datePicker = [[CDatePickerViewEx alloc]init];
    datePicker.delegate_mine = self;
    [self.textField_experitationDate setInputView:datePicker];
}
#pragma mark - updateExpiryDateTextField
-(void)updateExpiryDateTextField:(id)sender
{
    UIDatePicker *picker = (UIDatePicker*)self.textField_experitationDate.inputView;
    NSDate * selected = [picker date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"MM/yy"];
    self.textField_experitationDate.text = [dateFormatter stringFromDate:selected];
}
#pragma mark - bar button
-(void)action_done
{
    self.textField_creditCardNo.text = @"";
    self.textField_securityCode.text = @"";
    
    
    [self.textField_creditCardType resignFirstResponder];
    
    [self.textField_creditCardNo becomeFirstResponder];
    
    self.textField_creditCardType.text = [array_cardType objectAtIndex:[self.pickerView selectedRowInComponent:0]];
    
    
    if ([self.textField_creditCardType.text isEqualToString:@"Visa"])
    {
        // typerang = 2;
        
        //[self.imageView_CardType setImage:[UIImage imageNamed:@"visa"]];
        
        [self.textField_creditCardNo setPlaceholder:@"0000 0000 0000 0000"];
        [self.textField_securityCode setPlaceholder:@"000"];
    }
    else if ([self.textField_creditCardType.text isEqualToString:@"MasterCard"])
    {
        //  typerang = 2;
        
        // [self.imageView_CardType setImage:[UIImage imageNamed:@"mastercard"]];
        
        [self.textField_creditCardNo setPlaceholder:@"0000 0000 0000 0000"];
        [self.textField_securityCode setPlaceholder:@"000"];
    }
    else if ([self.textField_creditCardType.text isEqualToString:@"American Express"])
    {
        //  typerang = 3;
        
        // [self.imageView_CardType setImage:[UIImage imageNamed:@"American_Express"]];
        
        [self.textField_creditCardNo setPlaceholder:@"0000 000000 00000"];
        [self.textField_securityCode setPlaceholder:@"0000"];
    }
}
#pragma mark - UIAlertview Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView tag] == 100)
    {
        if (buttonIndex == 0)
        {
//            [ApplicationDelegate.single.array_cartObjects removeAllObjects];
            ApplicationDelegate.single.array_cartObjects = nil;
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

#pragma mark - picker delegate and data source
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if ([pickerView isKindOfClass:[CDatePickerViewEx class]])
    {
        return 0;
    }
    return [array_cardType count]?[array_cardType count]:0;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [array_cardType objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
}
#pragma mark - SelectedDate delegate
-(void)method_selectedDate:(NSString *)string_date
{
    self.textField_experitationDate.text = string_date;
}

#pragma mark - PaymentViewControllerDelegate

- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)scanViewController
{
    NSLog(@"User canceled payment info");
    
    // Handle user cancellation here...
    [scanViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)scanViewController
{
    // The full card number is available as info.cardNumber, but don't log that!
    NSLog(@"Received card info. Number: %@, expiry: %02lu/%lu, cvv: %@.", info.redactedCardNumber, (unsigned long)info.expiryMonth, (unsigned long)info.expiryYear, info.cvv);
    NSLog(@"card type:%@",[CardIOCreditCardInfo displayStringForCardType:info.cardType usingLanguageOrLocale:@""]);
    // Use the card info...
    
    self.textField_creditCardNo.text = info.cardNumber;
    self.textField_creditCardType.text = [CardIOCreditCardInfo displayStringForCardType:info.cardType usingLanguageOrLocale:@""];
    self.textField_securityCode.text = info.cvv;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setMonth:info.expiryMonth];
    [components setYear:info.expiryYear];
    NSDate * selected = [calendar dateFromComponents:components];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"MMMM/yyyy"];
    self.textField_experitationDate.text = [dateFormatter stringFromDate:selected];
    components = nil;
    dateFormatter = nil;
    
    [scanViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Popup delegate
#pragma mark - SelectPopValue
- (void)selectedValueFromDropDown:(NSString *)selectedValue controller:(DemoTableController *)controller
{
    self.textField_address.text = selectedValue;
    [popover dismissPopoverAnimated:YES];
}
- (void)presentedNewPopoverController:(FPPopoverController *)newPopoverController
          shouldDismissVisiblePopover:(FPPopoverController*)visiblePopoverController
{
    [visiblePopoverController dismissPopoverAnimated:YES];
}
- (void)popoverControllerDidDismissPopover:(FPPopoverController *)popoverController
{
    //    popover = nil;
}



@end