#import "AppDelegate.h"

#import <Wit/Wit.h>

@interface AppDelegate ()

@end

@implementation AppDelegate

#pragma mark - speech to text
-(void)method_speechToText
{
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    //KJSNOQHVL5RV6EZLNLLPH7NFT7JFKXER
    [Wit sharedInstance].accessToken = @"2ZAGVWTYXFFG7K6NWTNU2CPJSBGVJX5H"; // replace xxx by your Wit.AI access token
    //enabling detectSpeechStop will automatically stop listening the microphone when the user stop talking
    //WITVadConfigDetectSpeechStop
    [Wit sharedInstance].detectSpeechStop = WITVadConfigFull;
}


#pragma mark - app delegates
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    [self method_speechToText];
    
//    self.webServiceEngine = [[WebServiceEngine alloc] initWithDefaultSettings];
//    [self.webServiceEngine useCache];
    
    self.webServiceEngine = [[WebServiceEngine alloc] initWithHostName:k_MKNetworkURL customHeaderFields:[NSDictionary dictionaryWithObjectsAndKeys:@"application/json",@"Accept",@"application/json",@"Content-Type", nil]];
    [self.webServiceEngine useCache];

    self.single = [Singleton retriveSingleton];
    
    
//    NSUserDefaults *loginDetails = [NSUserDefaults standardUserDefaults];
//    if([[loginDetails stringForKey:@"isZipcodeEntered"] isEqualToString:@"YES"])
//    {
//        // Switch view to pick image
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"
//                                                             bundle:nil];
//        UINavigationController *vc_Home =
//        [storyboard instantiateViewControllerWithIdentifier:@"VC_HomeNavigation"];
//        self.window.rootViewController = vc_Home;
//        
//    }

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
