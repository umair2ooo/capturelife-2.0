/****************************************************************************************************************
*
* The technology detailed in this software is the subject of various pending and issued patents,
* both internationally and in the United States, including one or more of the following patents:
*
* 5,636,292 C1; 5,710,834; 5,832,119 C1; 6,286,036; 6,311,214; 6,353,672; 6,381,341; 6,400,827;
* 6,516,079; 6,580,808; 6,614,914; 6,647,128; 6,681,029; 6,700,990; 6,704,869; 6,813,366;
* 6,879,701; 6,988,202; 7,003,132; 7,013,021; 7,054,465; 7,068,811; 7,068,812; 7,072,487;
* 7,116,781; 7,158,654; 7,280,672; 7,349,552; 7,369,678; 7,461,136; 7,564,992; 7,567,686;
* 7,590,259; 7,657,057; 7,672,477; 7,720,249; 7,751,588; and EP 1137251 B1; EP 0824821 B1;
* and JP-3949679, all owned by Digimarc Corporation.
*
* Use of such technology requires a license from Digimarc Corporation, USA.  Receipt of this software
* conveys no license under the foregoing patents, nor under any of Digimarc’s other patent, trademark,
* or copyright rights.
*
* This software comprises CONFIDENTIAL INFORMATION, including TRADE SECRETS, of Digimarc Corporation,
* USA, and is protected by a license agreement and/or non-disclosure agreement with Digimarc.  It is
* important that this software be used, copied and/or disclosed only in accordance with such
* agreements.
*
* © Copyright, Digimarc Corporation, USA.  All Rights Reserved.
*
****************************************************************************************************************/

/** @class DMReader
* See the @ref DMReaderGroup "DMReader API" for complete documentation of this class.
* 
*/
/** @defgroup DMReaderGroup DMReader API
* The DMReader class contains methods for detecting watermarks and properties for setting reader configuration.
*/
#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>

#define kDMImageReader_eCoreReadResult      @"eCoreReadResult"						///< @ingroup DMReaderGroup
#define kDMImageReader_eCore_SUCCESS_MarkFoundAndRead   @"MarkFoundAndRead"             ///< @ingroup DMReaderGroup
#define kDMImageReader_eCore_FAIL_MarkNotPresent        @"MarkNotPresent"               ///< @ingroup DMReaderGroup
#define kDMImageReader_eCore_FAIL_MarkPresentUnreadable @"MarkPresentNotReadable"       ///< @ingroup DMReaderGroup
#define kDMImageReader_eCore_FAIL_MarkFound             @"MarkFoundButNotRead"          ///< @ingroup DMReaderGroup
#define kDMImageReader_eCore_FAIL_InternalError         @"InternalError"				///< @ingroup DMReaderGroup

#define kDMImageReader_PayloadPath          @"strPayloadPath"						///< @ingroup DMReaderGroup
#define kDMImageReader_nPayloadBits         @"nPayloadBits"								///< @ingroup DMReaderGroup

#define kDMImageReader_eCategory            @"eCategory"							///< @ingroup DMReaderGroup
#define kDMImageReader_eCategory_Chroma         @"Chroma"								///< @ingroup DMReaderGroup
#define kDMImageReader_eCategory_Classic        @"Classic"								///< @ingroup DMReaderGroup

#define kDMImageReader_eFormat              @"eFormat"								///< @ingroup DMReaderGroup
#define kDMImageReader_eFormat_BGRA             @"BGRA"									///< @ingroup DMReaderGroup
#define kDMImageReader_eFormat_YUV420           @"YUV420"								///< @ingroup DMReaderGroup

#define kDMImageReader_fScale               @"fScale"								///< @ingroup DMReaderGroup
#define kDMImageReader_fRotation            @"fRotation"							///< @ingroup DMReaderGroup

#define kDMImageReader_msTotalReadTime      @"msTotalReadTime"						///< @ingroup DMReaderGroup
#define kDMImageReader_msStage1             @"msStage1"								///< @ingroup DMReaderGroup
#define kDMImageReader_msStage2             @"msStage2"								///< @ingroup DMReaderGroup
#define kDMImageReader_msStage3             @"msStage3"								///< @ingroup DMReaderGroup

// More readInfo dictionary contents and published keys will be added in future...

/** @ingroup DMReaderGroup */
typedef enum DMCaptureFormat
{
    DMCaptureFormatBGRA,
    DMCaptureFormatYUV420
} DMCaptureFormat;

/** @ingroup DMReaderGroup */
typedef enum DMWatermarkCategory
{
    DMWatermarkCategoryClassic,
    DMWatermarkCategoryChroma
} DMWatermarkCategory;

/** @defgroup errorHandling Error Handling
* @ingroup DMReaderGroup
*/
extern NSString* const DigimarcReaderErrorDomain;			///< @ingroup errorHandling
#define kErrorReadFailure 901								///< @ingroup errorHandling

@class DMPayload;

# pragma mark DMReader Interface

@interface DMReader : NSObject

/**
* Reads an image buffer, looking for Digimarc image watermarks.
*
* Given an image buffer, returns YES if watermark is present, NO otherwise. CPM_Path is set if a watermark is
* present.  If an error occurs, details will be returned in the error object.
*
* @param imageBufferRef CVImageBufferRef containing the image to be read
* @param cpmPath Address of an NSString* variable to receive the CPM protocol pathname + value watermark ID string.  Must NOT be NULL.
* @param readerInfo Optional address of an NSDictionary* variable to receive any reader diagnostics.  May be nil.  Not available for all readers and read scenarios.
* @param error Address of an NSError* variable to receive any errors detected during the read.
*
* @return YES if watermark found, NO otherwise
*
* MAINTENANCE NOTE (9/25/2013): Replaces deprecated readImageBufferSynchronous:readResult:error:
*
* @ingroup DMReaderGroup
*/
- (BOOL) readImageBufferSynchronous:(CVImageBufferRef)imageBufferRef toCpmPath:(NSString**)cpmPath withReaderInfo:(NSDictionary**)readerInfo error:(NSError**)error;

/**
* Reads an image buffer in a blocking synchronous call.
*
* Given an image, returns YES if watermark is present, NO otherwise. readResult is set if a watermark is present.
* This method is intended primarily for testing.  If an error occurs, details will be returned in the error object.
*
* @param image UIImage object to be read
* @param cpmPath Address of an NSString* variable to receive the CPM protocol pathname + value watermark ID string.  Must NOT be NULL.
* @param readerInfo Optional address of an NSDictionary* variable to receive any reader diagnostics.  May be nil.  Not available for all readers and read scenarios.
* @param error Address of an NSError* variable to receive any errors detected during the read.
* @return YES if watermark found, NO otherwise
*
* MAINTENANCE NOTE (9/25/2013): Replaces deprecated readImageSynchronous:readResult:error:
*
* @ingroup DMReaderGroup
*/
- (BOOL) readImageSynchronous:(UIImage*)image toCpmPath:(NSString**)cpmPath withReaderInfo:(NSDictionary**)readerInfo error:(NSError**)error;

/**
* Optimize reading performance by scaling down very high resolution images.
*
* Reduces the size of the input image by the given ratio prior to watermark detection. Modifying this property
* may be useful in some SDK applications. See documentation about image resolution.
*
* Default: 1.  This value must be greater than or equal to 1.
*
* @ingroup DMReaderGroup
*/
@property (nonatomic) int downsampleFactor;

/**
* Enable reading for Digimarc Classic type image watermarks.
*
* Digimarc image watermarks can be recorded in either Classic or Chroma format.   The reader can be configured
* to look for Classic only, Chroma only, or both Classic and Chroma watermarks.
*
* This property selects whether the reader should look for Classic watermarks.
*
* - YES indicates that Classic watermarks are supported
* - NO indicates otherwise.
*
* Default: YES
*
* @ingroup DMReaderGroup
*/
@property (nonatomic) BOOL supportClassicMarks;

/**
* Enable reading for Digimarc Chroma type image watermarks.
*
* Digimarc image watermarks can be recorded in either Classic or Chroma format.   The reader can be configured
* to look for Classic only, Chroma only, or both Classic and Chroma watermarks.
*
* This property selects whether the reader should look for Chroma watermarks.
*
* - YES indicates that Chroma watermarks are supported
* - NO indicates otherwise.
*
* Default: YES
*
* @ingroup DMReaderGroup
*/
@property (nonatomic) BOOL supportChromaMarks;

/**
* Describes the color format of the input image.
*
* DMCaptureFormat is an enumerated type that describes the color format of the input image. This type indicates
* whether the camera delivers:
*
*  - YUV420 (DMCaptureFormatYUV420) frames
*  - BGRA (DMCaptureFormatBGRA) frames
*
* Generally, only older model cameras employ BGRA. YUV420 is preferred.
*
* Default: DMCaptureFormatYUV420
*
* @ingroup DMReaderGroup
*/
@property (nonatomic) DMCaptureFormat captureFormat;

/**
* Describes whether the camera supports auto-focus.
*
* - YES indicates that the camera used for video capture has auto-focus
* - NO indicates otherwise
*
* Setting this to NO can improve watermark reading for devices that do not have auto-focus. Most current devices have auto-focus.
*
* Default: YES
*
* @ingroup DMReaderGroup
*/
@property BOOL autoFocusOn;

/**
* 
*
* @deprecated (DMSDK 1.3.0) All DMSDK components are now tracked with the single version property in DMSManager.
* This DMReader version property will be removed in a future distribution of the DMSDK.  In the meantime, it will simply return the DMSManager version.
*
* @ingroup DMReaderGroup
*/
@property (readonly, nonatomic) __attribute__((deprecated)) NSString * version;

@end

