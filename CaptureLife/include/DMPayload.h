/*************************************************************
 
 The technology detailed in this software is the subject of various pending and issued patents,
 both internationally and in the United States, including one or more of the following patents:
 
 5,636,292 C1; 5,710,834; 5,832,119 C1; 6,286,036; 6,311,214; 6,353,672; 6,381,341; 6,400,827;
 6,516,079; 6,580,808; 6,614,914; 6,647,128; 6,681,029; 6,700,990; 6,704,869; 6,813,366;
 6,879,701; 6,988,202; 7,003,132; 7,013,021; 7,054,465; 7,068,811; 7,068,812; 7,072,487;
 7,116,781; 7,158,654; 7,280,672; 7,349,552; 7,369,678; 7,461,136; 7,564,992; 7,567,686;
 7,590,259; 7,657,057; 7,672,477; 7,720,249; 7,751,588; and EP 1137251 B1; EP 0824821 B1;
 and JP-3949679, all owned by Digimarc Corporation.
 
 Use of such technology requires a license from Digimarc Corporation, USA.  Receipt of this software
 conveys no license under the foregoing patents, nor under any of Digimarc’s other patent, trademark,
 or copyright rights.
 
 This software comprises CONFIDENTIAL INFORMATION, including TRADE SECRETS, of Digimarc Corporation,
 USA, and is protected by a license agreement and/or non-disclosure agreement with Digimarc.  It is
 important that this software be used, copied and/or disclosed only in accordance with such
 agreements.
 
 © Copyright, Digimarc Corporation, USA.  All Rights Reserved.
 
*************************************************************/
/// @file DMPayload.h

/** @class DMPayload
 * See the @ref DMPayloadGroup "DMPayload API" for complete documentation of this class.
 *
 */

/** @defgroup DMPayloadGroup DMPayload API
 *
 * DMPayload class encapsulates the payloads detected from DMSDK scanning of image or audio media.
 *
 * DMPayload is designed so that an application can manage it easily and provide it directly to the DMResolver component for resolution.
 * In general, applications have little need to access the internal structure of a payload, aside from perhaps comparing two DMPayload objects for equality, a task for which a comparison operator is supplied. However, to support some specific use cases, the DMPayload class provides read-only access to payload attributes. In particular, it is sometimes useful to access the value string contained in a payload.
 * @see See the @ref payloadTablePage "Payload Properties Table" for complete details on accessing and interpreting payload attributes.
 *
 * DMPayload also implements the NSCoding protocol, so that DMPayload objects may be easily archived and unarchived into
 * core data dbs or other storage containers.  This is often used by applications to build audit trails
 * or "recently visited" history lists.
 */

/**
 * The types of payloads
 *
 * @deprecated (DMSDK 1.2.0) DMPayloadType codes are being phased out to be replaced with DMPayloadCategory groups
 * along with detailed payload inspection methods.  DMPayloadType will remain
 * available for now, but will be removed in a future DMSDK release.
 *
 * The specific type numbers shown here are assigned by DDIM resolver v2 API (for types < 1000).
 *
 * @ingroup DMPayloadGroup
 */
typedef enum DMPayloadType
{
    DMPayloadTypeMobileImage = 5,		///< Digimarc 32-bit Classic or Chroma watermarks
    DMPayloadTypeMobileImageV6 = 6,		///< Digimarc 64-bit Classic or Chroma watermarks
    DMPayloadTypeMobileImageV7 = 7,		///< Digimarc 47-bit Packaging watermarks, for GTIN-12, GTIN-14
    DMPayloadTypeMobileImageV8 = 8,		///< Digimarc 64-bit Packaging watermarks, for GTIN-12, GTIN-14 + extra metadata
    DMPayloadTypeMobileImageV9 = 9,		///< Digimarc 98-bit Packaging watermarks, for SGTIN
    DMPayloadTypeBarcode = 253,			///< Standard 1D barcode types
    DMPayloadTypeAudioAFRE = 254,		///< Digimarc audio watermarks, as used in DDIM online audio embedding
    DMPayloadTypeAudioTDS4 = 255,		///< Digimarc audio watermarks, as used in some specialized applications
    DMPayloadTypeUnknown = 1000,		///< Unknown payload format, not resolvable
    DMPayloadTypeQRcode = 1001			///< Standard QR code barcodes.  Payload value may contain URL.  Not resolvable with DDIM.
} DMPayloadType;

/**
 * Payload Categories
 *
 * These categories provide simplified organization of all payload types from a common source.   Typical usage would be to
 * tie payloads detected to input source, sensor, or UI activity that generated them.  For example, in Discover the
 * payload category defines which icon to display in payoff history lists, when the payoff itself does not provide a thumbnail image.
 *
 * Payload categories, along with the detailed payload inspection methods, replace the deprecated DMPayloadType codes.
 *
 * @ingroup DMPayloadGroup
 */
typedef enum DMPayloadCategory
{
    DMPayloadCategoryImage = 1,         ///< All Digimarc Image Watermark types
    DMPayloadCategoryAudio = 2,         ///< All Digimarc Audio Watermark types
    DMPayloadCategoryBarcode = 3,       ///< All Barcode types, including 1D barcodes and QRcodes.  See also isQRCode attribute.
    DMPayloadCategoryUnknown = 1000
} DMPayloadCategory;

/*-------------------------------------------------------------------------------------*/
#pragma mark - DMPayload initializers and archivers
/*-------------------------------------------------------------------------------------*/

@interface DMPayload : NSObject <NSCoding>

/** NSCoding support -- archiving
 * DMPayload always stores its current internal format, consisting of a compound, fully qualified cpmPath string.
 *
 * The cpmPath string contains precise payload type, protocol, version, and value.
 * @ingroup DMPayloadGroup
 */
- (void)encodeWithCoder:(NSCoder *)encoder;

/** NSCoding support -- unarchiving
 * DMPayload looks for its preferred cpmPath string format in the incoming archive.  If not available, it will go
 * on to look for legacy forms of DMPayload type and data array objects.   The resulting DMPayload object
 * in either case is converted to current version DMPayload cpmPath string.
 *
 * This allows user applications built with previous generation DM Mobile Image SDKs, including Digimarc Discover
 * and white label variations, to be able to upgrade in place, migrating any archived user payload history to
 * current DMPayload format without having to provide a data converter.
 * @ingroup DMPayloadGroup
 */
- (id)initWithCoder:(NSCoder *)aDecoder;

/** Create an instance of DMPayload with the correct type.
 * @ingroup DMPayloadGroup
 */
- (id) initWithCpmPath:(NSString*)cpmPath;

/** Convenience method exposed for apple native barcode reader in app-defined image source.
 * @ingroup DMPayloadGroup
 */
- (id) initWithBarcode:(NSString*)barcodeContent;

/** Convenience method exposed for apple native barcode reader in app-defined image source.
 * @ingroup DMPayloadGroup
 */
- (id) initWithQRcode:(NSString*)qrcodeContent;

/*-------------------------------------------------------------------------------------*/
#pragma mark - Compare this DMPayload with other objects
/*-------------------------------------------------------------------------------------*/

/** Compare incoming payload with others to test for duplicates.
 *
 * This method is useful when comparing an incoming payload to a cache of one or more previously reported payloads.
 * The compare operand accepts any NSObject* or nil, so that objects of mixed types may be stored in the same cache container.
 *
 * For example metadata dictionaries returned from the Apple barcode reader may be stored in the same container as
 * DMPayload objects.
 *
 * @param objectToCompare Any NSObject or nil.
 * @result YES if objectToCompare is also a DMPayload object, with logically equal payload value, NO otherwise.
 *
 * @ingroup DMPayloadGroup
 */
- (BOOL) isEqual:(id)objectToCompare;

/*-------------------------------------------------------------------------------------*/
#pragma mark - DMPayload basic type queries
/*-------------------------------------------------------------------------------------*/

/** Returns the type of payload
 *
 * @deprecated (DMSDK 1.2.0) See comments in typedef for DMPayloadType.
 *
 * @ingroup DMPayloadGroup
 */
- (DMPayloadType) getPayloadType;

/** Returns the group or category of reader that detected this payload.
 *
 * See comments in typedef for DMPayloadCategory.  Also see related specific category query methods isImage, isAudio, isBarcode, isQRCode.
 *
 * @ingroup DMPayloadGroup
 */
- (DMPayloadCategory) getPayloadCategory;

/** Is this payload a member of the DMPayloadCategoryImage group ?
 *
 * @returns YES for all variations of Digimarc Image Watermarks.  Else NO.
 * @ingroup DMPayloadGroup
 */
- (BOOL) isImage;

/** Is this payload a member of the DMPayloadCategoryAudio group ?
 *
 * @returns YES for all variations of Digimarc Audio Watermarks.  Else NO.
 * @ingroup DMPayloadGroup
 */
- (BOOL) isAudio;

/** Is this payload a member of the DMPayloadCategoryBarcode group ?
 *
 * @returns YES for all variations of Barcodes, including 1D codes and QRcodes.  Else NO.
 * @ingroup DMPayloadGroup
 */
- (BOOL) isBarcode;

/** Is this payload a member of the DMPayloadCategoryBarcode group and is specifically a CODE39 barcode ?
 *
 * @returns YES for CODE39 only.  The DDIM resolver does not handle barcodes of this type, so sending this payload through DMResolver will result in a simulated payoff with a Google Search URL and query based on URL encoded version of the CODE39 barcode value string.   Some applications may choose to separate payloads of this type into their own application-specfiic payload handling logic.
 * @ingroup DMPayloadGroup
 */
- (BOOL) isCode39;

/** Is this payload a member of the DMPayloadCategoryBarcode group and is specifically a CODE128 barcode ?
 *
 * @returns YES for CODE128 only.  The DDIM resolver does not handle barcodes of this type, so sending this payload through DMResolver will result in a simulated payoff with a Google Search URL and query based on URL encoded version of the CODE128 barcode value string.   Some applications may choose to separate payloads of this type into their own application-specfiic payload handling logic.
 * @ingroup DMPayloadGroup
 */
- (BOOL) isCode128;

/** Is this payload a member of the DMPayloadCategoryBarcode group and is specifically a QRCODE ?
 *
 * @returns YES for QRCODE only.  The DDIM resolver does not handle barcodes of this type, so sending this payload through DMResolver will result in a simulated payoff.  If the QRCODE value string represents a URL, then the simulated payoff will contain this URL.  Else the simulated payoff will point to a Google Search URL and query based on URL encoded version of the QRCODE value string.   Some applications may choose to separate payloads of this type into their own application-specfiic payload handling logic.
 * @ingroup DMPayloadGroup
 */
- (BOOL) isQRCode;

/*-------------------------------------------------------------------------------------*/
#pragma mark - DMPayload advanced and detailed inspection
/*-------------------------------------------------------------------------------------*/

/** Temporary storage for parsed payload type identifier.
 *
 * Actual type information is contained within the cpmPath string.
 *
 * @deprecated (DMSDK 1.2) replaced with payloadCategory, category attribute query methods, and other CPM attribute queries defined in this interface.
 *
 * @ingroup DMPayloadGroup
 */
@property (readonly) int payloadType;

/** Temporary storage for parsed payload category identifier.
 *
 * Actual type information is contained within the cpmPath string.
 *
 * @ingroup DMPayloadGroup
 */
@property (readonly) int payloadCategory;

/** The fully qualified CPM payload path.
 *
 * Only the cpmPath is actually reported by DMSDK readers.  This single string contains a full identification
 * of the payload type, security key context, and value.
 *
 * The cpmPath will be automatically generated when reading in legacy payloadType and payloadData
 * from earlier versions of the DMPayload object.  This allows users to seamlessly upgrade Digimarc Discover
 * detection history payload database information into the current DMSDK format.
 *
 * @ingroup DMPayloadGroup
 */
@property (readonly, retain) NSString* cpmPath;

/** The effective number of data bits contained in this payload.
 *
 * This information is parsed from the fully qualified protocol and version type information in cpmPath.
 *
 * @ingroup DMPayloadGroup
 */
@property (readonly) int numberValueBits;

/** The grid portion of the fully qualified cpmPath
 *
 * @attention This property is primarily for internal use.
 *
 * Grids identify proprietary media processing algorithms used within the Digimarc image and audio readers.
 * In some cases the grid used to detect a watermark informs the process of identifying associated payoff materials.
 *
 * The most common grids are "GC41" for image watermarks, "Barcode" for 1D barcodes and QRcodes, and "Audio" for audio watermarks.
 *
 * @ingroup DMPayloadGroup
 */
@property (readonly) NSString* grid;

/** The protocol portion of the fully qualified cpmPath.
 *
 * @attention This property is primarily for internal use.
 *
 * Protocol + Version information identifies a specific data encoding scheme, including the number of effective
 * data bits in the final payload value.
 * The most common protocols are "KE" for image watermarks, "AFRE" or "TDS4" for audio watermarks.
 *
 * @ingroup DMPayloadGroup
 */
@property (readonly) NSString* protocol;

/** The protocol version portion of the fully qualified cpmPath.
 *
 * @attention This property is primarily for internal use.
 *
 * Protocol + Version information identifies a specific data encoding scheme, including the number of effective
 * data bits in the final payload value.
 *
 * The protocol version is typically expressed as "v#" string where # is the version number.   Common image watermark
 * versions can be seen in the names of the various DMPayloadType enum values.  Barcode and audio watermark versions are all
 * currently shown as "0" or "v0".
 *
 * @ingroup DMPayloadGroup
 */
@property (readonly) NSString* protocolVersion;

/** The key string identifies the catalog name of an encryption key registered within the CPM library.
 *
 * @attention This property is primarily for internal use.
 *
 * In some cases, a customized watermark encryption key will inform payload to payoff resolving to use an alternate,
 * OEM-specific lookup method.   Watermark encryption keys, embedding with keys, and reading with customized keys
 * require additional Digimarc technical customer support and is available only on a limited basis to meet specific
 * customer requirements.
 *
 * @ingroup DMPayloadGroup
 */
@property (readonly) NSString* key;

/** The payload value portion of the fully qualified cpmPath
 *
 * @deprecated (DMSDK 1.6) renamed to value for consistency with other properties
 *
 * @ingroup DMPayloadGroup
 */
@property (readonly) NSString* valueString;

/** The payload value portion of the fully qualified cpmPath
 *
 * The payload value string format varies by the payload type, grid, protocol, and version. See the @ref payloadTablePage "Payload Properties Table" for complete details on accessing and interpreting payload attributes.
 *
* @warning The internal format of the cpmPath is subject to change. In particular, 
* in an upcoming release, the value method for barcodes of all types will return ASCII hex strings to be consistent with the value method for image and audio payloads.
 * 
 * @note The @link DMPayload::barcodeValue barcodeValue@endlink method will continue to provide the ASCII decimal, UTF-8, and UTF-16
 * representations of the barcode value strings.
 *
 * @ingroup DMPayloadGroup
 */
@property (readonly) NSString* value;

/** The payload value portion of the fully qualified cpmPath in original barcode ASCII decimal or UTF format (not hex-encoded)
 *
 * The barcode value string format varies by the payload type, grid, protocol, and version. See the @ref payloadTablePage "Payload Properties Table" for complete details on accessing and interpreting payload attributes.
 *
 * @note The @link DMPayload::value value@endlink method currently provides the same original barcode value strings as this method, but will change in
 * an upcoming release to provide the ASCII hex representation of barcode value strings.
 *
 * @ingroup DMPayloadGroup
 */
@property (readonly) NSString* barcodeValue;

/** The Digimarc Resolver API v2 query string to use for this payload within DMResolver
 *
 * (serviceURL)/api/v2/payoff/(payload value string)/(payload type code)?...
 *
 * @deprecated (DMSDK 1.6) -- This form of the resolver resource ID is a legacy syntax. The few applications that need this low-level
 * detail should use @link DMPayload::resolverResourceCpmId resolverResourceCpmId@endlink instead.
 * 
 * DMPayload understands and works with the Digimarc Resolver API payoff query to identify the payload to be resolved.
 * DMResolver wraps this fragment with network connection, query packet construction & authentication, async I/O,
 * and handling of the resolve result.   DMResolver does not otherwise contain deep knowledge of cpmPaths or
 * payload internals.
 *
 * @ingroup DMPayloadGroup
 */
@property (readonly) NSString* resolverResourceId;

/** The Digimarc Resolver API query string for modern Common Payload Model (CPM) payloads.
 *
 * (serviceURL)/api/v2/payoffcpm/(cpm path string)?...
 *
 * @attention This property is primarily for internal use.
 *
 * DMPayload understands and works with the Digimarc Resolver API payoffcpm query to identify the payload to be resolved.
 * DMResolver wraps this fragment with network connection, query packet construction & authentication, async I/O,
 * and handling of the resolve result.   DMResolver does not otherwise contain deep knowledge of cpmPaths or
 * payload internals.
 *
 * @ingroup DMPayloadGroup
 */
@property (readonly) NSString* resolverResourceCpmId;

/** Query logical bits based on CPM protocol and version strings
 *
 * @attention This property is primarily for internal use.
 *
 * Note this is a generic utility function and is not tied to the specific cpmPath of this payload object.
 *
 * @ingroup DMPayloadGroup
 */
- (int) numberValueBitsForProtocol:(NSString*)protocol version:(NSString*)version;


@end
