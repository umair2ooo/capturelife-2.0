/****************************************************************************************************************
 *
 * The technology detailed in this software is the subject of various pending and issued patents,
 * both internationally and in the United States, including one or more of the following patents:
 *
 * 5,636,292 C1; 5,710,834; 5,832,119 C1; 6,286,036; 6,311,214; 6,353,672; 6,381,341; 6,400,827;
 * 6,516,079; 6,580,808; 6,614,914; 6,647,128; 6,681,029; 6,700,990; 6,704,869; 6,813,366;
 * 6,879,701; 6,988,202; 7,003,132; 7,013,021; 7,054,465; 7,068,811; 7,068,812; 7,072,487;
 * 7,116,781; 7,158,654; 7,280,672; 7,349,552; 7,369,678; 7,461,136; 7,564,992; 7,567,686;
 * 7,590,259; 7,657,057; 7,672,477; 7,720,249; 7,751,588; and EP 1137251 B1; EP 0824821 B1;
 * and JP-3949679, all owned by Digimarc Corporation.
 *
 * Use of such technology requires a license from Digimarc Corporation, USA.  Receipt of this software
 * conveys no license under the foregoing patents, nor under any of Digimarc’s other patent, trademark,
 * or copyright rights.
 *
 * This software comprises CONFIDENTIAL INFORMATION, including TRADE SECRETS, of Digimarc Corporation,
 * USA, and is protected by a license agreement and/or non-disclosure agreement with Digimarc.  It is
 * important that this software be used, copied and/or disclosed only in accordance with such
 * agreements.
 *
 * © Copyright, Digimarc Corporation, USA.  All Rights Reserved.
 *
 ****************************************************************************************************************/

#import <Foundation/Foundation.h>
#import <CoreLocation/CLLocation.h>
#import <UIKit/UIKit.h>
#import <EventKit/EventKit.h>

@class DMPayload;
@class DMResolveResult;

/** @class DMResolver
 * See the @ref DMResolverGroup "DMResolver API" for complete documentation of this class.
 *
 */

/** @defgroup DMResolverGroup DMResolver API
 * DMResolver resolves DMPayload to DMPayoff translation, via web service queries to Digimarc Resolver service.
 *
 * This class implements methods for managing the resolver, resolving a payload, reporting actions taken by the user,
 * and handling the resolution result (payoff).
 *
 * @resolverThreadsNote
 */

/*-------------------------------------------------------------------------------------*/
#pragma mark - DMResolver Error Codes
/*-------------------------------------------------------------------------------------*/

/** Error domain string associated with any NSError objects associated with DMResolver logic.
 *
 * See also underlying network, NSURL, NSURLConnection, NSHTTPConnection errors which will be passed
 * through DMResolver without translation to DMResolver error codes.
 * @ingroup DMResolverGroup
 */
extern NSString* const DigimarcResolverErrorDomain;

/** DMPayload types that are not supported by DMResolverSDK or the DDIM resolve api.
 * @ingrouop DMResolverGroup
 */
#define kDMResolverErrorUnknownPayloadType 401

/** Resolve request was canceled
 * @ingroup DMResolverGroup
 */
#define kDMResolverErrorRequestCanceled 402

/** Attempted to "resolve" a DMPayload containing a QRCode payload value that is not a URL.
 *
 * The Digimarc DDIM resolver does not actually support resolving any QRCodes,
 * but to provide a consistent interface with other watermark and barcode types,
 * the DMResolver API will attempt to simulate a DMStandardPayoff result when passed a QRCode containing a URL.
 *
 * This error message results when a QRCode is passed in that cannot be translated into a displayable standard payoff.
 * @ingroup DMResolverGroup
 */
#define kDMResolverErrorQRCodeDoesNotContainURL 403


/*-------------------------------------------------------------------------------------*/
#pragma mark - DMResolver Interface
/*-------------------------------------------------------------------------------------*/

/** @typedef ResolveCompletionBlock
 * Application-defined block to be called on async completion of resolving a payload to a payoff, or in case of an error.
 *
 * @code typedef void (^ResolveCompletionBlock)(DMResolveResult* result, NSError* error); @endcode
 *
 * @param result The DMResolveResult provided by DDIM in response to a previous resolve query.
 * @param error Error code if resolve query failed.
 * @ingroup resolvePayload
 */
typedef void (^ResolveCompletionBlock)(DMResolveResult* result, NSError* error);


/** @typedef IsAvailableCompletionBlock
 * Application-defined block to be called on async completion of testing availability of the resolver web service.
 *
 * @code typedef void (^IsAvailableCompletionBlock)(BOOL available, NSError* error); @endcode
 *
 * @param available   YES if network available, service URL was reachable, and service responded to query.  Otherwise NO.
 * @param error Error code if query failed.
 * @ingroup isAvailable
 */
typedef void (^IsAvailableCompletionBlock)(BOOL available, NSError* error);


/** @typedef ReportActionCompletionBlock
 * Application-defined block to be called on async completion of reporting the given action or in case of an error.
 *
 * @code typedef void (^ReportActionCompletionBlock)(NSString* actionToken, NSError* error); @endcode
 *
 * @param actionToken An actionToken string from a previously resolved DMPayoff.   DDIM dispenses unique action tokens for each successful resolve query.
 * @param error Error code if report action failed.
 * @ingroup reportAction
 */
typedef void (^ReportActionCompletionBlock)(NSString* actionToken, NSError* error);


/** @typedef DownloadImageCompletionBlock
 * Application-defined block to be called on async completion of a requested image download, or in case of an error.
 *
 * @code typedef void (^DownloadImageCompletionBlock)(UIImage* image, NSError* error); @endcode
 *
 * @param image UIImage in memory after download from URL.  May be nil if error.
 * @param error Error code if download failed.  Otherwise nil.
 * @ingroup  downloadImage
 */
typedef void (^DownloadImageCompletionBlock)(UIImage *image, NSError* error);


/*----------------------*/
@interface DMResolver : NSObject { }

/** Serializes the given dictionary contents into a new JSON string.
 *
 * @param dataToSerialize Pointer to any (non empty) NSDictionary or NSMutableDictionary.
 * @result a logically equivalent graph of JSON objects in a single string.
 * @ingroup DMResolverGroup
 */
+ (NSString*) serializeDictionary:(NSDictionary*)dataToSerialize;


/** Returns the singleton Resolver instance.
 *
 * Applications should use this method rather than constructing their own instance with alloc, init.
 * @ingroup DMResolverGroup
 */
+ (DMResolver *)sharedDMResolver;


/*----------------------*/
/**
 * @deprecated (DMSDK 1.3.0) All DMSDK components are now tracked with the single DMSManager version property.
 * This DMResolver version property will be removed in a future distribution of DMSDK.
 * In the meantime, this property will simply return the DMSManager version.
 *
 * @ingroup DMResolverGroup
 */
@property (readonly, nonatomic) __attribute__((deprecated)) NSString * version;


/** Login credentials for the resolver web service.
 *
 * The userName and password enable access to the resolver web service. These must be set before
 * attempting to resolve any payloads. They are supplied by Digimarc and are unique per application.
 * @ingroup DMResolverGroup
 */
@property (nonatomic, retain) NSString * userName;


/** Login credentials for the resolver web service.
 *
 * The userName and password enable access to the resolver web service. These must be set before
 * attempting to resolve any payloads. They are supplied by Digimarc and are unique per application.
 * @ingroup DMResolverGroup
 */
@property (nonatomic, retain) NSString * password;


/** Resolver Production Service URL
 *
 *  This is the normal, load balanced, high availability, production resolver intended for use by your completed application.
 *
 *  This is the default value of the serviceURL property.
 * @ingroup DMResolverGroup
 */
#define DMResolverProductionService @"http://resolver.digimarc.net"

/** Resolver Labs Service URL
 *
 *    This is a full featured, single instance of resolver, complete with its own separate database, for
 *    use in building and testing your application before deployment.  Note that any services created in the labs resolver
 *    are not migrated to production resolver, so any marked test content must be reproduced with new service ids from the
 *    actual production service watermark IDs.
 *
 * @ingroup DMResolverGroup
 */
#define DMResolverLabsService @"http://labs-resolver.digimarc.net"

/** Resolver ProductionServiceSSL URL
 *
 * Starting with DMSDK 1.7, the DDIM resolver will accept requests on both http and https connections for applications that require higher security.
 *
 * Normal http connections remain the recommended best practice for this API, as they provide significantly better UI response times,
 * particularly over cellular networks.  The resolver query datagrams do not reveal any personal or proprietary information,
 * and are already digitally signed to prevent tampering.
 *
 * @ingroup DMResolverGroup
 */
#define DMResolverProductionServiceSSL @"https://resolver.digimarc.net"

/** Resolver LabsServiceSSL URL
 *
 * Starting with DMSDK 1.7, the DDIM resolver will accept requests on both http and https connections for applications that require higher security.
 *
 * Normal http connections remain the recommended best practice for this API, as they provide significantly better UI response times,
 * particularly over cellular networks.  The resolver query datagrams do not reveal any personal or proprietary information,
 * and are already digitally signed to prevent tampering.
 *
 * @ingroup DMResolverGroup
 */
#define DMResolverLabsServiceSSL @"https://labs-resolver.digimarc.net"

/** The Resolver web service URL.
 *
 * The defined URLs in this group are supplied by Digimarc and should not be changed unless instructed to by Digimarc.
 *
 * To convert any of these URL strings to NSURL object use this pattern:
 *
 *    [DMResolver sharedResolver].serviceURL = [NSURL URLWithString:DMResolverProductionService];
 *
 * @ingroup DMResolverGroup
 */
@property (nonatomic, retain) NSURL * serviceURL;




/*----------------------*/
/** @defgroup  isAvailable resolverAvailable query API
 *
 * Async DMResolver method to determine resolver availability.
 *
 * @resolverThreadsNote
 *
 * @ingroup DMResolverGroup
 */
/**
 * Determines whether the Digimarc Resolver web service is available.
 *
 * This asynchronous method performs a
 * simple network operation to confirm network connectivity and an expected response from the service.
 * It's use is optional and its intended for debugging or testing the network prior to resolving a Payload.
 *
 * The web service URL is taken from the DMResolver serviceURL property.
 *
 * @param completionBlock This block will be called when query completes or on error.
 * @ingroup isAvailable
 */
- (void) resolverAvailable:(IsAvailableCompletionBlock)completionBlock;


/*----------------------*/
/** @defgroup  resolvePayload resolve API
 *
 * Async DMResolver method to resolve (translate) a payload to a payoff.
 *
 * @resolverThreadsNote
 *
 * @ingroup DMResolverGroup
 */
/**
 * Resolves the given payload to a payoff.
 *
 * This is an asynchronous method that can take several seconds to complete, depending on network conditions.
 *
 * @param payload A payload object previously derived form an image watermark, audio watermark, or barcode read.
 * @param completionBlock This block will be called on resolve query completed or error.
 * @ingroup resolvePayload
 */
-(void) resolve:(DMPayload*)payload completionBlock:(ResolveCompletionBlock)completionBlock;


/**
 * @deprecated (DMSDK 1.4.0) Location tracking has been removed from DMSDK.  This DMResolver resolve:withLocation:completionBlock: method will be removed in a future distribution of DMSDK.
 * In the meantime, any location passed in to this API will be ignored, with remaining params relayed to resolve:completionBlock: method.
 *
 * @ingroup resolvePayload
 */
-(void) __attribute__((deprecated)) resolve:(DMPayload*)payload
withLocation:(CLLocation *)location
completionBlock:(ResolveCompletionBlock)completionBlock;


/**
 * Cancels resolution of the given payload.
 *
 * @param payload  Payload to be cancelled. Will be compared with all payloads currently in the DMNetworkManager queue
 * as currently in progress.
 * @result  Returns YES if cancellation succeeded, NO otherwise.
 * @ingroup resolvePayload
 */
- (BOOL) cancelResolve:(DMPayload*)payload;


/**
 * Cancels all outstanding resolutions.
 *
 * @result Returns YES if cancellation succeeded, NO otherwise.
 * @ingroup resolvePayload
 */
- (BOOL) cancelAllResolves;


/*----------------------*/
/** @defgroup  reportAction reportAction user-action reporting API
 *
 * Async DMResolver method to report user's viewing of payoff content.
 *
 * @resolverThreadsNote
 *
 * @ingroup DMResolverGroup
 */
/**
 * Report the user's viewing of payoff content.
 *
 * Calling this method records metrics about payoff content each time the user actually views the content.
 * Applications are required to use this method for proper usage reporting.
 *
 * In some applications the watermark payload detection, resolve, and payoff will all go toward display
 * of a banner ad or other clickable content.  The reportAction reporting should take place if and when
 * the user clicks through to view the actual payoff content.
 *
 * @param actionToken  An actionToken from a previously resolved DMPayoff.
 * @param completionBlock  This block will be called on completion of the reportAction posting or error.
 * @ingroup reportAction
 */
- (void) reportAction:(NSString*)actionToken
      completionBlock:(ReportActionCompletionBlock)completionBlock;


/*----------------------*/
/** @defgroup  downloadImage downloadImage API
 *
 * Async DMResolver method to download a payoff thumbnail image.
 *
 * @resolverThreadsNote
 *
 * @ingroup DMResolverGroup
 */
/**
 * Start a request to asynchronously download an image from a URL.
 *
 * @param imageURL A thumbnailImageURL from a previously resolved DMPayoff.
 * @param completionBlock  This block will be called on download completed or error.
 * @ingroup  downloadImage
 */
- (void) downloadImage:(NSURL*)imageURL
       completionBlock:(DownloadImageCompletionBlock)completionBlock;

@end


/*-------------------------------------------------------------------------------------*/
#pragma mark - DMPayoff Interface
/*-------------------------------------------------------------------------------------*/

/** The different types of payoffs.
 * @deprecated (DMSDK 1.6) -- This type is no longer used.  All resolve results are now the same type encapsulated in a DMPayoff object.
 * @ingroup DMPayoffGroup
 */
typedef enum DMPayoffType : NSUInteger
{
    DMPayoffTypeStandard = 0,		///< A simple redirect URL, along with title, optional description, and optional thumbnail image.
    DMPayoffTypeInactive = 2		///< This payload exists in DDIM, but its associated service is either expired or not currently activated.
} DMPayoffType;


/** @class DMPayoff
 * See the @ref DMPayoffGroup "DMPayoff API" for complete documentation of this class.
 *
 */
/**
 * @defgroup DMPayoffGroup DMPayoff API
 *
 * The DMPayoff class represents the UI content returned from resolving a DMPayload (audio, image, or barcode detection).
 *
 * @note Prior to DMSDK 1.6, DMPayoff was an abstract base class on which two concrete subclasses DMInactivePayoff and DMStandardPayoff were built.
 * Over time, the distinction between these subclasses has proved more hindrance than help. An intermediate step was to simply handle all resolve
 * results as DMStandardPayoff objects. However, as of DMSDK 1.6 all resolver results are returned
 * as DMPayoff objects, which is now a concrete class containing all the properties and methods necessary for manipulating payoffs.
 * The DMInactivePayoff and DMStandardPayoff subclasses are deprecated. 
 *
 * @note For unrecognized or inactive payoffs, the DMPayoff.synthesizedContent property is TRUE and the DMPayoff contains generic content, a Google search result, or empty strings, as documented in the @link DMPayoff::title title@endlink, @link DMPayoff::subtitle subtitle@endlink, and @link DMPayoff::content content@endlink properties below.
 */
@interface DMPayoff : NSObject

/** 
 * Return a diagnostic string describing this payoff.
 * @ingroup DMPayoffGroup
 */
- (NSString*) description;

/** A descriptive title intended for display to the user in a payoff list or summary in the UI.
 *
 * This field is optional.  If not defined by DDIM content, a synthesized title based on payload value may be provided.   The application may assume
 * this field is never null.  If no displayable string is available, DMResolver will provide an empty string.
 *
 * @ingroup DMPayoffGroup
 */
@property (nonatomic, retain) NSString * title;

/** An optional subtitle for display of additional information in a payoff list or summary in the UI.
 *
 * This field is optional.  If not defined by DDIM content, a synthesized subtitle based on payload protocol may be provided.  The application may assume
 * this field is never null.  If no displayable string is available, DMResolver will provide an empty string.
 *
 * @ingroup DMPayoffGroup
 */
@property (nonatomic, retain) NSString * subtitle;

/** The content URL associated with this payoff.
 *
 * This field is optional.  If not defined by DDIM content, a synthesized generic Google search URL may be provided.  If no appropriate URL can be 
 * provided, the application may see a nil returned in this field.  In this case the application should handle selecting this payoff differently, as
 * there is no content URL for display in a web view.
 *
 * @ingroup DMPayoffGroup
 */
@property (readonly, retain) NSString * content;

/** The type of payoff content.
 *
 * @deprecated (DMSDK 1.6) -- This legacy field was relevant for Rich Payoffs, where a single payload would return a menu of available UI experiences
 * of different types: Web content URL's, Map Coordinates, Social Network posting, Phone Dialing, etc.  All of these are now obsolete except for the
 * Standard Payoff behavior featuring a web content URL.
 *
 * Typically @"Redirect".
 *
 * @ingroup DMPayoffGroup 
 */
@property (readonly, retain) NSString * actionType;

/** A unique token for the resolve query that returned this payoff.
 *
 * This token is to be used with the DMResolver reportAction:completeionBlock: API.  DDIM resolver uses this to track and report on payloads resolved,
 * payoffs viewed, and application user scanning/viewing behavior within the DDIM reporting system.  Content owners then use these reports to review
 * and improve their payoff content and ultimately improve user experience.
 *
 * When present, the action token is a system generated GUID string.  If not present, this field may contain an empty string.  Applications may assume
 * this field is never nil.
 *
 * Whether or not this field contains a value, it may be passed to the DMResolver reportAction:completionBlock: method. That method will detect an
 * empty token string and simply skip the upline reporting operation.
 *
 * @see reportAction DMResolver ReportAction API.
 * @ingroup DMPayoffGroup
 */
@property (readonly, retain) NSString * actionToken;

/** The payoff thumbnail image.
 *
 * This field is optional.  If not defined by DDIM content, it will be nil.
 *
 * If present, this field represents the URL for a displayable image that may be scaled down and shown alongside the payoff title and subtitle.
 *
 * @ingroup DMPayoffGroup
 */
@property (nonatomic, retain) NSURL * thumbnailImageURL;

/** The print image watermark location focal point.
 *
 * @deprecated (DMSDK 1.6) -- This legacy feature from earlier DDIM services is no longer used and no longer available in resolve results.
 * The payoff property has not yet been removed, but is unlikely to ever include any useful data.
 *
 * Indicates which of nine area of interests within the image is the focal point.   The focal
 * point is the area defined by the content owner in the DDIM, as the region of the original
 * image that will contain the watermark.
 *
 *      -------------------------------
 *      | 0.0,0.0 | 0.5,0.0 | 1.0,0.0 |
 *      -------------------------------
 *      | 0.0,0.5 | 0.5,0.5 | 1.0,0.5 |
 *      -------------------------------
 *      | 0.0,1.0 | 0.5,1.0 | 1.0,1.0 |
 *      -------------------------------
 *
 * @ingroup DMPayoffGroup
 */
@property (nonatomic) CGPoint thumbnailImageFocalPoint;

/** A string representing the logical payoff identity within the DDIM database.
 *
 * In some cases DDIM may return the same payoff content for more than one payload.
 * For example the same registered GTIN product code payoff may be registered for both
 * the Digimarc image v7 watermark and the equivalent barcode UPCA/UPCE/EAN13/EAN8 representations.
 * In this case the resolver will return the same payoff for several payload query requests.
 *
 * The application may use the correlation key to detect this scenario and optionally
 * retain only one of the payload/payoff pairs in their reading history.
 *
 * This field is not always provided by DDIM, or by some forms of synthesized payoffs.  
 * When a DDIM product service ID is not available, DMResolver will substitute the payload
 * cpmPath as the payoff correlationKey.
 *
 * @ingroup DMPayoffGroup
 */
@property (nonatomic, retain) NSString * correlationKey;

/** Payoff contains active DDIM content, or synthesized generic content.
 *
 * Applications may choose to treat payoffs with actual DDIM active content specifically for this payload,
 * differently than payoffs with generic or synthesized content.
 *
 * A resolve to active DDIM content may be either a direct payload->payoff lookup, or
 * may be an indirect payload->correlationKey->payoff lookup to a service defined for another payload protocol,
 * but with the same underlying GTIN product code.
 *
 * Synthesized content is defined as anything other than active content.  Examples of synthesized content include:
 * - Thank you for scanning
 * - Google searches
 * - QRCode payloads that contain usable URLs
 *
 * This field will return NO for payoffs with active DDIM content only.  All other payoffs will return YES.
 *
 * @ingroup DMPayoffGroup
 */
@property (nonatomic) BOOL synthesizedContent;

@end


/*-------------------------------------------------------------------------------------*/
#pragma mark - DMStandardPayoff Interface
/*-------------------------------------------------------------------------------------*/

/** @class DMStandardPayoff
 * @deprecated (DMSDK 1.6) -- All useful attributes of this class have been moved to the base class DMPayoff.
 */
/** @defgroup DMStandardPayoffGroup DMStandardPayoff API
 *
 * @warning (DMSDK 1.6) -- The DMStandardPayoff class is <b>deprecated</b>. All useful attributes of this class have been moved to the base class DMPayoff.
 *
 * This subclass of DMPayoff was previously used to define a single content URL, along with title, subtitle, and thumbnail image metadata for
 * display to the user. The DDIM resolver is no longer returning DMStandardPayoff results. All payoff metadata is returned as a DMPayoff object.
 *
 * See the @ref DMPayoffGroup "DMPayoff API" for relevant documentation.
 * @ingroup DMPayoffGroup
 */
@interface DMStandardPayoff : DMPayoff

@end


/*-------------------------------------------------------------------------------------*/
#pragma mark - DMInactivePayoff Interface
/*-------------------------------------------------------------------------------------*/

/** @class DMInactivePayoff
 * @deprecated (DMSDK 1.6) -- All useful attributes of this class have been moved to the base class DMPayoff.
 */
/** @defgroup DMInactivePayoffGroup DMInactivePayoff API
 *
 * @warning (DMSDK 1.6) -- The DMInactivePayoff class is <b>deprecated</b>. All useful attributes of this class have been moved to the base class DMPayoff.
 *
 * This subclass of DMPayoff was previously used for a payoff that has not been activated or has expired on the Digimarc Online Services Portal. The DDIM resolver is no longer returning DMInactivePayoff results.  Instead a DMPayoff is synthesized with a "Thank You for Scanning" user experience.
 *
 * See the @ref DMPayoffGroup "DMPayoff API" for relevant documentation.
 * @ingroup DMPayoffGroup
 */
@interface DMInactivePayoff : DMPayoff

@end


/*-------------------------------------------------------------------------------------*/
#pragma mark - DMResolveResult Interface
/*-------------------------------------------------------------------------------------*/

/** @class DMResolveResult
 * See the @ref DMResolverGroup "DMResolver API" for complete documentation of this class.
 *
 */
/** @defgroup DMResolveResultGroup DMResolveResult API
 *
 * The DMResolveResult is returned to the resolve completion block.  The resolve result wraps the original DMPayload
 * that initiated the resolve, the DMPayoff returned from the Digimarc Resolver, along with some summary information
 * and resolve elapsed time diagnostics.
 *
 * @ingroup DMResolverGroup
 */
@interface  DMResolveResult : NSObject


/** DMPayoffType is an enumerated type indicating the type of payoff.
 *
 * @deprecated (DMSDK 1.6) -- This field is no longer used.  All resolve results are now the same type encapsulated in a DMPayoff object.
 * 
 * To identify an inactive payoff, look for a nil in DMResolveResult.payoff field for no payoff available
 * or more typically look at DMPayoff.synthesizedContent property for payoff contains generic search or 'Thank you for scanning' payoff.
 *
 * @ingroup DMResolveResultGroup
 */
@property DMPayoffType payoffType;  // __attribute__((deprecated))


/** The payoff object.
 *
 * The payoff object contains the user visible metadata associated with a detected watermark.  This is
 * defined within the Digimarc Discover Identity Manager (DDIM) system and associated with the watermark via the Digimarc Resolver.
 *
 * See also DMPayoff.
 *
 * @ingroup DMResolveResultGroup
 */
@property (retain) DMPayoff * payoff;


/** The payload which triggered this resolve query and associated payoff.
 *
 * In the presence of multiple payloads, the application
 * can use this to ensure that it is handling the proper payoff for each payload.
 * @ingroup DMResolveResultGroup
 */
@property (readonly, retain) DMPayload * payload;


/** Time taken for the resolve call.
 *
 * For diagnostic purposes only.
 */
@property int resolveTimeMs;

@end

