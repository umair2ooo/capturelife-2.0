//
//  DMSManager.h
//  Digimarc Mobile SDK (DMSDK)
//
//  Created by localTstewart on 7/12/13.
//  Copyright (c) 2013 Digimarc. All rights reserved.
//
/** @file DMSManager.h */

#ifndef DMSManager_H
#define DMSManager_H

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>
#import "DMSDelegateProtocol.h"

/** @defgroup DMSManagerGroup DMSManager API
 *
 * The DMSManager class controls image and audio sources and reports the detection of watermarks and barcodes.
 *
 */
/** @class DMSManager
 * See the @ref DMSManagerGroup "DMSManager API" for complete documentation of this class.
 *
 */

/** Enable verbose logging of readers configuration parsing, detailed unit test results.
 * @ingroup DMSManagerDiags
 */
extern BOOL DMSGLOBAL_TEST_LOGGING_MODE;

@class DMPayload;       // pre-CPM payload object, see DMReader.h
@class DMResolver;      // see DMResolver.h

/** Reader scheduling profiles.
 *
 * This type is used to select profile settings for both the audio and image media sources.
 * @ingroup DMSManagerConfig
 */
typedef enum DMSProfile : NSUInteger
{
    DMSProfileIdle,			///< Media stream remains running, but no frames are dispatched to readers.
    DMSProfileLow,			///< Slowest rate.  Many frames are skipped between reader dispatches.
    DMSProfileMedium,		///< Medium rate.
    DMSProfileHigh			///< Highest rate.  Most frames are dispatched to readers.
} DMSProfile;

#define kDMSImageProfile_Default DMSProfileMedium		///< @ingroup DMSManagerConfig
#define kDMSAudioProfile_Default DMSProfileMedium		///< @ingroup DMSManagerConfig

#define kDMSImageCacheDepth_Default 1					///< @ingroup DMSManagerCache
#define kDMSAudioCacheDepth_Default 1					///< @ingroup DMSManagerCache

/** @name Apple-defined device model strings returned from systemModelName method
 *
 * To compare your system model with a specific device, use NSString isEqualTo: method.
 * To compare your system model with a device family, use NSString hasPrefix: method.
 * @ingroup DMSManagerConfig
 */
/** @{ */
#define kModelNamePrefix_iPhone3G           @"iPhone1"      ///< @ingroup DMSManagerConfig
#define kModelName_iPhone2G_Gsm             @"iPhone1,1"      ///< @ingroup DMSManagerConfig
#define kModelName_iPhone3G_Gsm             @"iPhone1,2"      ///< @ingroup DMSManagerConfig

#define kModelNamePrefix_iPhone3GS          @"iPhone2"      ///< @ingroup DMSManagerConfig
#define kModelName_iPhone3GS_Gsm            @"iPhone2,1"      ///< @ingroup DMSManagerConfig

#define kModelNamePrefix_iPhone4            @"iPhone3"      ///< @ingroup DMSManagerConfig
#define kModelName_iPhone4_Gsm              @"iPhone3,1"      ///< @ingroup DMSManagerConfig
#define kModelName_iPhone4_GsmRevA          @"iPhone3,2"      ///< @ingroup DMSManagerConfig
#define kModelName_iPhone4_Cdma             @"iPhone3,3"      ///< @ingroup DMSManagerConfig

#define kModelNamePrefix_iPhone4S           @"iPhone4"      ///< @ingroup DMSManagerConfig
#define kModelName_iPhone4S_GsmCdma         @"iPhone4,1"      ///< @ingroup DMSManagerConfig

#define kModelNamePrefix_iPhone5            @"iPhone5"      ///< @ingroup DMSManagerConfig
#define kModelName_iPhone5_Gsm              @"iPhone5,1"      ///< @ingroup DMSManagerConfig
#define kModelName_iPhone5_GsmCdma          @"iPhone5,2"      ///< @ingroup DMSManagerConfig
#define kModelName_iPhone5C_Gsm             @"iPhone5,3"      ///< @ingroup DMSManagerConfig
#define kModelName_iPhone5C_GsmCdma         @"iPhone5,4"      ///< @ingroup DMSManagerConfig

#define kModelNamePrefix_iPhone5S           @"iPhone6"      ///< @ingroup DMSManagerConfig
#define kModelName_iPhone5S_Gsm             @"iPhone6,1"      ///< @ingroup DMSManagerConfig
#define kModelName_iPhone5S_GsmCdma         @"iPhone6,2"      ///< @ingroup DMSManagerConfig

#define kModelNamePrefix_iPad1              @"iPad1"      ///< @ingroup DMSManagerConfig
#define kModelName_iPad1                    @"iPad1,1"      ///< @ingroup DMSManagerConfig

#define kModelNamePrefix_iPad2              @"iPad2"      ///< @ingroup DMSManagerConfig
#define kModelName_iPad2_Wifi               @"iPad2,1"      ///< @ingroup DMSManagerConfig
#define kModelName_iPad2_Gsm                @"iPad2,2"      ///< @ingroup DMSManagerConfig
#define kModelName_iPad2_Cdma               @"iPad2,3"      ///< @ingroup DMSManagerConfig
#define kModelName_iPad2_WifiRevA           @"iPad2,4"      ///< @ingroup DMSManagerConfig
#define kModelName_iPadMini1_Wifi           @"iPad2,5"      ///< @ingroup DMSManagerConfig
#define kModelName_iPadMini1_Gsm            @"iPad2,6"      ///< @ingroup DMSManagerConfig
#define kModelName_iPadMini1_GsmCdma        @"iPad2,7"      ///< @ingroup DMSManagerConfig

#define kModelNamePrefix_iPad3              @"iPad3"      ///< @ingroup DMSManagerConfig
#define kModelName_iPad3_Wifi               @"iPad3,1"      ///< @ingroup DMSManagerConfig
#define kModelName_iPad3_GsmCdma            @"iPad3,2"      ///< @ingroup DMSManagerConfig
#define kModelName_iPad3_Gsm                @"iPad3,3"      ///< @ingroup DMSManagerConfig
#define kModelName_iPad4_Wifi               @"iPad3,4"      ///< @ingroup DMSManagerConfig
#define kModelName_iPad4_Gsm                @"iPad3,5"      ///< @ingroup DMSManagerConfig
#define kModelName_iPad4_GsmCdma            @"iPad3,6"      ///< @ingroup DMSManagerConfig

#define kModelNamePrefix_iPadAir            @"iPad4"      ///< @ingroup DMSManagerConfig
#define kModelName_iPad5Air_Wifi            @"iPad4,1"      ///< @ingroup DMSManagerConfig
#define kModelName_iPad5Air_Cellular        @"iPad4,2"      ///< @ingroup DMSManagerConfig
#define kModelName_iPadMini2Retina_Wifi     @"iPad4,4"      ///< @ingroup DMSManagerConfig
#define kModelName_iPadMini2Retina_Cellular @"iPad4,5"      ///< @ingroup DMSManagerConfig

#define kModelName_iPod1G                   @"iPod1,1"      ///< @ingroup DMSManagerConfig
#define kModelName_iPod2G                   @"iPod2,1"      ///< @ingroup DMSManagerConfig
#define kModelName_iPod3G                   @"iPod3,1"      ///< @ingroup DMSManagerConfig
#define kModelName_iPod4G                   @"iPod4,1"      ///< @ingroup DMSManagerConfig
#define kModelName_iPod5G                   @"iPod5,1"      ///< @ingroup DMSManagerConfig
/** @} */

@interface DMSManager : NSObject

/*-------------------------------------------------------------------------------------*/
#pragma mark - DMSManager Configuration
/*-------------------------------------------------------------------------------------*/

/** @defgroup DMSManagerConfig DMSManager Readers Configuration and Runtime Profile Scheduling API
 *
 * This API defines and configures the array of image and audio readers the DMSManager will
 * apply to incoming media source data.
 *
 * The DMSReadersConfig.json configuration file defines a boolean masterEnable switch for each
 * reader, along with 3 reader profiles with varying degree of aggressiveness.  The application
 * may select at runtime which of these profiles to apply to the image reading system and
 * audio reading system.
 *
 * The configuration profiles for each reader implement processing filtering by selecting
 * how many incoming frames to skip over between reader dispatches.   An interval of 1 in
 * a profile would indicate process every frame.  An interval of 3 would indicate process
 * every 3rd frame, etc.   In the case of audio, all input is contiguously buffered, but
 * the detection event on this audio stream is triggered by counting incoming 1/8 second
 * blocks.
 *
 * By counting frames (rather than time intervals) the DMSReadersConfig automatically
 * scales for faster or slower mobile devices offering different camera capture frame rates.
 * It is also possible for the application to select alternate DMSReaderConfig files at
 * initialization time to further tune DMSManager behavior for specific device models.
 * The DMSDK distribution file system contains 2 DMSReadersConfig JSON files, one for
 * normal devices (iPhone4S and newer, iPad2 and newer) and one for older devices with
 * lower performance (iPhone 3GS, iPhone4, various iPodTouch models).  The DMSDemo sample
 * application demonstrates methods for identifying device model, and selecting and loading
 * the JSON readers config into DMSManager during startup.
 *
 * The DMSReadersConfig file is processed only at initialization time, before the application
 * opens a DMSManager session, or after closing and before reopening a new DMSManager session.
 * This might be useful in the scenario where a user enters a configuration activity to
 * select a completely different type of readers/detectors to be used.  The application
 * should then close any currently open session, load the new config file, then open a
 * new session.
 *
 * The DMSManager imageReaderProfile and audioReaderProfile can be changed at any time during
 * the life of a session, to dynamically increase or decrease the amount of CPU resources
 * and reading activity being performed on the incoming media streams by DMSDK.
 *
 * @ingroup DMSManagerGroup
 */


/** Current image reading profile (readwrite).
 *
 * The application may modify this setting at any time during an open session,
 * causing immediate adjustment in the frequency that incoming image frames are
 * dispatched to readers.
 *
 * @ingroup DMSManagerConfig
 */
@property DMSProfile imageReadingProfile;


/** Current audio reading profile (readwrite).
 *
 * The application may modify this setting at any time during an open session,
 * causing immediate adjustment in the frequency that incoming audio samples are
 * dispatched to readers.
 *
 * @ingroup DMSManagerConfig
 */
@property DMSProfile audioReadingProfile;


/** Convenience method to translate DMSProfile to a UI display string.
 * @ingroup DMSManagerConfig
 */
+ (NSString*) stringFromDMSProfile:(DMSProfile)profile;


/** Returns the current system model name from IOS kernel.  Retrieved with uname() API.
 *
 * See also \#defines for all (current) iOS device model names.
 * @ingroup DMSManagerConfig
 */
+ (NSString*) systemModelName;


/** Singleton DMSManager accessor.
 * Will lazily auto-create the DMSManager on first reference.
 * @ingroup DMSManagerConfig
 */
+ (DMSManager*) sharedDMSManager;


/** Singleton DMResolver accessor.
 *
 * All initialization of the resolver (Username/Password/ServerURL/options) is the responsibility
 * of the application.   The application is also responsible for all activity resolving payloads
 * to payoffs, handling network errors or retries, and storing and displaying payoff results.
 *
 * In future distributions of DMSDK, the DMSManager may also use this configured shared instance of
 * the resolver to report usage statistics and download runtime updates to the Common Payload Model
 * database.
 * @ingroup DMSManagerConfig
 */
+ (DMResolver*) sharedDMResolver;


/** Standard initializer is remapped to return singleton sharedDMSManaager.
 * @ingroup DMSManagerConfig
 */
- (id) init;


/** Load the readers array from DMSReadersConfig JSON data
 *
 * The application is responsible for providing this configuration resource during
 * DMSManager initialization and before opening a DMSManager session.
 *
 * The config JSON data contains a description of available image and audio readers,
 * enables, reader-specific tuning options, and profile dispatch schedules for each
 * reader and DMSProfile.
 *
 * @param JSONData A complete description of the image and audio readers array.
 * @ingroup DMSManagerConfig
 */
- (BOOL) loadReadersConfigFromJSONData:(NSData*)JSONData;


/** Load the readers array from DMSReadersConfig in a JSON string.
 *
 * Convenience method, converts incoming NSString to NSData, then calls loadReadersConfigFromJSONData:
 * @ingroup DMSManagerConfig
 */
- (BOOL) loadReadersConfigFromJSONString:(NSString*)JSONString;


/** Load the readers array from DMSReadersConfig in a JSON resource file.
 *
 * Convenience method, converts incoming file contents to NSData, then calls loadReadersConfigFromJSONData:
 * @ingroup DMSManagerConfig
 */
- (BOOL) loadReadersConfigFromJSONFile:(NSString*)JSONFilePathname;


/*-------------------------------------------------------------------------------------*/
#pragma mark - DMSManager Session and Media Source operations API
/*-------------------------------------------------------------------------------------*/

/** @defgroup DMSManagerSession DMSManager Session and Media Source operations
 *
 * Application work flow:
 *	- Create and initialize an instance of DMSManager
 *	- Load a DMSReadersConfig readers array configuration into DMSManager
 *	- Create and initialize an imageSource to provide incoming image data
 *	- Create and initialize an audioSource to provide incoming audio data
 *	- Open a DMSManager session
 *	- Start media sources via DMSManager using startImageSource, startAudioSource.
 *	- (optional) Pause media sources via DMSManager using stopImageSource, stopAudioSource.
 *	- (optional) Resume media sources via DMSManager using startImageSource, startAudioSource.
 *	- Stop media sources via DMSManager using stopImageSource, stopAudioSource.
 *	- Close the DMSManager session
 *	- (optional) Load a different DMSReadersConfig for some other type of UI activity or CPU balancing
 *	- (optional) Open and run additional DMSManager sessions
 *	- Release the DMSManager
 *
 * Status, error, warning and payload detection events may occur at any time during a session,
 * and are reported asynchronously to the delegate.
 *
 * @ingroup DMSManagerGroup
 */


/**  The delegate receives notifications of watermark and barcode detections, status changes and error events.
 *
 * The delegate is usually a ViewController or Model class within the application,
 * and is typically the same class that instantiates, configures and controls both
 * the DMSManager and associated image and audio sources.
 *
 * @ingroup DMSManagerSession
 */
@property (weak) id<DMSDelegateProtocol> delegate;


/** The imageSource is responsible for driving the camera or image files, and providing image frame data to DMSManager.
 *
 * The imageSource and audioSource properties are assigned by the openSessionWithImageSource:audioSource:
 * method that begins a DMSManager session.
 *
 * @ingroup DMSManagerSession
 */
@property (weak) id<DMSImageSourceProtocol> imageSource;


/** The audioSource is responsible for driving the microphone or audio files and providing audio samples data to DMSManager.
 *
 * The imageSource and audioSource properties are assigned by the openSessionWithImageSource:audioSource:
 * method that begins a DMSManager session.
 *
 * @ingroup DMSManagerSession
 */
@property (weak) id<DMSAudioSourceProtocol> audioSource;

/** Options dictionary for special options
 *
 * The options dictionary contains key value pairs for defined DMSManager option keys. Set to nil or an empty dictionary to clear all options. Supports KVO observation. Reserved for future use.
 *
 * @ingroup DMSManagerSession
 */
@property (copy) NSDictionary *options;

/** Setter for a specific option value
 *
 * Convenience function for setting a value in the options dictionary without providing a new dictionary value.
 *
 * @ingroup DMSManagerSession
 */
-(void)setOptionValue:(id)value forKey:(NSString *)string;

/** Getter for a specific option value
 *
 * Convenience function for fetching a value from the options dictionary. Identical to calling valueForKey: on the options property, or valueForKeyPath:@"options.[...]" on DMSManager.
 *
 * @ingroup DMSManagerSession
 */
-(id)optionValueForKey:(NSString *)key;

/** Opens a new DMSManager session.
 *
 * It is up to the caller to create and initialize (but not start) the image and audio sources prior to performing
 * this open.
 *
 * DMSManager will Attach to each source and perform various media properties queries to setup the readers.  Once the
 * sources have been attached and all readers initialized, the delegate will receive any warnings or errors posted
 * from the image and audio reader managers and finally a status change to DMSStatusOpen.
 *
 * @param imageSource (may be nil if application will be handling audio media only)
 * @param audioSource (may be nil if application will be handling image media only)
 * @result YES on success.  If any errors are encountered during session open, separate error or warning notifications will be sent to delegate.
 *
 * @ingroup DMSManagerSession
 */
- (BOOL) openSessionWithImageSource:(id<DMSImageSourceProtocol>)imageSource
audioSource:(id<DMSAudioSourceProtocol>)audioSource;

/**
 *
 * @deprecated (DMSDK 1.2.0) The Delegate param has been removed from the open session call and moved up to a wider scope
 * DMSManager property.  You should now set the DMSManager delegate property early on,
 * and leave in place throughout the application life cycle.  This makes it possible for DMSManager to report
 * warnings and errors that occur outside of the the session scope, such as activity to load the configuration
 * file and setup the readers array.   The replacement API is: openSessionWithImageSource:audioSource: .
 *
 * @ingroup DMSManagerSession
 */
- (BOOL) __attribute__((deprecated)) openSessionWithDelegate:(id<DMSDelegateProtocol>)delegate
imageSource:(id<DMSImageSourceProtocol>)imageSource
audioSource:(id<DMSAudioSourceProtocol>)audioSource;


/** Closes an existing DMSManager session.
 *
 * DMSManager will stop and detach the imageSource and audioSource components, flush all caches
 * and media data buffers, and release any dynamic session resources.
 *
 * @ingroup DMSManagerSession
 */
- (void) closeSession;


/** Current status of the imageSource
 *
 * Reflects whether the image source is currently running, as controlled by startImageSource and stopImageSource.
 *
 * Starting with DMSDK 1.6.0 the DMSManager accepts redundant start/stop commands to simplify application control
 * of current status.  In Digimarc Discover, this is controlled by the Sensor manager, which prefers to
 * update the state of all sensors whenever anything changes for any of the sensors.   With this
 * new currently running flag, DMSDK will recognize a request to start an already started source,
 * or stop an already stopped source.  In this case DMSDK will ignore the redundant request,
 * and avoid the overhead or side effects of normal source startup or shutdown operations.
 *
 * @ingroup DMSManagerSession
 */
@property (readonly) BOOL imageSourceIsRunning;


/** Start the image source via DMSManager.
 *
 * Start should be called after completing the open of a new session.   This is not done automatically to
 * allow the application to more precisely control when the media starts flowing.   Start should also be called on return
 * from a temporary interruption, e.g. on return from viewing a watermark payoff URL.
 *
 * @ingroup DMSManagerSession
 */
- (BOOL) startImageSource;


/** Stop the image source via DMSManager.
 *
 * Stop should be called when the DMSManager session is to remain open, but the media flow needs to be stopped temporarily.
 * The most common use case for this is to suspend media processing and new payload detection while the user is viewing
 * a previously found payoff.   Stop will also be called automatically when the DMSManager session is closed.
 *
 * @ingroup DMSManagerSession
 */
- (void) stopImageSource;


/** Current status of the audioSource
 *
 * Reflects whether the audio source is currently running, as controlled by startAudioSource and stopAudioSource.
 *
 * Starting with DMSDK 1.6.0 the DMSManager accepts redundant start/stop commands to simplify application control
 * of current status.  In Digimarc Discover, this is controlled by the Sensor manager, which prefers to
 * update the state of all sensors whenever anything changes for any of the sensors.   With this
 * new currently running flag, DMSDK will recognize a request to start an already started source,
 * or stop an already stopped source.  In this case DMSDK will ignore the redundant request,
 * and avoid the overhead or side effects of normal source startup or shutdown operations.
 *
 * @ingroup DMSManagerSession
 */
@property (readonly) BOOL audioSourceIsRunning;


/** Start the audio source via DMSManager.
 *
 * Start should be called after completing the open of a new session.   This is not done automatically to
 * allow the application to more precisely control when the media starts flowing.   Start should also be called on return
 * from a temporary interruption, e.g. on return from viewing a watermark payoff URL.
 *
 * @ingroup DMSManagerSession
 */
- (BOOL) startAudioSource;


/** Stop the audio source via DMSManager.
 *
 * Stop should be called when the DMSManager session is to remain open, but the media flow needs to be stopped temporarily.
 * The most common use case for this is to suspend media processing and new payload detection while the user is viewing
 * a previously found payoff.   Stop will also be called automatically when the DMSManager session is closed.
 *
 * @ingroup DMSManagerSession
 */
- (void) stopAudioSource;


/** Manually flush any buffered audio samples
 *
 * After the incoming audio pipeline is interrupted, there can be a discontinuity that could lead to
 * unexpected results when processing is later resumed.  For example audio samples buffered before
 * the interruption could lead to a payload detection that is now stale and no longer relevant to
 * the recently resumed audio.
 *
 * For this reason, the startAudioSource and stopAudioSource methods will automatically flush any
 * audio samples buffered within DMSManager.  This method is exposed here to allow for unit testing,
 * or other special purpose application requirements.
 *
 * @ingroup DMSManagerSession
 */
- (void) clearAudioBuffers;


/*-------------------------------------------------------------------------------------*/
#pragma mark - DMSManager Payload Cache API
/*-------------------------------------------------------------------------------------*/

/** @defgroup DMSManagerCache DMSManager Payload Cache API
 *
 * As long as the same marked media is incoming, the DMSManager continues to
 * detect the same image watermarks, audio watermarks, or barcodes. As a convenience to the application, the DMSManager provides
 * a payload cache system to identify duplicate payloads and report only new payloads for application attention.
 *
 * There are separate caches for image and audio payloads. The image payload cache is shared by all image watermark
 * and barcode readers and the audio payload cache is shared by all audio watermark readers.
 *
 * Each cache is controlled by a maximum cache depth property. To control the cache behavior, set this property to:
 *		- 0 to disable the cache entirely and report all detections as new.
 *		- 1 to cache only the single most recently reported payload.
 *		- N to cache most recently reported N payloads.
 *
 * When the cache is full, any newly detected payload is placed in the cache and the oldest detected payload is dropped.
 * @ingroup DMSManagerGroup
 */


/** Depth of the image payload cache.
 * @ingroup DMSManagerCache
 */
@property int imagePayloadCacheMaxDepth;


/** Depth of the audio payload cache.
 * @ingroup DMSManagerCache
 */
@property int audioPayloadCacheMaxDepth;


/** Optionally suppress payload detection notifications that are not marked isNew == YES.
 *
 * The application may choose to be notified of all payload detections
 * or only of new detections &mdash; those that are not present in the payload cache.
 *
 * Default value is NO (report all detections)
 * @ingroup DMSManagerCache
 */
@property BOOL reportOnlyNewDetections;									///< @ingroup cacheMgmt


/** Remove a specific payload from the image payload cache.
 * @ingroup DMSManagerCache
 */
- (void) removeFromImagePayloadCache:(DMPayload*)payload;


/** Remove all payloads from the image payload cache.
 * @ingroup DMSManagerCache
 */
- (void) clearImagePayloadCache;


/** Remove a specific payload from the audio payload cache.
 * @ingroup DMSManagerCache
 */
- (void) removeFromAudioPayloadCache:(DMPayload*)payload;


/** Remove all payload from the audio payload cache.
 * @ingroup DMSManagerCache
 */
- (void) clearAudioPayloadCache;


/*-------------------------------------------------------------------------------------*/
#pragma mark - DMSManager methods for incoming image source data
/*-------------------------------------------------------------------------------------*/

/** @defgroup DMSManagerIncomingMedia DMSManager Incoming Image and Audio Media Data API
 *
 * This API provides methods for the image and audio sources to post incoming media data
 * for processing by DMSManager.
 *
 * @ingroup DMSManagerGroup
 */


/**
 * @deprecated (DMSDK 1.5.0) The DMSDK recommended camera settings now accesses a web-hosted knowledge base to provide timely updates
 * to the recommended camera settings even as new ios devices join the market and Digimarc research identifies improved optimization
 * of the camera for watermark reading performance.
 *
 * The selected session or capture device attributes can be viewed with the native AVCaptureSession and AVCaptureDevice properties
 * after completing this initialization.
 *
 * @param session AVCaptureSession -- caller to construct default session and populate with camera capture device input and appropriate output(s)
 * @param device AVCaptureDevice -- camera capture device input already installed in session
 *
 * @return YES if successful, NO if unable to setup the camera or session
 *
 * @ingroup DMSManagerIncomingMedia
 */
+ (BOOL) __attribute__((deprecated)) initializeCaptureSession:(AVCaptureSession*)session andDevice:(AVCaptureDevice*)device;


/** Initialize incoming AVCaptureSession and AVCaptureDevice with recommended settings for current system model
 *
 * The selected session or capture device attributes can be viewed with the native AVCaptureSession and AVCaptureDevice properties
 * after completing this initialization.
 *
 * @param session AVCaptureSession -- caller to construct default session and populate with camera capture device input and appropriate output(s)
 * @param device AVCaptureDevice -- camera capture device input already installed in session
 *
 * @return YES if successful, NO if unable to setup the camera or session
 *
 * @ingroup DMSManagerIncomingMedia
 */
- (BOOL) initializeRecommendedCameraSettingsInCaptureSession:(AVCaptureSession*)session andDevice:(AVCaptureDevice*)device;


/** Version string associated with the currently loaded Recommended Camera Settings Knowledge Base.
 * @ingroup DMSManagerIncomingMedia
 */
- (NSString*) cameraSettingsKBVersion;


/** Rule Name for the currently applied camera settings rule, within the currently loaded Recommended Camera Settings KnowledgeBase.
 * @ingroup DMSManagerIncomingMedia
 */
- (NSString*) cameraSettingsKBCurrentRuleName;


/** Full diagnostic dump of currently loaded Recommended Camera Settings KnowledgeBase.
 * @ingroup DMSManagerIncomingMedia
 */
- (NSString*) cameraSettingsKBDetailedDescription;


/** Process one frame from an expected series of YUV pixel buffer frames from camera preview or movie player.
 *
 * The DMSManager will increment an incoming frames counter and use this frame # when scheduling which readers if
 * any to use in processing this frame.  This allows DMSManager to throttle CPU usage up/down in response to circumstances,
 * often interleaving readers to balance load rather than running all readers on each or specific frames.
 *
 * Any payloads detected, errors encountered, or status changes will be reported asynchronously, regradless of
 * whether wait until done flag is set.
 *
 * @see See also the testImageBuffer:cpmPath:readerInfo:error: method that will skip the DMSManager profiles/scheduling
 * layer, invoke the image watermark/barcode readers on each incoming frame (one per call), and then bypass the
 * delegate notification system and return results directly to caller.
 *
 * @param buffer Incoming CVImageBufferRef from AVCaptureSession or similar streaming video source.
 * @ingroup DMSManagerIncomingMedia
 */
- (void) incomingImageBuffer:(CVImageBufferRef)buffer;


/** Proceses a single UIImage frame
 *
 * This method takes a single UIImage frame, performs image watermark and/or barcode detection,
 * and routes results back through the normal streaming media delegate notifications.
 *
 * @see See also the testImage:cpmPath:readerInfo:error: method for a single frame test method
 * that bypasses the delegate notification system and returns results directly to caller.
 *
 * @param image Incoming UIImage* already loaded from file, download, or other method.
 * @ingroup DMSManagerIncomingMedia
 */
- (void) incomingImage:(UIImage*)image;


/** Proceses a single YUV pixel buffer
 *
 * This is useful for testing or replaying captured images.  In this case there will be a single
 * frame.  The streaming frames scheduling profiles will be bypassed and all enabled detectors will be
 * run on the single incoming image.
 *
 * Unlike other DMS methods, this test method runs synchronously and reports back any payload found
 * directly (rather than using the async delegate payload detected notifications).
 *
 * @param [in] buffer Incoming CVImageBufferRef from AVCaptureSession or similar streaming video source.
 * @param [in, out] cpmPath A pointer to a string pointer to receive the CPM path string for detected imageWatermark or imageBarcode if found, else nil. [in] pointer to pointer should NOT be nil.
 * @param [in, out] readerInfo A pointer to a dictionary pointer to receive the reader diagnostics & auxilliary info (if provided by reader), else nil.  [in] pointer to pointer may be nil.
 * @param [in, out] error A pointer to an error object pointer to receive the error info, if any, generated by the read.  [in] pointer to pointer may be nil.
 * @return YES if imageWatermark or imageBarcode detected and reported.  Else NO.
 * @ingroup DMSManagerIncomingMedia
 */
- (BOOL) testImageBuffer:(CVImageBufferRef)buffer cpmPath:(NSString**)cpmPath readerInfo:(NSDictionary**)readerInfo error:(NSError**)error;


/** Proceses a single UIImage frame
 *
 * This is useful for testing or replaying previously captured images.  In this case there will be a single
 * frame.  The streaming frames scheduling profiles will be bypassed and all enabled detectors will be
 * run on the single incoming image.
 *
 * Unlike other DMS methods, this test method runs synchronously and reports back any payload found
 * directly (rather than using the async delegate payload detected notifications).
 *
 * @param [in] image Incoming UIImage* already loaded from file, download, or other method.
 * @param [in, out] cpmPath A pointer to a string pointer to receive the CPM path string for detected imageWatermark or imageBarcode if found, else nil. [in] pointer to pointer should NOT be nil.
 * @param [in, out] readerInfo A pointer to a dictionary pointer to receive the reader diagnostics & auxilliary info (if provided by reader), else nil.  [in] pointer to pointer may be nil.
 * @param [in, out] error A pointer to an error object pointer to receive the error info, if any, generated by the read.  [in] pointer to pointer may be nil.
 * @return YES if imageWatermark or imageBarcode detected and reported.  Else NO.
 * @ingroup DMSManagerIncomingMedia
 */
- (BOOL) testImage:(UIImage*)image cpmPath:(NSString**)cpmPath readerInfo:(NSDictionary**)readerInfo error:(NSError**)error;


/** Report incoming image media stream or image file has changed
 *
 * The image source should invoke this DMSManager callback when switching between media sources.  The DMSManager will respond
 * by refreshing its image source query calls before returning from this synchronous notification.
 *
 * @param comment string to include in image media change warning notification to application
 * @ingroup DMSManagerIncomingMedia
 */
- (void) imageMediaChanged:(NSString*)comment;


/*-------------------------------------------------------------------------------------*/
#pragma mark - DMSManager methods for incoming audio data
/*-------------------------------------------------------------------------------------*/

/** Post any number of incoming PCM audio samples from the audio source.
 *
 * The incoming samples array may be any number of samples, but each sample should be fully represented
 * with all sample bytes for all channels interleaved.   DMSManager will internally split channels
 * into separate buffers and assemble contiguous blocks of 1/8 second (2000 samples at 16KHz).
 *
 * As each 1/8 second samples block is filled, it is asynchronously sent into the audio reader
 * manager for dispatch to audio readers based on current audioReaderProfile.   The incoming
 * audio thread waits only for the buffering step, then immediately returns to continue collecting
 * audio.
 *
 * The exception to this rule is if the audioSource has requested DMSManager to process audio
 * synchronously such as when working with audio files.  In this case the audio source thread
 * will block and wait for the audioReader manager and any audio readers dispatch to be completed
 * before returning to collect more audio source data.
 *
 * @param data an NSData containing a packed set of PCM audio samples
 * @param count identifies the number of samples in this chunk.
 * @ingroup DMSManagerIncomingMedia
 */

- (void) incomingAudioBuffer:(NSData*)data numSamples:(NSUInteger)count;

/** Report incoming audio media changed
 *
 * The audio source should invoke this DMSDK callback when switching between media sources.  The DMSManager will respond
 * by refreshing its audio source format query calls before returning from this synchronous notification.
 * If the audio format has changed, the DMSManager flushes its internal audio buffers.
 *
 * @param comment string to include in audio media change warning notification to application
 * @ingroup DMSManagerIncomingMedia
 */
- (void) audioMediaChanged:(NSString*) comment;


/*-------------------------------------------------------------------------------------*/
#pragma mark - DMSManager Diagnostics
/*-------------------------------------------------------------------------------------*/

/** @defgroup DMSManagerDiags DMSManager Diagnostic, Logging, and Utility API
 *
 * This API provides additional information useful for implementing test applications,
 * or inspecting DMSManager runtime operations during development.
 *
 * @ingroup DMSManagerGroup
 */


/** Log DMSManager error events to console.
 *
 * This will echo any error notifications to system console.
 *
 * This is sometimes useful for catching errors that occur outside of an open DMSManager session, when no delegate is yet configured to receive notifications.
 *
 * Default value is NO (do not log errors to console).
 * @ingroup DMSManagerDiags
 */
@property BOOL logErrors;


/** Log DMSManager warning events to console.
 *
 * This will echo any warning notifications to system console.
 *
 * This is sometimes useful for catching warnings that occur outside of an open DMSManager session, when no delegate is yet configured to receive notifications.
 *
 * Default value is NO (do not log warnings to console).
 * @ingroup DMSManagerDiags
 */
@property BOOL logWarnings;


/** Should reader diagnostic information should be included with payload detection notifications?
 *
 * Disabling this info reduces memory footprint.
 *
 * Default value is NO (omit reader diagnostics)
 * @ingroup DMSManagerDiags
 */
@property BOOL reportOptionalReaderInfoDiagnostics;


/** Log each dispatch event to each image reader.
 *
 * This is sometimes useful when balancing reader configuration and profile dispatch schedules.
 * @ingroup DMSManagerDiags
 */
@property BOOL logImageReaderDispatches;


/** Log each payload detection from image readers.
 *
 * This will log the detection event, along with payload cpmPath string, frame counter, and timestamp.
 * @ingroup DMSManagerDiags
 */
@property BOOL logImageReaderDetections;


/** Dump the contents of any readerInfo dictionary associated with payload detections from image readers.
 *
 * This will follow the logImageReaderDetection output (if enabled) as a separate log message.
 * @ingroup DMSManagerDiags
 */
@property BOOL logImageReaderDetectionReaderInfo;


/** Log each dispatch event to each audio reader.
 *
 * This is sometimes useful when balancing reader configuration and profile dispatch schedules.
 * @ingroup DMSManagerDiags
 */
@property BOOL logAudioReaderDispatches;


/** Log each payload detection from audio readers.
 *
 * This will log the detection event, along with payload cpmPath string, frame counter, and timestamp.
 * @ingroup DMSManagerDiags
 */
@property BOOL logAudioReaderDetections;


/** Dump the contents of any readerInfo dictionary associated with payload detections from image readers.
 *
 * This will follow the logImageReaderDetection output (if enabled) as a separate log message.
 * @ingroup DMSManagerDiags
 */
@property BOOL logAudioReaderDetectionReaderInfo;


/** Convenience method to translate DMSStatus to a UI display string.
 * @ingroup DMSManagerDiags
 */
+ (NSString*) stringFromDMSStatus:(DMSStatus)status;


/** DMSDK version number string, formatted as major.minor.patch.changelist
 * @ingroup DMSManagerDiags
 */
- (NSString*) version;


/** Current DMSManager status indicator.
 * The delegate is notified of any status changes.   The current status is also available here for access at any time, from any where.
 * @ingroup DMSManagerDiags
 */
- (DMSStatus) currentStatus;


/** Post an error to DMSManager -- for eventual relay on to delegate.
 *
 * This method exposed in DMSManager interface for use by image or audio sources and for unit testing.
 * If possible, consider posting errors within the DMSManager or DMResolver error domains,
 * and document the error codes and UI strings within the DMSDK package.
 *
 * @param error A standard NSError object.  Any error may be reported.
 * @ingroup DMSManagerDiags
 */
- (void) postError:(NSError*)error;


/** Post a warning message to DMSManager -- for eventual relay on to delegate.
 *
 * This method exposed in DMSManager interface for use by image or audio sources and for unit testing.
 *
 * The warning source and message should be self descriptive and suitable for logging to system console.
 * Generally applications are advised to not display warning messages directly on UI surfaces, as the
 * message content and appropriate response are generally of a more technical nature.
 *
 * @param source Should clearly identify the module that is sending the warning.
 * @param msg Should clearly identify the problem and be suitable for display in system console log.
 *
 * @ingroup DMSManagerDiags
 */
- (void) postWarningFrom:(NSString*)source message:(NSString*)msg;

@end

#endif
