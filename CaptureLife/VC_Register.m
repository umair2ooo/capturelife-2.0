//
//  VC_Register.m
//  CaptureLife
//
//  Created by Fahim Bilwani on 4/27/15.
//  Copyright (c) 2015 Royal Cyber. All rights reserved.
//

#import "VC_Register.h"
#import "RadioButton.h"
#import "Singleton.h"

@interface VC_Register ()
{
    RadioButton *radio1;
    Singleton *single;
}

@property (weak, nonatomic) IBOutlet UIScrollView *scroller;
@property (weak, nonatomic) IBOutlet UITextField *text_FirstName;
@property (weak, nonatomic) IBOutlet UITextField *text_LastName;
@property (weak, nonatomic) IBOutlet UITextField *text_Email;
@property (weak, nonatomic) IBOutlet UITextField *text_Password;

- (IBAction)action_Creat:(id)sender;
- (IBAction)action_back:(id)sender;
- (IBAction)action_cancel:(id)sender;
@end

@implementation VC_Register

- (void)viewDidLoad
{
    [super viewDidLoad];
    single = [Singleton retriveSingleton];
    //[self.scroller setContentSize:CGSizeMake(0, 800)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)action_Creat:(id)sender
{
    if (![self.text_Email.text length] || [single method_NSStringIsValidEmail:self.text_Email.text] == false)
    {
        [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                    message:@"Invalid email address"
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
    }
    
    else if (![self.text_Password.text length])
    {
        [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                    message:@"Please enter Password"
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
    }
    
    else if (![self.text_FirstName.text length])
    {
        [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                    message:@"Please enter your Full Name"
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
    }
    
    else if (![self.text_LastName.text length])
    {
        [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                    message:@"Please enter Last Name"
                                   delegate:nil
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil, nil]
         show];
    }
    
    else
    {
        //[self method_submitForm];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                        message:@"Account Created successfully"
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil, nil];
        
        alert.tag = 100;
        
        [alert show];
    }
}

#pragma mark - action_back
- (IBAction)action_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - action_cancel
- (IBAction)action_cancel:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - uialertview delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView tag] == 100)
    {
        if (buttonIndex == 0)
        {
            ApplicationDelegate.single.isGuestCheckOut = true;
            //[self performSegueWithIdentifier:@"segue_exitToLogin" sender:nil];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}
@end
