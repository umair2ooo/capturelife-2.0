#import <Foundation/Foundation.h>

@interface Singleton : NSObject

@property(nonatomic, strong)NSString *string_controllerName;
@property(nonatomic, strong)NSDictionary *dic_productDetail;
@property(nonatomic, strong)NSString *string_WCToken;
@property(nonatomic, strong)NSString *string_WCTrustedToken;
@property(nonatomic, strong)NSString *string_orderId;
@property(nonatomic, strong)NSMutableArray *array_cartObjects;
@property(nonatomic) BOOL isLoggedIn;
@property(nonatomic) BOOL isGuestCheckOut;
@property(nonatomic) BOOL fromDetail;


+(Singleton *)retriveSingleton;

-(BOOL) method_connectToInternet;
-(BOOL) method_NSStringIsValidEmail:(NSString *)checkString;
-(BOOL) method_checkOnlySpaces:(NSString *)str;
-(BOOL)method_phoneNumberValidation:(NSString *)phoneNumber;

-(float) getHeightForText:(NSString*) text withFont:(UIFont*) font andWidth:(float) width;

@end